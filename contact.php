<?php //Template Name: Contact ?>
<?php get_header(); ?>
<!-- Page Title Section -->
<div class="container">
	<div class="row">
		<div class="webriti_page_heading">
			<h1>Contact <span>Us</span></h1>
			<p></p>
			<div class="page_separator" id=""></div>
		</div>
	</div>
</div>
<!-- /Page Title Section -->
<!-- Contact Google Map Section -->
<div class="container">
	
	<div class="row webriti_contact_area">
		<div class="webriti_google_map">			
			<iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d1805.5883481418268!2d75.86509616933229!3d25.16351089759604!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sus!4v1400592876404" width="100%" height="350" frameborder="0" style="border:0"></iframe>
		</div>
	</div>

</div>
<!-- /Contact Google Map Section -->
<!-- Contact Get in touch Section -->
<div class="container">
	<div class="row">
		<div class="contact_heading_title">
			<h2>Get in Touch</h2>
			<div class="contact_separator" id=""></div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-6">
			<h2 class="cont_head_title">Send Us a Message</h2>
			<div class="row webriti_form">
			<div id="myformdata">
			<form id="contactus_form" method="post" class="" action="">
			<?php wp_nonce_field('webriti_name_nonce_check','webriti_name_nonce_field'); ?>
			
				<div class="col-xs-6">
					<input type="text" placeholder="Your Name" id="yourname" name="yourname" class="contact_input_control"  onClick="if(this.value=='Your Name...'){this.value=''}" onBlur="if(this.value==''){this.value='Your Name...'}" />
				</div>
				<div class="col-xs-6">
					<input type="text" placeholder="email" id="email" name="email" class="contact_input_control"  onClick="if(this.value=='Your Email...'){this.value=''}" onBlur="if(this.value==''){this.value='Your Email...'}" />
				</div>
				<div class="col-xs-12">
					<input type="text" placeholder="subject" name="subject" class="contact_input_control" id="subject" onClick="if(this.value=='Subject...'){this.value=''}" onBlur="if(this.value==''){this.value='subject...'}" />
				</div>
				<div class="col-xs-12">
					<textarea class="contact_textarea_control" placeholder="Message" name="message" rows="5" id="message" onClick="if(this.value=='Write Message...'){this.value=''}" onBlur="if(this.value==''){this.value='Write Message...'}"></textarea>
				</div>
				<div class="col-xs-12">					
					<input  type="submit" name="submit" id="submit"  class="cont_form_btn" value="Send Message" />
				</div>
			
			</form>
			</div>
			</div>
			<div id="mailsent" style="display:none;height:348px;">
					<div class="alert alert-success"  >
					<strong>Thank  you!</strong> You successfully sent contact information...
					</div>
				</div>
		</div>
		
		<div class="col-md-6">
			<div class="row webriti_form">
				<div class="col-xs-12">
					<div class="cont_info_address">
					<h2 class="cont_head_title">Contact Info</h2>
						<address>
						6-A, Industrial Estate, NRC<br>
						Kota, Rajasthan (INDIA).<br>
						<abbr title="Phone">Email:</abbr> <a href="mailto:themes@webriti.com">themes@webriti.com</a><br>
						</address>
					</div>
				</div>
			</div>
		</div>
		
	</div>
			
					<?php 
						if(isset($_POST['submit']))
						{
							$flag=1;
							 if(empty($_POST['yourname']))
							{
								$flag=0;
								
								echo "Please Enter Your Name<br>";
							} 
							
							else if(!preg_match('/[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*/',$_POST['yourname']))
							{
								$flag=0;
								echo "Please Enter Valid Name<br>";
							}
							
							 if($_POST['email']=='')
							{
								$flag=0;
								echo "Please Enter E-mail<br>";
							}
							  if($_POST['subject']=='')
							{
								$flag=0;
								echo "Please Enter E-mail<br>";
							}	
							else if(!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i", $_POST['email']))
								{
									$flag=0;
								echo "Please Enter Valid E-Mail<br>";
								}
							 
							
							 if($_POST['message']=='')
							{
								$flag=0;
								echo "Please Enter Message";
							}
							
							 
							if ( empty($_POST) || !wp_verify_nonce($_POST['webriti_name_nonce_field'],'webriti_name_nonce_check') )
						{
						   print 'Sorry, your nonce did not verify.';
						   exit;
						}
						else
						{
							if($flag==1)
							{
								
							$to = get_option('admin_email');
							$subject = trim($_POST['yourname']). trim($_POST['subject']) .get_option("blogname");
							$massage = stripslashes(trim($_POST['message']))."Message sent from:: ".trim($_POST['email']);
							$headers = "From: ".trim($_POST['yourname'])." <".trim($_POST['email']).">\r\nReply-To:".trim($_POST['email']);
							
							$maildata =wp_mail($to, $subject, $massage, $headers);
					
					
							//$maildata =wp_mail(sanitize_email(get_option('admin_email')),trim($_POST['yourname']),trim($_POST['subject']) ." sent you a message from ".get_option("blogname"),stripslashes(trim($_POST['message']))."Message sent from:: ".trim($_POST['email']),"From: ".trim($_POST['yourname'])." <".trim($_POST['email']).">\r\nReply-To:".trim($_POST['email']));
						
							 if($maildata){ 
								echo "<script>jQuery('#myformdata').hide();</script>";
								echo "<script>jQuery('#mailsent').show();</script>";
								
							}
							}
						}
		}
		?>
</div>
<!-- /Contact Get in touch Section -->
<?php get_footer(); ?>