<?php //Template Name: SEO Optimized Images Detail ?>
<?php get_header(); ?>
<!-- Homepage Slider Section -->
<div class="themedetail_main_slider">
	<div class="container">
		<div class="row">
			
			<div class="col-md-7">
					<iframe class="seo_video_slide" width="560" height="315" src="https://www.youtube.com/embed/ltJwMpFXkG8" frameborder="0" allowfullscreen></iframe>
			</div>
			<br><br>
			<div class="col-md-5 col-xs-12 col-sm-12 themedetail_data seo_margin">
				<h2>Seo Optimized Images</h2>
				<p>Automatically add optimized TITLE and ALT tags to the images and get more traffic from search engines.</p>
                <div class="themedetail_btntop">
				</div>
			</div>
			
		</div>
	</div>
</div>

<div class="homepage_main_slide_shadow"></div>

<!-- /Homepage Slider Section -->

<!-- Theme Features Section -->
<div class="seo_details_section">
	<div class="container">
		<div class="row">
			<div class="col-sm-8">
				<h3>Follow the instruction for testing this plugin.</h3><p></p><p></p>
				<p>1. Click the Red Button in the image on the left and login with the username "<b>test</b>" and password "<b>test</b>".</p>
				
				<p>2. Once you logged in check the plugin setting by clicking the SEO Optimized Images tab. </p>
				
				<p>3. After configuring your seo settings go to front end of the test site and use the inspect tool of your respective browser to check for the updated alt and title attributes as per your configuration.</p>
				
				<p>4. We have already added separate pages for the featured image and the woocommerce  to make things more clear. </p>
				<b><h3><a href="http://webriti.com/seo-optimized-images/">Ready To Buy</a></h3></b>
				<p>For further clarification kind drop us a mail <a href="http://webriti.com/contact-us/">here</a></p>
				
			</div><!-- /.col -->
					
			<div class="col-sm-4">
				<div class="browse-theme admin_views">
					<div>
						<h3>View Admin Dashboard</h3>
					</div>
					
					<a class="btn btn-danger" target="_blank" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;" href="https://seo-testdrive.webriti.com/wp-login.php" target="_blank">Click Here</a>
					<p style="color:#fff;font-size:18px;style=" font-family:="" 'helvetica="" neue',="" helvetica,="" arial,="" sans-serif;""=""><br>
						<label style="color:#fff;font-size:17px">Username: test<br> Password: test</label>
					</p>
				</div>
			</div><!-- /.col -->
						
		</div>
	</div>
 </div>
<!-- /Theme Features Section -->


<!--/Plugin Detail Testimonial Section-->
<?php get_footer(); ?>
