<script>
jQuery(function() {
			//	This js For Homepage Testimonial Section
			jQuery('#hc_testimonial').carouFredSel({
				width: '100%',
				responsive : true,
				circular: true,
				prev: '#prev3',
				next: '#next3',
				directon: 'left',
				auto: true,
				scroll : {
						items : 1,
						duration : 1200,
						timeoutDuration : 1200
					},
			});
			
			jQuery('#webriti-testimonial').carouFredSel({
				//width: '100%',
				responsive : true,
				circular: true,
				pagination: "#pager2",
				
				 items: {
                        visible: {
                            min: 1,
                            max: 10
                        }
                    },

				prev: '#prev3',
				next: '#next3',
				directon: 'left',

				auto: true,
				 scroll : {
						items : 1,
						duration : 1500,
						//fx:"uncover-fade",
						//easing: "elastic",
						timeoutDuration : 6000
					},

			});
			
		});
</script>
<!-- Homepage Testimonial Section -->
<div id="reviews" class="themedatail_testimonial_section seo_testimonial_margin">
	<div class="container">
		<div class="themedetail_heading_title">
			<h2>Know what our clients has to say about us</h2>
			<div id="" class="themedetail_separator"></div>
		</div>
		
		<div class="row" id="webriti-testimonial">			
			<div class="col-md-12 themedatail_trestimonial_area pull-left">
				<p>" Thanks brother for an Awesome Plugin , May god bless you and keep you. "</p>
				<div><img class="img-circle testimonial_img" src="<?php echo get_template_directory_uri() ?>/images/testimonial/seo-1.jpeg"></div>
				<h2><i></i>Asaph Azariah <i></i></h2>
			</div>	
			<div class="col-md-12 themedatail_trestimonial_area pull-left">
				<p>" It was not in the end exactly what I needed, but if you are interested in such a solution, it does work and since it does not alter the database, little harm can be done by switching on this plugin. Great work, Priyanshu! "</p>
				<div><img class="img-circle testimonial_img" src="<?php echo get_template_directory_uri() ?>/images/testimonial/seo-2.jpeg"></div>
				<h2><i></i>stevenhermans <i></i></h2>
			</div>
			<div class="col-md-12 themedatail_trestimonial_area pull-left">
				<p>" Just a great simple effective plugin. "</p>
				<div><img class="img-circle testimonial_img" src="<?php echo get_template_directory_uri() ?>/images/testimonial/seo-3.jpeg"></div>
				<h2><i></i>Noahj Champion <i></i></h2>
			</div>
			<div class="col-md-12 themedatail_trestimonial_area pull-left">
				<p>" The first plugin that told you to deactivate Yoast (installed on more than 3M websites!!) to work...funny! "</p>
				<div><img class="img-circle testimonial_img" src="<?php echo get_template_directory_uri() ?>/images/testimonial/no-image.png"></div>
				<h2><i></i>Recyclart<i></i></h2>
			</div>
			<div class="col-md-12 themedatail_trestimonial_area pull-left">
				<p>" Great response on forum too :D "</p>
				<div><img class="img-circle testimonial_img" src="<?php echo get_template_directory_uri() ?>/images/testimonial/no-image.png"></div>
				<h2><i></i>Cocis<i></i></h2>
			</div>
			<div class="col-md-12 themedatail_trestimonial_area pull-left">
				<p>" Went looking for a new way to apply alt and title tags when my old plugin stopped working on everything when i started using image cdn with jetpack photon. A+++ Kudos for better coding. All seems well. "</p>
				<div><img class="img-circle testimonial_img" src="<?php echo get_template_directory_uri() ?>/images/testimonial/no-image.png"></div>
				<h2><i></i>Tecvoid<i></i></h2>
			</div>
			<div class="col-md-12 themedatail_trestimonial_area pull-left">
				<p>" Perfect performance and quick installation. "</p>
				<div><img class="img-circle testimonial_img" src="<?php echo get_template_directory_uri() ?>/images/testimonial/no-image.png"></div>
				<h2><i></i>Atmosferabio <i></i></h2>
			</div>
			<div class="col-md-12 themedatail_trestimonial_area pull-left">
				<p>" Good plugin and good support. "</p>
				<div><img class="img-circle testimonial_img" src="<?php echo get_template_directory_uri() ?>/images/testimonial/no-image.png"></div>
				<h2><i></i>Oipeirates  <i></i></h2>
			</div>
		</div>
		
		<div class="row">
			<div class="pager testi-pager" id="pager2">
				<a href="#" class=""><span>1</span></a>
				<a href="#" class="selected"><span>2</span></a>
				<a href="#" class=""><span>3</span></a>
			</div>
		</div>
		
		
	</div>
</div>		
<!-- /Homepage Testimonial Section -->