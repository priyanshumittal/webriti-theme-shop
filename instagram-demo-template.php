<?php
/**
 * Template Name:Instagram  Demo Template

 */
 
 get_header();
?> 
<div class="container">
<div class="row">
<div class="webriti_page_heading">
<h1>Easy<span> Instagram Feed</span>Demo</h1>
<p></p>
<div class="page_separator"></div>
</div>
</div>
</div>
<div class="container">
<div class="col-md-8">
<?php 
echo do_shortcode('[easyinstagramfeed ]');

?>
<link rel='stylesheet' href='http://localhost/wordpress3.8/wp-admin/load-styles.php?c=0&amp;dir=ltr&amp;load=dashicons,admin-bar,wp-admin,buttons,wp-auth-check,wp-color-picker&amp;ver=4.1' type='text/css' media='all' />
<script type='text/javascript' src='http://localhost/wordpress3.8/wp-admin/load-scripts.php?c=0&amp;load%5B%5D=jquery-core,jquery-migrate,utils,jquery-ui-core,jquery-ui-widget,jquery-ui-tabs,jquery-ui-mouse,jquery-ui-draggable,jquery-ui-sl&amp;load%5B%5D=ider,jquery-touch-punch,iris,wp-color-picker&amp;ver=4.1'></script>


 <script type="text/javascript">
		//Select a type
		
/*jQuery(document).ready(function($){
    $('#eif-color-field').wpColorPicker();
	//alert('hello');
});*/
</script>
</div>

<div class="col-md-4">
<div class="webriti_sidebar_section"><div class="webriti_sidebar_widget">
<div class="webriti_sidebar_widget_title"><h2>Manage Feed Options:</h2>
<div class="webriti-separator-small"></div></div>
<div id="search_form">
	<form action="" method="post" id="form" class="">
		
	
				<div style="margin-bottom:15px">
					<label style="display:block;padding-top:25px;">Show Photos From:</label>
					<div>
						<input class="radio" type="radio" name="feed_search_type" value="user" id="user"  style="display:inline-block;">
						<label for="user_id">User ID:</label>
						<input type="text" name="user_id" id="user_id" placeholder="Eg: 12345678"  style="width:65%;height: 32px;margin-left:5px;">
					</div>
				
					<div>
						<input class="radio" type="radio" name="feed_search_type" value="hashtag" id="hashtag"  checked="" style="display:inline-block;">
						<label for="hashtag">Hashtag:</label>
						<input type="text" name="hashtag_val" id="hashtag_val" placeholder="Eg: adidas" style="width:65%;height: 32px;"><br>
					
				   </div>
				</div>
		        
				<div style="margin-bottom:15px;">
					<label for="feed_cols" >Number of columns:</label>
					
						<select id="feed_cols" name="feed_cols" style="margin-left:10px;">
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4" selected="">4</option>
							<option value="5">5</option>
							<option value="6">6</option>
						</select>
			
				</div>
				<div style="margin-bottom:15px;">
					<label for="padding">Image Padding:</label>
					<input type="text" name="padding_img" id="padding_img"  style="width: 50px;height: 30px;">
					<select id="padding_img_unit" name="padding_img_unit" style="height:30px;margin-left:10px;">
						<option value="%">%</option>
						<option value="px" selected="">px</option>
					</select>
				</div>
			
				

					<input type="submit" id="searchsubmit" value="View Feed" tabindex="11" style="float: left;">
				   
	
	</form>
</div>
</div></div>	

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
 
	    <script type="text/javascript">
		//Select a type

		//jQuery(document).ready(function($) {
	    	var feed_type = $('input:radio[name=feed_search_type]:checked').val();
			
	    	$('input:radio[name=feed_search_type]').on('change', function(){
	    		feed_type = $(this).val();
				//alert(feed_type);
	    	});
			
			$('#form').submit(function(){
	        	var eif_user_id = $('#user_id').val(),
	        		eif_hashtag = $('#hashtag_val').val(),
	        		feed_cols = $('#feed_cols').val();
					img_padding = $('#padding_img').val();
					img_padding_unit = $('#padding_img_unit').val()
				
	        	while(eif_hashtag.charAt(0) === '#')
					    eif_hashtag = eif_hashtag.substr(1);

	        	if( feed_type == 'user' ){
	        		var demo_url = '?user_id=' + eif_user_id + '&cols=' + feed_cols +'&img_padding='+ img_padding +'&img_padding_unit='+img_padding_unit;
					
	        	} else {
	        		var demo_url = '?hashtag=' + eif_hashtag + '&cols=' + feed_cols +'&img_padding='+ img_padding +'&img_padding_unit='+img_padding_unit;
					
	        	}

	            $(this).attr( 'action', demo_url );
	        });

			//Get the parameters from the URL
	        var cur_type = getQueryVariable('type'),
	        	cur_id = getQueryVariable('user_id'),
	        	cur_hashtag = getQueryVariable('hashtag'),
	        	cur_cols = getQueryVariable('cols'),
				cur_padding = getQueryVariable('img_padding'),
				cur_padding_unit = getQueryVariable('img_padding_unit');
			
				
	        if(cur_hashtag !== false)
			{
			 var feed_type = 'hashtag';
		
			}
			else if(cur_id !== false)
			{
			var feed_type = 'user';
			}
			
			
		
			
	        if(cur_cols == false)
			cur_cols = '4';
			if(cur_padding == false)
			cur_padding = '3';
			if(cur_padding_unit == false)
			cur_padding_unit= 'px';

	        if ( feed_type == 'user' ){
			//alert("user");
	        	$('input:radio[name=feed_search_type]#user').prop('checked', true);
	        	if(cur_id == false) cur_id = '';
	        	$('#user_id').val( cur_id );
				$('#padding_img').val(cur_padding);
				$('#padding_img_unit').val(cur_padding_unit);
	        } else if ( feed_type == 'hashtag' ){
	
	        	$('input:radio[name=feed_search_type]#hashtag').prop('checked', true);
	        	if(cur_hashtag == false) cur_hashtag = 'adidas';
	        	$('#hashtag_val').val( cur_hashtag );
				$('#padding_img').val(cur_padding);
				$('#padding_img_unit').val(cur_padding_unit);
	        }			//Set the cols
	        $('#feed_cols').val( cur_cols );

	        //Used to get the URL params
	        function getQueryVariable(variable){
		       var query = window.location.search.substring(1);
		       var vars = query.split("&");
		       for (var i=0;i<vars.length;i++) {
		               var pair = vars[i].split("=");
		               if(pair[0] == variable){return pair[1];}
		       }
		       return(false);
			}

			//});
			
		</script>
</div>
<div class="clear"></div>
</div>
<?php get_footer();?>