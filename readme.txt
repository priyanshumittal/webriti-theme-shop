WEBRITI - THEMESHOP
@Version 1.3.9
* Updated Elementor demo template of the Appointment theme.
@Version 1.3.8
* Updated appointment theme main and elementor detail pages.
@Version 1.3.7
* Updated freemius checkout code for appointment detail page.
@Version 1.3.6
* Added showcase section in appointment detail page.
* Added appointment showcase page.
@Version 1.3.5
* Updated Spasalon theme detail and all themes detail pages.
* Updated free theme detail page links.
@Version 1.3.4.1
* eregi function replaced by preg-match in contact.php file as the function is deprecated in 5.3 and removed from 7.0 and onwards.
@Version 1.3.4
* Updated appointment pro, busiprof pro, and quality pro themes detail page.
@Version 1.3.3
* Added function section in appointment & busiprof themes detail page.
* Updated SEO optimized image detail page links.
@Version 1.3.2
* Corrected freemius checkout code in SEO optimized Images plugin detail page.
@Version 1.3.1
* Added freemius checkout code in SEO optimized Images plugin detail page.
@Version 1.3
* Added freemius checkout code in new spasalon detail pages.
@Version 1.2.9
* Added freemius checkout code in busiprof, quality, rambo, elitepress, and spasalon detail pages.
@Version 1.2.8
* Added freemius checkout code in Appointment Pro theme detail page.
@Version 1.2.7
* Added the Approach FSE theme detail page.
@Version 1.2.6
* Updated Appointment starter sites demos.
@Version 1.2.5
* Added the Elementor templates demo in Appointment Pro theme detail page.
@Version 1.2.4
* Updated the Appointment Pro theme detail page.
@Version 1.2.3
* Updated the Rambo Pro theme detail page.
@Version 1.2.2
* Updated the Appointment Pro theme detail page.
* Updated the ElitePress Pro theme detail page.
@Version 1.2.1
* Updated the Wallstreet Pro theme detail page.
* Updated the Busiprof Pro theme detail page.
* Updated the Free WordPress themes page.
@Version 1.2
Updated Seo plugin details page
@Version 1.1.4
* Updated the Quality theme detail page and free theme page.
@Version 1.1.3
* Updated the Appointment Pro theme detail page.
* Updated the free wordpress themes download links.
@Version 1.1.2
* Update the free theme download links.
@Version 1.1.1
* Change custom Instagram pro demo page button link.
@Version 1.1
* Added Page builder plugin support feature in pricing table and feature section in SEO detail page .

Version:1.0 //Released