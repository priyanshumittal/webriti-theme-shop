<?php get_header(); ?>
<div class="page-seperator"></div>
<div class="container">	
	<div class="row qua_404_wrapper">		
		<div class="col-md-12 webriti_404_error_section">
			<div class="error_404">
				<h2><?php _e('Error 404','webriti'); ?></h2>
				<h4><?php _e('Oops! Page not found','webriti'); ?></h4>
				<p><?php _e('We`re sorry, but the page you are looking for doesn`t exist.','webriti'); ?></p>
				<p><a href="<?php echo esc_html(site_url());?>" class="error_404_btn"><?php _e('Go to Homepage','webriti'); ?></a></p>
			</div>
		</div>		
	</div>	
</div>
<!-- 404 Error Section -->
<?php get_footer(); ?>