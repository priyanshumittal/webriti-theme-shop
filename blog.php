<?php //Template Name: BLOG ?>
<?php get_header(); ?>
<!-- Page Title Section -->
<div class="container">
	<div class="row">
		<div class="webriti_page_heading">
			<h1>Our <span>Blog</span></h1>
			<p></p>
			<div class="page_separator"></div>
		</div>
	</div>
</div>
<!-- /Page Title Section -->
<!-- Blog & Sidebar Section -->
<div class="container">
	<div class="row">		
		<!--Blog Area-->
		<div class="col-md-8">
		<?php  $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				$args = array( 'post_type' => 'post','paged'=>$paged);		
				$post_type_data = new WP_Query( $args );
					while($post_type_data->have_posts()):
					$post_type_data->the_post();
					global $more;
					$more = 0;  ?>
			<div id="post-<?php the_ID(); ?>" <?php post_class('webriti_blog_section'); ?>>
				<div class="webriti_post_title_wrapper">
					<h2><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
					<div class="webriti_post_detail">
						<?php echo get_avatar( get_the_author_meta( 'ID' ), 32 ); ?>
						<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php the_author(); ?></a>
						<small>|</small>
						<?php the_time('M j,Y');?>
						<small>|</small>
						<a href="<?php the_permalink(); ?>"><i class="fa fa-comments"></i>&nbsp;&nbsp;<?php comments_number( 'No Comments', 'one comments', '% comments' ); ?></a>						
						<?php if(get_the_tag_list() != '') { ?>
						<small>|</small>
							<div class="webriti_tags">
								<?php the_tags('',' , ', '<br />'); ?>							
							</div>							
						<?php } ?>
					</div>
				</div>
				<div class="clear"></div>
				<?php $defalt_arg =array('class' => "img-responsive"); ?>
				<?php if(has_post_thumbnail()): ?>
				<div class="webriti_blog_post_img">
					<a  href="<?php the_permalink(); ?>">
						<?php the_post_thumbnail('', $defalt_arg); ?>
					</a>					
				</div>
				<?php endif; ?>	
				<div class="webriti_blog_post_content">
					<?php  the_content( __( 'Read More' , 'webriti' ) ); ?>
				</div>	
			</div>
			<?php endwhile ?>
			<?php $Webriti_pagination = new Webriti_pagination();
				  $Webriti_pagination->Webriti_page($paged, $post_type_data); ?>
		</div>
		<!--/Blog Area-->
		<?php get_sidebar(); ?>
	</div>
</div>
<?php get_footer(); ?>