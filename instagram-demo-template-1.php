<?php
/**
 * Template Name:Instagram  Demo Template 1

 */
 
 get_header();
?> 
<style type="text/css">
.button-insta-login {
	background: #008ec2;
    border-color: #006799;
    color: #fff;
    display: inline-block;
    text-decoration: none;
    font-size: 13px;
    line-height: 26px;
    height: 28px;
    margin: 0;
    padding: 0 10px 1px;
    cursor: pointer;
    border-width: 1px;
    border-style: solid;
    -webkit-appearance: none;
    border-radius: 3px;
    white-space: nowrap;
    box-sizing: border-box;
}

.eif_lightbox_username img[src="undefined"] {
   display: none;
}

#search_form .radio {
	min-height: 0px !important;
}

.button-insta-login:hover, .button-insta-login:focus {
    color: #fff;
    text-decoration: none;
}
</style>
<div class="container">
<div class="row">
<div class="webriti_page_heading">
<h1>Easy<span> Instagram Feed</span>Demo</h1>
<p></p>
<div class="page_separator"></div>
</div>
</div>
</div>
<div class="container">
<div class="col-md-8">
<?php 
echo do_shortcode('[easyinstagramfeed ]');

?>
</div>

<div class="col-md-4">
<div class="webriti_sidebar_section"><div class="webriti_sidebar_widget">
<div class="webriti_sidebar_widget_title"><h2>Manage Feed Options:</h2>
<div class="webriti-separator-small"></div></div>
<div id="search_form">
	<form action="" method="post" id="form" class="">
		
	
				<div style="margin-bottom:15px">
					<label style="display:block;padding-top:25px;">Show Photos From:</label>
					<div>
						<input class="radio" type="radio" name="feed_search_type" value="user" id="user"  style="display:inline-block;">
						<label for="user_id">User ID:</label>
						<input type="text" name="user_id" id="user_id" style="display:none;" placeholder="Eg: 12345678"  style="width:65%;height: 32px;margin-left:5px;">
					
   						 	<a href="https://instagram.com/oauth/authorize/?client_id=44a5744739304a48af362318108030bc&amp;scope=basic+public_content&amp;redirect_uri=http://webriti.com/easy-instagram-feed/eiflite/?return_uri=https://webriti.com/easy-instagram-feed-demo/&amp;response_type=token&state=https://webriti.com/easy-instagram-feed-demo" class="button-insta-login">Connect to my account</a>
						
						<br>
		<span id="userhashtagsection" ><input type="checkbox" id="eif_filter" name="eif_filter"  >
		Tagged by: <input type="text" id="eif_filter_tagged_by" name="eif_filter_tagged_by" style="margin-top:4px;width:60%;height: 32px;margin-left:5px;" />
		</span>
					
					</div>
				
					<div>
						<input class="radio" type="radio" name="feed_search_type" value="hashtag" id="hashtag"  checked="" style="display:inline-block;">
						<label for="hashtag">Hashtag:</label>
						<input type="text" name="hashtag_val" id="hashtag_val" placeholder="Eg: adidas" style="width:65%;height: 32px;"><br>
					
				   </div>
				   
				   
				   </div>
				   <label for="comman display">Display comman:</label>
				   <input  type="checkbox" id="eif_display_comman" name="eif_display_comman" >
				   <div>
				   
				</div>
		        
				<div style="margin-bottom:15px;">
					<label for="feed_cols" >Number of columns:</label>
					
						<select id="feed_cols" name="feed_cols" style="margin-left:10px;">
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4" selected="">4</option>
							<option value="5">5</option>
							<option value="6">6</option>
						</select>
			
				</div>
				<div style="margin-bottom:15px;">
					<label for="padding">Image Padding:</label>
					<input type="text" name="padding_img" id="padding_img"  style="width: 50px;height: 30px;">
					<select id="padding_img_unit" name="padding_img_unit" style="height:30px;margin-left:10px;">
						<option value="%">%</option>
						<option value="px" selected="">px</option>
					</select>
				</div>
			
				

					<input type="submit" id="searchsubmit" value="View Feed" tabindex="11" style="float: left;">
					<!--<a href="http://webriti.com/instagramuserid/instagramuserid.php" target="_blank" style="
    margin-left: 69px;
    text-decoration: underline;
">GET YOUR USER ID</a>-->
				   
	
	</form>
</div>
</div></div>	

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
 
	    <script type="text/javascript">
		//Select a type
		
		
		$('input[name=eif_display_comman]').click(function(){
      $(this).val(this.checked ? 1:0);
		});
		
		$('input[name=eif_filter]').click(function(){
      $(this).val(this.checked ? 1:0);
		});
		
		var feed_type = $('input:radio[name=feed_search_type]:checked').val();
		if(feed_type !='user'){
			jQuery('#userhashtagsection').stop(true,true).hide();
		}
		
		 $(".radio").change(function (){ 
        if (this.value == "user"){ 
           $('#userhashtagsection').stop(true,true).show();
		 
        } else {
            $('#userhashtagsection').stop(true,true).hide();
			
        }
    });

		//jQuery(document).ready(function($) {
	    	var feed_type = $('input:radio[name=feed_search_type]:checked').val();
			
	    	$('input:radio[name=feed_search_type]').on('change', function(){
	    		feed_type = $(this).val();
				
	    	});
			
			$('#form').submit(function(){
	        	var eif_user_id = $('#user_id').val(),
	        		eif_hashtag = $('#hashtag_val').val(),
					filter = $('#eif_filter').val(),
					display_comman  = $('#eif_display_comman').val(),
					
					tagged_by = $('#eif_filter_tagged_by').val().toLowerCase(),
					
	        		feed_cols = $('#feed_cols').val();
					img_padding = $('#padding_img').val();
					img_padding_unit = $('#padding_img_unit').val()
				
	        	while(eif_hashtag.charAt(0) === '#')
					    eif_hashtag = eif_hashtag.substr(1);
				

	        	if( feed_type == 'user' ){
	        		var demo_url = '?user_id=' + eif_user_id + '&cols=' + feed_cols +'&img_padding='+ img_padding +'&img_padding_unit='+img_padding_unit +'&filter='+filter +'&tagged_by='+tagged_by +'&display_comman='+display_comman;
					
	        	} else {
	        		var demo_url = '?hashtag=' + eif_hashtag + '&cols=' + feed_cols +'&img_padding='+ img_padding +'&img_padding_unit='+img_padding_unit +'&display_comman='+display_comman;
					
	        	}

	            $(this).attr( 'action', demo_url );
	        });

			//Get the parameters from the URL
	        var cur_type = getQueryVariable('type'),
	        	cur_id = getQueryVariable('user_id'),
	        	cur_hashtag = getQueryVariable('hashtag'),
	        	cur_cols = getQueryVariable('cols'),
				cur_padding = getQueryVariable('img_padding'),
				cur_padding_unit = getQueryVariable('img_padding_unit');
				filter = getQueryVariable('filter');
				TaggedBY = getQueryVariable('tagged_by');
				Display_Comman = getQueryVariable('display_comman');
				
			
				
	        if(cur_hashtag !== false)
			{
			 var feed_type = 'hashtag';
		
			}
			else if(cur_id !== false)
			{
			var feed_type = 'user';
			}
			
			
		
			
	        if(cur_cols == false)
			cur_cols = '4';
			if(cur_padding == false)
			cur_padding = '3';
			if(cur_padding_unit == false)
			cur_padding_unit= 'px';

	        if ( feed_type == 'user' ){
				$('#userhashtagsection').stop(true,true).show();
			//alert("user");
	        	$('input:radio[name=feed_search_type]#user').prop('checked', true);
	        	if(cur_id == false) cur_id = '';
	        	$('#user_id').val( cur_id );
				$('#padding_img').val(cur_padding);
				$('#padding_img_unit').val(cur_padding_unit);
				if(filter == 1){
					$('#eif_filter').prop('checked', true);
					$('#eif_filter').val(filter);
					$('#eif_filter_tagged_by').val(TaggedBY);
					}
	        } else if ( feed_type == 'hashtag' ){
	
	        	$('input:radio[name=feed_search_type]#hashtag').prop('checked', true);
	        	if(cur_hashtag == false) cur_hashtag = 'adidas';
	        	$('#hashtag_val').val( cur_hashtag );
				$('#padding_img').val(cur_padding);
				$('#padding_img_unit').val(cur_padding_unit);
	        }			//Set the cols
	        $('#feed_cols').val( cur_cols );
	
			if(Display_Comman == 1){
					$('#eif_display_comman').prop('checked', true);
					$('#eif_display_comman').val(Display_Comman);
					}

	        //Used to get the URL params
	        function getQueryVariable(variable){
		       var query = window.location.search.substring(1);
		       var vars = query.split("&");
		       for (var i=0;i<vars.length;i++) {
		               var pair = vars[i].split("=");
		               if(pair[0] == variable){return pair[1];}
		       }
		       return(false);
			}

			//});
			
			
(function($){
var string_hash = window.location.hash;
var token = string_hash.substring(14);
if(token!='')
{
	



	        	var eif_user_id = token,
	        		
				
	        		feed_cols = $('#feed_cols').val();
					img_padding = $('#padding_img').val();
					img_padding_unit = $('#padding_img_unit').val();
					
				
	        	
				

	        
	        		var demo_url = '?user_id=' + eif_user_id + '&cols=' + feed_cols +'&img_padding='+ img_padding +'&img_padding_unit='+img_padding_unit;
					
	        	//alert(demo_url);
	        	window.location.replace("https://webriti.com/easy-instagram-feed-demo/" + demo_url);

	            //$(this).attr( 'action', demo_url );
	      


}




})(jQuery);
			
		</script>
</div>
<div class="clear"></div>
</div>
<?php get_footer();?>