<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<!--[if IE]>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">``
	<![endif]-->
	<meta charset="<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />    
	<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" />	
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.ico" />
	<link href="<?php echo get_stylesheet_uri(); ?>" rel="stylesheet" />	
	<?php wp_head(); ?>
</head>
<body <?php  if(is_page_template('blog.php') || is_single()) { body_class('blog_bg'); } else { body_class(); }?>>

<!--Top Infobar-->
<!--<div id="webriti_infobar">
	<a href="http://webriti.com/all-theme-package/">
		<div class="themeshop_infobar">
			<p class="themeshop_text">
				<span class="infobar_notice">
					<span class="font-pxnsmbld"></span> 
					<span class="font-pxnlt">Get all webriti themes</span> 
					<span class="font-pxnsmbld">in just $99</span>&nbsp;
					<span class="themeshop_click ibtn yellow small">Join The Club</span>
				</span>
			</p>
		</div>	
	</a>
</div>
<br /><br /><br />-->
<!--/Top Infobar-->

<nav class="navbar navbar-inverse" role="navigation">
      <div class="container">
	  <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-9">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo home_url( '/' ); ?>"><img src="<?php echo get_template_directory_uri() ?>/images/logo.png" alt="webriti" title="Webriti Themes"></a>
        </div>
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-9">
		<?php
		wp_nav_menu( array(  
				'theme_location' => 'primary',
				//'container'  => 'nav-collapse collapse navbar-inverse-collapse',
				'menu_class' => 'nav navbar-nav navbar-right',
				'fallback_cb' => 'webriti_fallback_page_menu',
				'walker' => new webriti_nav_walker()
				)
			);	
		?>
		</div>
		</div><!-- /.container-fluid -->
</nav>