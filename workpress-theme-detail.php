<?php //Template Name: Workpress-Theme-Detail ?>
<?php get_header(); ?>
<!-- Homepage Slider Section -->
<div class="themedetail_main_slider">
	<div class="container">
		<div class="row">
			
			<div class="col-md-7">
				<a href="#"><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/workpress_theme_slide.png"></a>
			</div>
			
			<div class="col-md-5 themedetail_data">
				<h2>Workpress - Pro</h2>
				<p>Workpress is a very simple WordPress theme for your personal websites. It is developed on the Bootstrap 3 framework - amazing and beautiful HTML, CSS, and JS framework.</p>
				<div class="themedetail_btntop">
                    <a class="themedetail_slide_btn" target="_blank" href="http://webriti.com/demo/wp/preview/?prev=workpress/">View Demo</a><span>or</span>
					<a class="buy_theme_btn" href="#myModal"  data-toggle="modal" style="color:#fff">Buy Now</a>
                </div>
			</div>
			
		</div>
	</div>
</div>
<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery(".close").click(function(){
				jQuery("#myModal").hide();
			});
		});
	</script>
	<style>.homebtn {
		padding: 5px 22px !important;
		font-family: 'OpenSansBold';
		margin-bottom: 7px !important;
		color: hsl(0, 100%, 100%);
		text-shadow: none;
		background-color: hsl(0, 81%, 44%);
		background-repeat: repeat-x;
		border-color: hsl(103, 50%, 54%) hsl(103, 50%, 54%) hsl(0, 0%, 18%);
		border-color: hsla(0, 0%, 0%, 0.1) hsla(0, 0%, 0%, 0.1) hsla(0, 0%, 0%, 0.25);
		display: inline-block;
		text-align: center;
		text-decoration: none;
		vertical-align: middle;
		cursor: pointer;
		font-size: 18px;
		line-height: 30px;
		border: 1px solid hsl(0, 0%, 80%);
		border-color: hsl(0, 0%, 90%) hsl(0, 0%, 90%) hsl(0, 0%, 75%);
		border-color: hsla(0, 0%, 0%, 0.1) hsla(0, 0%, 0%, 0.1) hsla(0, 0%, 0%, 0.25);
		border-bottom-color: hsl(0, 0%, 70%);
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		border-radius: 4px;
		-webkit-box-shadow: inset 0 1px 0 hsla(0, 100%, 100%, 0.2), 0 1px 2px hsla(0, 0%, 0%, 0.05);
		-moz-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
		box-shadow: inset 0 1px 0 hsla(0, 100%, 100%, 0.2), 0 1px 2px hsla(0, 0%, 0%, 0.05);
		}
		.homebtn:hover{ text-decoration:none; color:#fff;}
	</style>
<div id="myModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="background:transparent;border:0px;display:none;">
		<div class="modal-header" style="border:0px;">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true" ><img src="<?php echo get_stylesheet_directory_uri().'/images/close_256.png'?>" /></button>
    		<h4 id="myModalLabel"></h4>
  		</div>
  		<div class="modal-body" align="center">
  			<div class="spasalon-pro-modal" style="background:#FFFFFF; width:290px;border:2px solid #ddd;webkit-box-shadow: 0 5px 15px rgba(0,0,0,.5);box-shadow: 0 50px 50px rgba(0,0,0,0.5);" >
  				<button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-right: -19px;
margin-top: -16px; opacity:1"><img src="<?php echo get_template_directory_uri(); ?>/images/close_256.png"></button>
				<div id="title"><h4 style="background:#6BB3D1;margin-top:0%;padding-bottom:25px;padding-top:7px;color:#FFFFFF;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;">Workpress-Pro</h4>
  					<div class="rate" style="display: block;font: bold 25px/62px Georgia, Serif;color: #777;
background: #CFEEFC;border: 5px solid #fff;margin: -31px auto 5px;
-moz-border-radius: 100px;-webkit-border-radius: 100px;border-radius: 5200px;-moz-box-shadow: 0 5px 20px #ddd inset, 0 3px 0 #999 inset;-webkit-box-shadow: 0 5px 20px #ddd inset, 0 3px 0 #999 inset;box-shadow: 0 5px 20px #ddd inset, 0 3px 0 #999 inset;">
                        <span>$</span><strong>49.99</strong>
                     </div>
    			</div>
   				<h5 style="color:#F22853;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;">KEY FEATURES</h5>
                <table class="table table-hover" >
                    <tr ><td style="text-align:center;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:14px;">
                   <strong> Responsive Design</strong>
                    </td></tr>
                    <tr><td style="text-align:center;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:14px;">
                    <strong>08 Page Templates</strong> 
                    </td></tr>
                    <tr><td style="text-align:center;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:14px;">
                   <strong> Unique & Different Color Scheme </strong>
                    </td> </tr>
                   <tr><td style="text-align:center;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:14px;">
                    <strong>Use theme for Lifetime. </strong>
                    </td></tr>
                    <tr><td style="text-align:center;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:14px;">
                    <strong>1 year of Support Access </strong>
                    </td></tr>
					<tr><td style="text-align:center;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:14px;">
                    <strong>1 year of Theme Updates </strong>
                    </td></tr>
                </table>
    			<center>
    			<a href="http://www.webriti.com/amember/signup/workpress"  class="homebtn"><strong>Buy Now</strong></a>
    			</center><br />
    		</div>
  		</div> 
	</div>
<div class="homepage_main_slide_shadow"></div>
<!-- /Homepage Slider Section -->
<!-- Theme Features Section -->
<div class="container">
	
	<div class="row">
		<div class="themedetail_heading_title">
			<h2>Our Unique Theme Features</h2>
			<div id="" class="themedetail_separator"></div>
		</div>
	</div>
	
	<div class="row">
		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-tablet"></i>
				</div>
				<div class="media-body">
					<h3>Reponsive Layout</h3>
					<p>Our all Themes are Mobile friendly and easily adapts the various screen sizes.</p>
				</div>
			</div>
		</div>		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-cog"></i>
				</div>
				<div class="media-body">
					<h3>Theme Options</h3>
					<p>Theme provides Theme Options Panel, for customizing the the theme.</p>
				</div>
			</div>
		</div>		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-thumbs-o-up"></i>
				</div>
				<div class="media-body">
					<h3>Friendly Support</h3>
					<p>Our great support team is ready to help. Our clients are valuable for us.</p>
				</div>
			</div>
		</div>		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-language"></i>
				</div>
				<div class="media-body">
					<h3>Translation Ready</h3>
					<p>Themes our translation ready you can translate theme in your own language.</p>
				</div>
			</div>
		</div>		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-globe"></i>
				</div>
				<div class="media-body">
					<h3>Browser Compatibility</h3>
					<p>Themes our cross browser compatible. Theme supports all modern browser. </p>
				</div>
			</div>
		</div>		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-file-code-o"></i>
				</div>
				<div class="media-body">
					<h3>Font Awesome Icons</h3>
					<p>Easily style icon color, size, shadow, and anything that's possible with CSS.</p>
				</div>
			</div>
		</div>		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-sliders"></i>
				</div>
				<div class="media-body">
					<h3>Custom Widgets</h3>
					<p>Theme has custom widgets to add in sidebar.</p>
				</div>
			</div>
		</div>		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-file"></i>
				</div>
				<div class="media-body">
					<h3>Page Templates</h3>
					<p>Theme has 8 page templates. </p>
				</div>
			</div>
		</div>		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-thumbs-o-up"></i>
				</div>
				<div class="media-body">
					<h3>SEO Friendly urls</h3>
					<p>Option provided to rename the custom post types in order to create seo friendly urls.</p>
				</div>
			</div>
		</div>	
	</div>
 </div>
<!-- /Theme Features Section -->
<div class="themedatail_testimonial_section">
	<?php get_template_part('index', 'testimonial'); ?>
</div>

<!--Theme Detail Testimonial Section-
<div class="themedatail_testimonial_section">
	<div class="container">		
		
		<div class="row" id="webriti-testimonial">			
			<div class="col-md-12 themedatail_trestimonial_area pull-left">
				<p>"Webriti themes are elegant, simple & creative. They can fit a wide variety of business fields. As for the technical support, I've received a very quick, efficient and kind attention. Keep up the good work!"</p>
				<div><img class="img-circle testimonial_img" src="<?php // echo get_template_directory_uri() ?>/images/testi1.jpg"></div>
				<h2><i></i>YUNNY ANDRADE<i></i></h2>
			</div>
			
				
		</div>		
	</div>
</div>
<!--/Theme Detail Testimonial Section-->
<!--Theme Detail Image Section -->
<div class="container">
	<div class="row">
		<div class="themedetail_img_heading_title">
			<h2>Our Unique Theme Features</h2>
			<div class="direction_arrow">
				<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/arrow.png">
			</div>
		</div>
	</div>	
	<div class="row">
		<div class="col-md-12 themedetail_image">
			<img class="img-responsive" title="Workpress Theme Detail" src="<?php echo get_template_directory_uri() ?>/images/workpress_themeshop_detail.jpg">
		</div>
	</div>	
</div>
<!-- /Theme Detail Image Section -->
<?php get_footer(); ?>