<?php  //THEME DEFINATIONS
	/**Includes reqired resources here**/
	define('WEBRITI_TEMPLATE_DIR_URI',get_template_directory_uri());
	
	define('WEBRITI_TEMPLATE_DIR',get_template_directory());
	define('WEBRITI_THEME_FUNCTIONS_PATH',WEBRITI_TEMPLATE_DIR.'/inc');
	
	
	require_once( WEBRITI_THEME_FUNCTIONS_PATH .'/menu/default_menu_walker.php');
	require_once(WEBRITI_THEME_FUNCTIONS_PATH . '/menu/webriti_nav_walker.php');
	require_once( WEBRITI_THEME_FUNCTIONS_PATH . '/webriti-sidebar-latest-post.php');
	require_once( WEBRITI_THEME_FUNCTIONS_PATH .'/pagination/webriti_pagination.php');
	require_once( WEBRITI_THEME_FUNCTIONS_PATH . '/comment-function.php');
	require_once( WEBRITI_THEME_FUNCTIONS_PATH . '/webriti-sidebar-latest-postimg.php');
	
function webriti_setup() {
	add_theme_support( 'post-thumbnails' );
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'webriti' ),
	) );
}
add_action( 'after_setup_theme', 'webriti_setup' );

	//wp title tag starts here
	function webriti_title( $title, $sep )
	{	global $paged, $page;		
		if ( is_feed() )
			return $title;
		// Add the site name.
		$title .= get_bloginfo( 'name' );
		// Add the site description for the home/front page.
		$site_description = get_bloginfo( 'description' );
		if ( $site_description && ( is_home() || is_front_page() ) )
			$title = "$title $sep $site_description";
		// Add a page number if necessary.
		if ( $paged >= 2 || $page >= 2 )
			$title = "$title $sep " . sprintf( _e( 'Page', 'quality' ), max( $paged, $page ) );
		return $title;
	}	
	add_filter( 'wp_title', 'webriti_title', 10, 2);
	
	
	
	function webriti_scripts()
	{
	wp_enqueue_style('bootstrap', WEBRITI_TEMPLATE_DIR_URI . '/css/bootstrap.css');
	
	// Merged the following css with style.css for better performance - ANkit & Priyanshu Dt: 14th June 
	wp_enqueue_style('theme-menu', WEBRITI_TEMPLATE_DIR_URI . '/css/theme-menu.css');
	//wp_enqueue_style('font', WEBRITI_TEMPLATE_DIR_URI . '/css/font/font.css');	
	// wp_enqueue_style('media', WEBRITI_TEMPLATE_DIR_URI . '/css/media-responsive.css');

	
	wp_enqueue_style('FA', WEBRITI_TEMPLATE_DIR_URI . '/css/font-awesome-4.0.3/css/font-awesome.min.css');
		
	//JS
	wp_enqueue_script('menu', WEBRITI_TEMPLATE_DIR_URI .'/js/bootstrap.min.js',array('jquery'),'1.0.0',true);
	//wp_enqueue_script('menu-js', WEBRITI_TEMPLATE_DIR_URI .'/js/menu.js');
	 
	 if ( !is_page('easy-instagram-feed-demo') ) {

      wp_enqueue_script('menu-js', WEBRITI_TEMPLATE_DIR_URI .'/js/menu.js');

     }
	 
	wp_enqueue_script('carof', WEBRITI_TEMPLATE_DIR_URI .'/js/caroufredsel/jquery.carouFredSel-6.2.1-packed.js',array(),'1.0.0',true);
	//wp_enqueue_script('menu', WEBRITI_TEMPLATE_DIR_URI .'/js/menu.js',array(),'1.0.0',true);	
	wp_enqueue_script('scroll', WEBRITI_TEMPLATE_DIR_URI .'/js/scroll-top.js',array(),'1.0.0',true);	
	
	}
	add_action('wp_enqueue_scripts', 'webriti_scripts');
	if ( is_singular() ){ wp_enqueue_script( "comment-reply" );	}
	
	// Read more tag to formatting in blog page 
	function _new_content_more($more)
	{  global $post;
	   return '<a href="' . get_permalink() . "#more-{$post->ID}\" class=\"webriti_blog_btn\">Read More</a>";
	}   
	add_filter( 'the_content_more_link', '_new_content_more' );
	
	//Side Bar and Footer Widgets
	add_action( 'widgets_init', 'webriti_widgets_init');
		function webriti_widgets_init() {
		/*sidebar*/
		register_sidebar( array(
				'name' => __( 'Sidebar', 'webriti' ),
				'id' => 'sidebar-primary',
				'description' => __( 'The primary widget area', 'webriti' ),
				'before_widget' => '<div class="webriti_sidebar_widget" >',
				'after_widget' => '</div>',
				'before_title' => '<div class="webriti_sidebar_widget_title"><h2>',
				'after_title' => '</h2><div class="webriti-separator-small"></div></div>',
			) );

		register_sidebar( array(
				'name' => __( 'Footer Widget Area', 'webriti' ),
				'id' => 'footer-widget-area',
				'description' => __( 'footer widget area', 'webriti' ),
				'before_widget' => '<div class="col-md-3 col-sm-6 footer_widget_column">',
				'after_widget' => '</div>',
				'before_title' => '<h3 class="footer_widget_title">',
				'after_title' => '<div id="" class="qua-footer-separator"></div></h3>',
			) );
		}
		
		//Gravtar's Class
		add_filter('get_avatar','add_gravatar_class');

		function add_gravatar_class($class) {		
		$class = str_replace("class='avatar", "class='img-circle blog_auther_img", $class);
		return $class;
		}
		
	/* 	//Comment  -- By Ankit
		function get_avatar_url($author_id, $size){
			$get_avatar = get_avatar( $author_id, $size );
			preg_match("/src='(.*?)'/i", $get_avatar, $matches);
			return ( $matches[1] );
		} */
		
		//IMG RESIZE
		if ( function_exists( 'add_image_size' ) ) 
		{ 
		add_image_size('bloggg',70,70,true);
		}
		
		add_filter( 'intermediate_image_sizes', '_image_presets');
		function _image_presets($sizes){
		if(isset($_REQUEST['post_id'])):
		$type = get_post_type($_REQUEST['post_id']);	
		foreach($sizes as $key => $value){
    	
		 if($type=='post' && $value != 'bloggg' )
		 { unset($sizes[$key]); }
				
		}
		endif;
		return $sizes;	 
		}
		
	/*--------------------------------------------------------------------*/
	/*     Register Google Fonts
	/*--------------------------------------------------------------------*/
	function themeshop_fonts_url() {
		
		$fonts_url = '';
			
		$font_families = array();
	 
		$font_families = array('Titillium Web:300,400,600,700,800','italic');
	 
			$query_args = array(
				'family' => urlencode( implode( '|', $font_families ) ),
				'subset' => urlencode( 'latin,latin-ext' ),
			);
	 
			$fonts_url = add_query_arg( $query_args, '//fonts.googleapis.com/css' );

		return $fonts_url;
	}
	function themeshop_scripts_styles() {
		wp_enqueue_style( 'themeshop-fonts', themeshop_fonts_url(), array(), null );
	}
	add_action( 'wp_enqueue_scripts', 'themeshop_scripts_styles' );
		
?>