<?php //Template Name: SEO Optimized Images ?>
<?php get_header(); ?>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="https://checkout.freemius.com/checkout.min.js"></script>
<!-- Homepage Slider Section -->
<div class="themedetail_main_slider">
	<div class="container">
		<div class="row">
			
			<div class="col-md-7">
				<a href=""><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/seo.png"></a>
			</div>
			
			<div class="col-md-5 themedetail_data">
				<h2>Seo Optimized Images</h2>
				<p>Automatically add optimized TITLE and ALT tags to the images and get more traffic from search engines.</p>
				<div class="themedetail_btntop">
                	<a class="themedetail_slide_btn" id="scroll_pricing" href="#pricing">Buy Now</a><span>or</span>
                    <a class="buy_theme_btn" href="http://webriti.com/seo-optimized-images-demo/">View Demo</a>
                </div>
			</div>
			
			
		</div>
	</div>
</div>

<div class="homepage_main_slide_shadow"></div>

<!-- /Homepage Slider Section -->

<!-- Why use Seo Optimized Images Plugin -->

<div class="container">
	
	<div class="row">
		<div class="themedetail_heading_title">
			<h2>Why use Seo Optimized Images Plugin</h2>
			<div id="" class="themedetail_separator"></div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 theme_feature_area">
			<div class="media">
				<div class="media-body">
				<p>Creating optimized alt tags is a great way to <b>increase traffic from search engines</b>. If you have an existing blog with a lot of images then adding alt tags manually can be next to impossible.</p> 

<p>Using the SEO optimized Images plugin you can <b>automatically add optimized alt and title tags to your images</b> in less than 5 minutes. All you need to do is activate the plugin and it will automatically add effective alt tags to all the images.</p>  

<p>The plugin will <b>add alt tags to all the existing images as well as newly uploaded images.</b></p><p>

If you want to remove the alt tags simply deactivate the plugin and the alt tags added by the plugin will be removed. The plugin does not create additional database entries hence it is light to use and does not increase the site load time. </p>
				</div>
			</div>
		</div>
	</div>
 </div>

<!-- /Why use Seo Optimized Images Plugin -->


<!-- Plugin Features Section -->
<div class="container">
	
	<div class="row">
		<div class="themedetail_heading_title">
			<h2>Seo Optimized Images Features</h2>
			<div id="" class="themedetail_separator"></div>
		</div>
	</div>
	
	<div class="row">
		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-compress"></i>
				</div>
				<div class="media-body">
					<h3>Automatically add alt & title tags</h3>
					<p>This Plugin automatically updates all images with proper ALT and TITLE attributes for SEO purposes</p>
				</div>
			</div>
		</div>
		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-search"></i>
				</div>
				<div class="media-body">
					<h3>Works for exsisting and new images</h3>
					<p>Add alt & title tags to existing images as well as newly uploaded images.</p>
				</div>
			</div>
		</div>
		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-bar-chart"></i>
				</div>
				<div class="media-body">
					<h3>Feature Images Support</h3>
					<p>Add alt & title tags to featured images.

</p>
				</div>
			</div>
		</div>
		
		<div class="clearfix"></div>
		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-rocket"></i>
				</div>
				<div class="media-body">
					<h3>Woocommerce Images Support</h3>
					<p>Add alt & title tags to Woo-Commerce product images.<p>
				</div>
			</div>
		</div>
		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-get-pocket"></i>
				</div>
				<div class="media-body">
					<h3>Customizable alt and title tags</h3>
					<p>Add tags based on post title, image file name, post category and tags.</p>
				</div>
			</div>
		</div>
		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-users"></i>
				</div>
				<div class="media-body">
					<h3>Custom Rules</h3>
					<p>Selectively add alt and title tags to images based on custom rules.</p>
				</div>
			</div>
		</div>
		
		<div class="clearfix"></div>
		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-file-text-o"></i>
				</div>
				<div class="media-body">
					<h3>Page Builders</h3>
					<p>Support for Popular page builders like elementor, site origin, visual composer, beaver builder etc etc.<p>
				</div>
			</div>
		</div>
		
	</div>
 </div>
<!-- /Plugin Features Section -->

<!-- Pricing Section -->
<div class="pricing" id="pricing">
	<div class="overlay">
		<div class="container">
		
			<div class="row">
				<div class="themedetail_heading_title">
					<h2>Choose Your Plan & Buy</h2>
					<div id="" class="themedetail_separator"></div>
          <p style="text-align: center; margin-top: 10px;"><a href="#reviews" style="">Learn what others are saying about us.</a></p>
				</div>
			</div>
			
			<div class="row package">
				<div class="col-sm-8 col-sm-offset-2">
					<div class="col-md-6 col-xs-12">
						<div class="single_package">
							<h4>$69</h4>
							<div class="package_headwrap">                                
								<h3 style="margin-top:0px;">Business</h3>
								<p>For Optimizing Images on a WordPress site.</p>
							</div>
							<div class="package_dis">
								<ul class="package_list">
									<li>
										<i class="fa fa-question-circle green" data-toggle="tooltip" data-placement="right" title="You can use for Unlimited Site"></i> Single Site License
									</li>
									<li>
										<i class="fa fa-check green"></i>
											Support for popular page builders like Elementor, Site Origin etc
										<span style="background: #ff0000;border-radius: 2px;font-size: 10px;line-height: 17px;font-family: 'OpenSansBold';
										text-transform: uppercase;color: #ffffff;display: inline-block;vertical-align: text-bottom;margin-left: 3px;letter-spacing: 1px;font-weight: 700;padding: 0px 5px;">New</span>
									</li>
									<li>
										<i class="fa fa-check green"></i> Automatically update ALT and TITLE Tags
									</li>
									<li>
										<i class="fa fa-check green"></i> <b>Featured Images</b> Support	
									</li>
										<li>
										<i class="fa fa-check green"></i> Support for<b> Custom Post Type Images</b>	
									</li>
									<li>
									<i class="fa fa-times red"></i><b>WooCommerce</b> Product Images Support</b>
									</li>
									<li>
										<i class="fa fa-check green"></i> Add alt and title tags to images based on <b>Custom Rules</b>
									</li>
									<li>
										<i class="fa fa-check green"></i> 1 Year support and updates.
									</li>
								</ul>
								<div class="plan_button">
									<a id="buy-now-biz" class="theme_btn detail_btn">Buy Now</a>
                </div>
							</div>                  
						</div>
					</div>
				
				
					<div class="col-md-6 col-xs-12">           
						<div class="single_package active">
							<div class="ribbon">
								<h5 class="popular">Popular</h5>
							</div>
							<h4>$89</h4>
							<div class="package_headwrap">                                
								<h3 style="margin-top:0px;">Business with WooCommerce</h3>
								<p>For Optimizing Images on a WordPress site.</p>
							</div>
							<div class="package_dis">                  
								<ul class="package_list">
									<li>
										<i class="fa fa-question-circle green" data-toggle="tooltip" data-placement="right" title="You can use for Unlimited Site"></i> Single Site License
									</li>
									<li>
										<i class="fa fa-check green"></i>
											Support for popular page builders like Elementor, Site Origin etc
										<span style="background: #ff0000;border-radius: 2px;font-size: 10px;line-height: 17px;font-family: 'OpenSansBold';
										text-transform: uppercase;color: #ffffff;display: inline-block;vertical-align: text-bottom;margin-left: 3px;letter-spacing: 1px;font-weight: 700;padding: 0px 5px;">New</span>
									</li>
									<li>
										<i class="fa fa-check green"></i> Automatically update ALT and TITLE Tags
									</li>
									<li>
										<i class="fa fa-check green"></i> <b>Featured Images</b> Support	
									</li>
										<li>
										<i class="fa fa-check green"></i> Support for<b> Custom Post Type Images</b>	
									</li>
									<li>
										<i class="fa fa-check green"></i> <b>WooCommerce</b> Product Images Support
									</li>
									<li>
										<i class="fa fa-check green"></i> Add alt and title tags to images based on <b>Custom Rules</b>
									</li>
									<li>
										<i class="fa fa-check green"></i> 1 Year support and updates.
									</li>
									
									
								</ul>
								<div class="plan_button">
                  
									<a id="buy-woo-button" class="theme_btn demo_btn">Buy Now</a>
 <script>
    // trigger checkout pop up correspnding to biz plan
    $('#buy-now-biz').on('click', function (e) {
    var handler = FS.Checkout.configure({
        plugin_id:  '11446',
        plan_id:    '19501',
        public_key: 'pk_6e46acfd96081d4a426676bda6038',
        image:      'https://your-plugin-site.com/logo-100x100.png'
    });
        handler.open({
            name     : 'Seo Optimized Images Pro',
            licenses : 1,
            // You can consume the response for after purchase logic.
            purchaseCompleted  : function (response) {
                // The logic here will be executed immediately after the purchase confirmation.                                // alert(response.user.email);
            },
            success  : function (response) {
                // The logic here will be executed after the customer closes the checkout, after a successful purchase.                                // alert(response.user.email);
            }
        });
        e.preventDefault();
    });
    // trigger checkout pop up correspnding to woo plan
    $('#buy-woo-button').on('click', function (e) { 
    var handler = FS.Checkout.configure({
        plugin_id:  '11446',
        plan_id:    '19458',
        public_key: 'pk_6e46acfd96081d4a426676bda6038',
        image:      'https://your-plugin-site.com/logo-100x100.png'
    });
        handler.open({
            name     : 'Seo Optimized Images Pro',
            licenses : 1,
            // You can consume the response for after purchase logic.
            purchaseCompleted  : function (response) {
                // The logic here will be executed immediately after the purchase confirmation.                                // alert(response.user.email);
            },
            success  : function (response) {
                // The logic here will be executed after the customer closes the checkout, after a successful purchase.                                // alert(response.user.email);
            }
        });
        e.preventDefault();
    });
</script>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
 </div>
<!-- /Pricing Section -->

<!-- Theme Features Section -->
<div class="question_section">
	<div class="container">
		
		<div class="row">
			<div class="themedetail_heading_title">
				<h2>Got Questions..? Well We've Got Answers!</h2>
				<div id="" class="themedetail_separator"></div>
			</div>
		</div>
		
		<div class="row">
					
			<div class="col-sm-6">
				
				<h3>Do I need coding skills to use Seo Optimized Images Plugins?</h3>
				<p>Absolutely not. You just need to activate the plugin and configure the alt and title attributes.</p>
				
				<h3>I have some Pre Sales queries. How do I contact you?</h3>
				<p>Click <a href="http://webriti.com/contact-us/" title="Contact Here">here</a> to contact us with Pre Sales queries.</p>
				
				<h3>Do you offer Refunds?</h3>
				<p> We have 15 days money back gaurantee. If you are not satisied with the product, than simply send us an email and we will refund your purchase right away. Our goal has always been to create a happy, thriving community. </p>
				
			</div><!-- /.col -->
					
			<div class="col-sm-6">
				
				<h3>How is support provided?</h3>
				<p>For support simple contact us via this <a href="https://users.freemius.com/login" target="_blank" title="Contact Here">form</a></p>
				
				<h3>What types of payment methods are accepted?</h3>
				<p>Currently we accept Paypal and Stripe as a payment gateways. You can directly pay via Credit / Debit Card, it is not mandatory for you to have PayPal account.</p>
				
				</div><!-- /.col -->
						
		</div>
	</div>
 </div>
<!-- /Theme Features Section -->

<!--Plugin Detail Testimonial Section-->
<?php get_template_part('index', 'seo-testimonial'); ?>
<!--/Plugin Detail Testimonial Section-->
<?php get_footer(); ?>