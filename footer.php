<!-- Footer Widget Secton -->
<div class="footer_widget_area">	
	<div class="container">
		<div class="row">
			<?php if ( is_active_sidebar( 'footer-widget-area' ) )
			{ ?>
			<div class="">
				<?php dynamic_sidebar( 'footer-widget-area' ); ?>
			</div>	
			<?php } else { ?> 
			<div class="col-md-3 col-sm-6 footer_widget_column">		
				<p class="footer_logo"><a href="index.html"><img title="Webriti Themes" src="<?php echo get_template_directory_uri() ?>/images/footer_logo.png"></a></p>
				<p>Webriti is a developer of Premium Wordpress Themes. We create theme which are beautiful, functional and flexible And yes, we also provide Awesome Support!!</p>
			</div>
			
			<div class="col-md-3 col-sm-6 footer_widget_column">		
				<h3 class="footer_widget_title">
					Support
					<div class="qua-footer-separator" id=""></div>
				</h3>
				<div class="footer_widget_link">
					<a href="#">Support Forums</a>
					<a href="#">Pre Sales Enquiry</a>
					<a href="#">Terms and Conditions</a>
					<a href="#">FAQ</a>
				</div>
			</div>
			
			<div class="col-md-3 col-sm-6 footer_widget_column">		
				<h3 class="footer_widget_title">
					Blog
					<div class="qua-footer-separator" id=""></div>
				</h3>
							
				<div class="media footer_widget_post">
					
					<div class="media-body">
						<h3><a href="#">WordPress.com vs. WordPress.org [Self hosted WordPress]</a></h3>
						<span class="footer_widget_date">Written on Monday, 06 January 2014 00.00 </span>
					</div>
				</div>
				
				<div class="media footer_widget_post">
					
					<div class="media-body">
						<h3><a href="#">WordPress.com vs. WordPress.org [Self hosted WordPress]</a></h3>
						<span class="footer_widget_date">Written on Monday, 06 January 2014 00.00 </span>
					</div>
				</div>
				
			</div>
			
			<div class="col-md-3 col-sm-6 footer_widget_column">		
				<h3 class="footer_widget_title">
					Webriti
					<div class="qua-footer-separator" id=""></div>
				</h3>
				<div class="footer_widget_link">
					<a href="#">Themes</a>
					<a href="#">Blog</a>
					<a href="#">Plugin</a>
					<a href="#">Contact</a>
				</div>
			</div>
			<?php } ?>
		</div>
	
	</div>
	
</div>
<div class="footer_area">
	<div class="container">
		<div class="col-md-12">
		<p>Powered by WordPress © <a href="http://www.webriti.com">Webriti 2020</a></p>
		</div>	
	</div>	
</div>
<a href="#" class="page_scrollup"><i class="fa fa-chevron-up"></i></a>
<?php wp_footer(); ?>
<!-- /Footer Widget Secton -->
</body>
</html>