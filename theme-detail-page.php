<?php 
//Template Name: Theme Detail Page Template
get_header(); ?>
<!-- Page Title Section -->
<div class="container">
	<div class="row">
		<div class="webriti_page_heading">
			<?php the_post(); ?>
			<h1><?php the_title(); ?><span></span></h1>
			<p></p>
			<div class="page_separator" id=""></div>
		</div>
	</div>
</div>
<!-- /Page Title Section -->
<!-- Blog & Sidebar Section -->
<div class="container">
<div class="row">	
	<!--Blog Area-->
	<div class="col-md-12">	
	<div class="webriti_blog_section">
		<div class="clear"></div>
		<?php $defalt_arg =array('class' => "img-responsive"); ?>
		<?php if(has_post_thumbnail()): ?>
		<div class="webriti_blog_post_img">
			<a  href="<?php the_permalink(); ?>">
				<?php the_post_thumbnail('', $defalt_arg); ?>
			</a>					
		</div>
		<?php endif; ?>	
		<div class="webriti_blog_post_content">
			<?php the_content( __( 'Read More' , 'webriti' ) );  ?>
		</div>	
	</div>
	<?php comments_template('',true); ?>
	</div>
	
</div>
</div>
<?php get_footer(); ?>