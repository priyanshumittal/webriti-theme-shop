<?php //Template Name: Webriti Installation Service ?>
<?php get_header(); ?>
<!-- Homepage Slider Section -->
<div class="themedetail_main_slider">
	<div class="container">
		<div class="row">
			
			
			
			<div class="col-md-12" style="text-align:center;margin-top:0px;padding-bottom:25px;">
				<h2>WordPress Theme Installation Service</h2>				
			</div>
			
			
		</div>
	</div>
</div>

<div class="homepage_main_slide_shadow"></div>

<!-- /Homepage Slider Section -->

<!-- Why use Seo Optimized Images Plugin -->

<div class="container">
	
	<div class="row">
		<div class="themedetail_heading_title">
			<h2>Get your WordPress website up and running in no time.</h2>
			<div id="" class="themedetail_separator"></div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 theme_feature_area">
			<div class="media">
				<div class="media-body">
				<p>WordPress is great, but setting up a new theme can be tricky. Want to create a website but don’t have enough time for it? Let’s make a deal. Our WordPress service gives you a simple and quick way of getting WordPress setup and your theme setup without hassle. We have created a service package that gives you everything you need to have your theme and your content setup in no real time.</p>
				<p>Buy one of our themes, and choose a service plan listed below and let us handle the installation part and meanwhile create your actual content.</p> 
				</div>
			</div>
		</div>
	</div>
 </div>

<!-- /Why use Seo Optimized Images Plugin -->
<!-- Pricing Section -->
<div class="pricing" id="pricing">
	<div class="overlay">
		<div class="container">
		
			<div class="row">
				<div class="themedetail_heading_title">
					<h2>Choose Your Service Package</h2>
					<div id="" class="themedetail_separator"></div>
				</div>
			</div>
			
			<div class="row package">
				<div class="col-sm-8 col-sm-offset-2">
					<div class="col-md-6 col-xs-12">
						<div class="single_package">
							<h4>$49</h4>
							<div class="package_headwrap">                                
								<h3 style="margin-top:0px;">Starter Package</h3>
								<p>For Theme Installation.</p>
							</div>
							<div class="package_dis">
								<ul class="package_list">
									<li>
										<i class="fa fa-check green"></i> Theme Install
									</li>
									<li>
										<i class="fa fa-check green"></i>Add sample content	
									</li>
										<li>
										<i class="fa fa-check green"></i> Logo Upload	
									</li>
									<li>
									<i class="fa fa-check green"></i>Removing footer credits
									</li>
									<li>
										<i class="fa fa-check green"></i> Install required plugins
									</li>
									<li>
										<i class="fa fa-check green"></i> Add sample navigation menu
									</li>
									<li>
									<i class="fa fa-times red"></i>WordPress Install
									</li>
								</ul>
								<div class="plan_button">
									<a class="theme_btn detail_btn" href="http://www.webriti.com/amember/signup/starter-package">Buy Now</a>
								</div>
							</div>                  
						</div>
					</div>
				
				
					<div class="col-md-6 col-xs-12">           
						<div class="single_package active">
							<div class="ribbon">
								<h5 class="popular">Popular</h5>
							</div>
							<h4>$89</h4>
							<div class="package_headwrap">                                
								<h3 style="margin-top:0px;">Advanced Package</h3>
								<p>For WordPress and Theme Installation.</p>
							</div>
							<div class="package_dis">                  
								<ul class="package_list">
									<li>
									<i class="fa fa-check green"></i>WordPress Installation
									</li>
									<li>
										<i class="fa fa-check green"></i> Theme Install
									</li>
									<li>
										<i class="fa fa-check green"></i>Add sample content	
									</li>
										<li>
										<i class="fa fa-check green"></i> Logo Upload	
									</li>
									<li>
									<i class="fa fa-check green"></i>Removing footer credits
									</li>
									<li>
										<i class="fa fa-check green"></i> Install required plugins
									</li>
									<li>
										<i class="fa fa-check green"></i> Add sample navigation menu
									</li>									
								</ul>
								<div class="plan_button">
									<a class="theme_btn demo_btn" href="http://www.webriti.com/amember/signup/advanced-package">Buy Now</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
 </div>
<!-- /Pricing Section -->

<!-- Theme Features Section -->
<div class="question_section">
	<div class="container">
		<div class="row">
		<div class="themedetail_heading_title">
				<h2>What we offer?</h2>
				<div id="" class="themedetail_separator"></div>
			</div>
		<p>Our service includes WordPress installation, our theme installation, plugins of your choice, demo data, setting up theme options, adding the logo, posts, pages, widgets etc as per the installation package you select above.
<br>If you have yet to decide on a host for your WordPress site, we recommend <a href="https://www.bluehost.com/track/webriti/" target="_blank"><b>Bluehost hosting service</b></a> which makes your website faster, safer, and better supported than anywhere else.</p>
		</div><p></p>
		<div class="row"> 
		<h3>How It Works?</h3>
		<p>Once you purchase any of above installation services, <a href="http://webriti.com/installation-claim-form/" target="_blank"><b>claim your installation</b></a> by sending your website appropriate login details. </p>
		<h3 id="notice">Important Notes</h3>
		<ul class="theme_install_point">
			<li>
				<p><i class="fa fa-check green"></i>Firstly you have to purchase the theme seperately, service package includes installation not the theme.</p>
			</li>

			<li>
				<p><i class="fa fa-check green"></i>The service package is only for single domain.</p>
			</li>

			<li>
				<p><i class="fa fa-check green"></i>Logo designing is not the part of any service, logo will be provided by you.</p>
			</li>
				
			<li>
				<p><i class="fa fa-check green"></i>Purchasing one of the above packages does not let you any priority support. To get help after the installation has been completed visit this link and we’ll help you out.</p>
			</li>																		
		</ul>
		</div>
	</div>
 </div>
<!-- /Theme Features Section -->

<!--Plugin Detail Testimonial Section-->
<?php get_template_part('index', 'seo-testimonial'); ?>
<!--/Plugin Detail Testimonial Section-->
<?php get_footer(); ?>
