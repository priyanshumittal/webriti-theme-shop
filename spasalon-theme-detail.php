<?php //Template Name: SpaSalon-Theme-Detail ?>
<?php get_header(); ?>
<!-- Homepage Slider Section -->
<div class="themedetail_main_slider">
	<div class="container">
		<div class="row">
			
			<div class="col-md-7">
				<a href=""><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/theme_detail.png"></a>
			</div>
			
			<div class="col-md-5 themedetail_data">
				<h2>SpaSalon - Pro</h2>
				<p>A responsive theme for SPA SALON and BEAUTY SALON type of business that uses multiple nav menus, Right-sidebar, Featured Slider and Beautifully designed home page all manage via option panel.</p>
				<div class="themedetail_btntop">
                    <a class="themedetail_slide_btn" target="_blank" href="https://spasalon-pro.webriti.com/">View Demo</a><span>or</span>
					<a class="buy_theme_btn spasalon_pro-freemius" style="color:#fff">Buy Now</a>
                </div>
			</div>
			
		</div>
	</div>
</div>

<div class="homepage_main_slide_shadow"></div>
<!-- /Homepage Slider Section -->
<!-- Theme Features Section -->
<div class="container">
	
	<div class="row">
		<div class="themedetail_heading_title">
			<h2>Our Unique Theme Features</h2>
			<div id="" class="themedetail_separator"></div>
		</div>
	</div>
	
	<div class="row">
		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-tablet"></i>
				</div>
				<div class="media-body">
					<h3>Reponsive Layout</h3>
					<p>Our all Themes are Mobile friendly and easily adapts the various screen sizes.</p>
				</div>
			</div>
		</div>		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-cog"></i>
				</div>
				<div class="media-body">
					<h3>Theme Options</h3>
					<p>Theme provides Theme Options Panel, for customizing the the theme.</p>
				</div>
			</div>
		</div>		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-thumbs-o-up"></i>
				</div>
				<div class="media-body">
					<h3>Friendly Support</h3>
					<p>Our great support team is ready to help.Our clients are valuable for us.</p>
				</div>
			</div>
		</div>		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-language"></i>
				</div>
				<div class="media-body">
					<h3>Translation Ready</h3>
					<p>Themes our translation ready you can translate theme in your own language.</p>
				</div>
			</div>
		</div>		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-globe"></i>
				</div>
				<div class="media-body">
					<h3>Browser Compatibility</h3>
					<p>Themes our cross browser competible. Theme supports all modern browser. </p>
				</div>
			</div>
		</div>		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-file-code-o"></i>
				</div>
				<div class="media-body">
					<h3>Shortcodes</h3>
					<p>Theme has a variety of short code.You can add them into Post / Page.</p>
				</div>
			</div>
		</div>		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-sliders"></i>
				</div>
				<div class="media-body">
					<h3>Custom Widgets</h3>
					<p>Theme has custom widgets to add in sidebar.</p>
				</div>
			</div>
		</div>		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-file"></i>
				</div>
				<div class="media-body">
					<h3>Page Templates</h3>
					<p>Theme has six page templates. </p>
				</div>
			</div>
		</div>		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-thumbs-o-up"></i>
				</div>
				<div class="media-body">
					<h3>Multi Colors Schemes</h3>
					<p>Variety of colors are provided with the theme.</p>
				</div>
			</div>
		</div>	
	</div>
 </div>
<!-- /Theme Features Section -->
<div class="themedatail_testimonial_section">
	<?php get_template_part('index', 'testimonial'); ?>
</div>

<!--Theme Detail Image Section -->
<div class="container">
	<div class="row">
		<div class="themedetail_img_heading_title">
			<h2>Our Unique Theme Features</h2>
			<div class="direction_arrow">
				<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/arrow.png">
			</div>
		</div>
	</div>	
	<div class="row">
		<div class="col-md-12 themedetail_image">
			<img class="img-responsive" title="Spa Theme Detail" src="<?php echo get_template_directory_uri() ?>/images/theme_detail_spasalon.jpg">
		</div>
	</div>	
</div>
<!-- /Theme Detail Image Section -->

<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="https://checkout.freemius.com/checkout.min.js"></script>
<script>
    var handler = FS.Checkout.configure({
        plugin_id:  '11283',
        plan_id:    '19174',
        public_key: 'pk_bbf2edd33df2ecbab4887066f1391',
        image:      'https://your-plugin-site.com/logo-100x100.png'
    });
    
    $('.spasalon_pro-freemius').on('click', function (e) {
        handler.open({
            name     : 'SpaSalon Pro WordPress Theme',
            licenses : 1,
            // You can consume the response for after purchase logic.
            purchaseCompleted  : function (response) {
                // The logic here will be executed immediately after the purchase confirmation.                                // alert(response.user.email);
            },
            success  : function (response) {
                // The logic here will be executed after the customer closes the checkout, after a successful purchase.                                // alert(response.user.email);
            }
        });
        e.preventDefault();
    });
</script>
<?php get_footer(); ?>