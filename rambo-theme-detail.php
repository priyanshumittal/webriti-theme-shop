<?php //Template Name: Rambo-Theme-Detail ?>
<?php get_header(); ?>
<!-- Homepage Slider Section -->
<div class="themedetail_main_slider">
	<div class="container">
		<div class="row">
			
			<div class="col-md-7">
				<a href="https://demo.webriti.com/?theme=Rambo%20Pro"><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/rambo_theme_mockup.png"></a>
			</div>
			
			<div class="col-md-5 themedetail_data">
				<h2>Rambo - Pro</h2>
				<p>Rambo is a Responsive Multi-Purpose WordPress Theme. It is ideal for creating a corporate website. It boasts of a highy functional Home Page and Widgetized Footer Area. Build an effective online presence with Rambo</p>
				<div class="themedetail_btntop">
                    <a class="themedetail_slide_btn" target="_blank" href="https://demo.webriti.com/?theme=Rambo%20Pro">View Demo</a><span>or</span>
					<a class="buy_theme_btn rambo_pro-freemius" style="color:#fff">Buy Now</a>
                </div>
			</div>			
		</div>
	</div>
</div>
<div class="homepage_main_slide_shadow"></div>
<!-- /Homepage Slider Section -->
<!-- Theme Features Section -->
<div class="container">
	
	<div class="row">
		<div class="themedetail_heading_title">
			<h2>Our Unique Theme Features</h2>
			<div id="" class="themedetail_separator"></div>
		</div>
	</div>
	
	<div class="row">
		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-tablet"></i>
				</div>
				<div class="media-body">
					<h3>Reponsive Layout</h3>
					<p>All of our Theme contain Responsive framework that adapt to Mobile Devices.</p>
				</div>
			</div>
		</div>		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-cog"></i>
				</div>
				<div class="media-body">
					<h3>Theme Options</h3>
					<p>Theme provides Theme Options Panel, for customizing the the theme.</p>
				</div>
			</div>
		</div>		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-thumbs-o-up"></i>
				</div>
				<div class="media-body">
					<h3>Friendly Support</h3>
					<p>Our great support team is ready to help.Our clients are valuable for us.</p>
				</div>
			</div>
		</div>		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-language"></i>
				</div>
				<div class="media-body">
					<h3>Translation Ready</h3>
					<p>Themes our translation ready you can translate theme in your own language.</p>
				</div>
			</div>
		</div>		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-globe"></i>
				</div>
				<div class="media-body">
					<h3>Browser Compatibility</h3>
					<p>Themes our cross browser competible. Theme supports all modern browser. </p>
				</div>
			</div>
		</div>		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-file-code-o"></i>
				</div>
				<div class="media-body">
					<h3>Shortcodes</h3>
					<p>Theme has a variety of short code.You can add them into Post / Page.</p>
				</div>
			</div>
		</div>			
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-sliders"></i>
				</div>
				<div class="media-body">
					<h3>Custom Widgets</h3>
					<p>Customize the Top Header, Footer and Sidebar with inbuilt custom widgets.</p>
				</div>
			</div>
		</div>		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-file"></i>
				</div>
				<div class="media-body">
					<h3>Page Templates</h3>
					<p>There are 28 page templates in the theme like Multiple Blog Layouts, Service Templates etc.</p>
				</div>
			</div>
		</div>		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-thumbs-o-up"></i>
				</div>
				<div class="media-body">
					<h3>Multi Color Schemes</h3>
					<p>Rambo ships with 5 color schemes. Customize your website color scheme with 1 click.</p>
				</div>
			</div>
		</div>	
    <div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-thumbs-o-up"></i>
				</div>
				<div class="media-body">
					<h3>Header Variations</h3>
					<p>There are various header variations in the theme like Classic Header, Overlap Header and more.</p>
				</div>
			</div>
		</div>	
    <div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-thumbs-o-up"></i>
				</div>
				<div class="media-body">
					<h3>Section Variations</h3>
					<p>There are various section variations of the business template in the theme like Service section variations.</p>
				</div>
			</div>
		</div>	
    <div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-thumbs-o-up"></i>
				</div>
				<div class="media-body">
					<h3>Search Effects</h3>
					<p>There are 2 options available in our theme for search: Pop up light and Pop up dark.</p>
				</div>
			</div>
		</div>
	</div>
 </div>
<!-- /Theme Features Section -->
<!--Theme Detail Testimonial Section-->
<div class="themedatail_testimonial_section">
	<?php get_template_part('index', 'testimonial'); ?>
</div>

<!--Theme Detail Image Section -->
<div class="container">
	<div class="row">
		<div class="themedetail_img_heading_title">
			<h2>Our Unique Theme Features</h2>
			<div class="direction_arrow">
				<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/arrow.png">
			</div>
		</div>
	</div>	
	<div class="row">
		<div class="col-md-12">
			<img class="img-responsive" title="Rambo Theme Detail 1" src="<?php echo get_template_directory_uri() ?>/images/rtd-1.jpg">
		</div>
    <div class="col-md-12 themedetail_image">
			<img class="img-responsive" title="Rambo Theme Detail 2" src="<?php echo get_template_directory_uri() ?>/images/rtd-2.jpg">
		</div>
	</div>	
</div>
<!-- /Theme Detail Image Section -->

<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="https://checkout.freemius.com/checkout.min.js"></script>
<script>
    var handler = FS.Checkout.configure({
        plugin_id:  '11282',
        plan_id:    '19172',
        public_key: 'pk_86382d3a3a35e977b1b4654c728e7',
        image:      'https://your-plugin-site.com/logo-100x100.png'
    });
    
    $('.rambo_pro-freemius').on('click', function (e) {
        handler.open({
            name     : 'Rambo Pro WordPress Theme',
            licenses : 1,
            // You can consume the response for after purchase logic.
            purchaseCompleted  : function (response) {
                // The logic here will be executed immediately after the purchase confirmation.                                // alert(response.user.email);
            },
            success  : function (response) {
                // The logic here will be executed after the customer closes the checkout, after a successful purchase.                                // alert(response.user.email);
            }
        });
        e.preventDefault();
    });
</script>
<?php get_footer(); ?>