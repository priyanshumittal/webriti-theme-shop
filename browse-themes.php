<?php //Template Name: ALL THEMES  ?>
<?php get_header(); ?>
<!-- Page Title Section ---->
<div class="container">
	<div class="row">
		<div class="webriti_page_heading">
			<h1>Premium <span>WordPress</span> Themes</h1>
			<p>Our collection features Premium WordPress Themes for bloggers, businesses and freelancers.</p>
			<div class="page_separator" id=""></div>
		</div>
	</div>
</div>
<!-- /Page Title Section ---->
<!-- Our Theme Section ---->
<div class="container">		
	<div class="row">
		<div class="col-md-4 col-sm-6">			
			<div class="theme_showcase_area ">
				<div class="theme_showcase_area_link"><a href="http://webriti.com/appointment" alt="Appointment WordPress Theme" title="Appointment">Appointment</a></div>
				<a href="http://webriti.com/appointment"><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/showcase10.jpg" title="Appointment"></a>
				<p>Appointment is a clean and Fresh theme for corporates. You can use it for your business, Corporate, Retaurant, portfolio, blogging or any type of site.</p>
				<div class="theme_btn_align">
          <a type="button" class="theme_btn demo_btn" href="https://demo.webriti.com/?theme=Appointment%20Pro">Light Demo</a>
					<a type="button" class="theme_btn demo_btn" href="https://appointment-dark-pro.webriti.com/">Dark Demo</a>
					<a type="button" class="theme_btn detail_btn" href="http://webriti.com/appointment">Detail</a>	
				</div>				
			</div>
			<div class="ribbon ribbon-large ribbon-red">
				<div class="banner">
				<div class="text" style="font-family: ronnia, Helvetica, Arial, sans-serif;"><strong><center>Popular</center></strong></div>
				</div>
			</div>
		</div>	
		
		<div class="col-md-4 col-sm-6">
			<div class="theme_showcase_area">
				<div class="theme_showcase_area_link"><a href="http://webriti.com/spasalon" title="Spasalon">Spasalon</a></div>
				<a href="http://webriti.com/spasalon"><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/showcase1.jpg" title="Spasalon"></a>
				<p>A responsive theme for SPA SALON and BEAUTY SALON type of business that uses multiple nav menus, Right-sidebar, Featured Slider.</p>
				<div class="theme_btn_align">
					<a type="button" class="theme_btn demo_btn" href="https://spasalon-pro.webriti.com/">Demo</a>
					<a type="button" class="theme_btn detail_btn" href="http://webriti.com/spasalon">Detail</a>	
				</div>
			</div>
		</div>
		
		<div class="col-md-4 col-sm-6">
			<div class="theme_showcase_area">
				<div class="theme_showcase_area_link"><a href="http://webriti.com/busiprof" alt="Busiprof" title="Busiprof">Busiprof</a></div>
				<a href="http://webriti.com/busiprof"><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/showcase2.jpg" title="Busiprof"></a>
				<p>A Beautiful and FLexible Responsive Business Theme. It is ideal for creating a corporate website. It boasts of a highy functional Home Page.</p>
				<div class="theme_btn_align">
					<a type="button" class="theme_btn demo_btn" href="https://demo.webriti.com/?theme=Busiprof%20Pro">Demo</a>
					<a type="button" class="theme_btn detail_btn" href="http://webriti.com/busiprof">Detail</a>
				</div>
			</div>
		</div>	
		
		<div class="col-md-12"><div class="theme_showcase_seperator"></div></div>	
		
		<div class="col-md-4 col-sm-6">
			<div class="theme_showcase_area">
				<div class="theme_showcase_area_link"><a href="http://webriti.com/rambo" alt="rambo" title="Rambo">Rambo</a></div>
				<a href="http://webriti.com/rambo"><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/rambo_showcase.jpg" title="Rambo"></a>
				<p>Rambo is a Responsive Multi-Purpose Wordpress Theme. It is ideal for creating a corporate website. It boasts of a highy functional Home Page .Very Cleaned Design.</p>
				<div class="theme_btn_align">
					<a type="button" class="theme_btn demo_btn" href="https://demo.webriti.com/?theme=Rambo%20Pro">Demo</a>
					<a type="button" class="theme_btn detail_btn" href="http://webriti.com/rambo">Detail</a>	
				</div>
			</div>
		</div>

		<div class="col-md-4 col-sm-6">			
			<div class="theme_showcase_area ">
				<div class="theme_showcase_area_link"><a href="http://webriti.com/wallstreet/" alt="wallstreet" title="wallstreet">WallStreet</a></div>
				<a href="http://webriti.com/wallstreet/"><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/showcase7.jpg" title="Wallstreet"></a>
				<p>WallStreet is a theme with a different look, responsive and multipurpose theme. Dark color scheme with beautiful typography makes it a must have.</p>
				<div class="theme_btn_align">
					<a type="button" class="theme_btn demo_btn" href="https://demo.webriti.com/?theme=Wallstreet%20Pro">Demo</a>
					<a type="button" class="theme_btn detail_btn" href="http://webriti.com/wallstreet/">Detail</a>	
				</div>
			</div>
		</div>	
		
		<!-- <div class="col-md-4 col-sm-6">
			<div class="theme_showcase_area">
				<div class="theme_showcase_area_link"><a href="http://webriti.com/healthcentre" title="healthcentre">Health Centre</a></div>
				<a href="http://webriti.com/healthcentre"><img class="img-responsive" src="<?php //echo get_template_directory_uri() ?>/images/showcase4.jpg" title="healthcentre"></a>
				<p>HealthCenter is a Responsive Multi-Purpose Wordpress Theme. It is ideal for Gym/Health Healing Center Owner . It boasts of a highy functional Home Page.</p>
				<div class="theme_btn_align">
					<a type="button" class="theme_btn demo_btn" href="http://webriti.com/demo/wp/preview/?prev=healthcentre/">Demo</a>
					<a type="button" class="theme_btn detail_btn" href="http://webriti.com/healthcentre">Detail</a>	
				</div>
			</div>
		</div> -->	
		
		<div class="col-md-4 col-sm-6">
			<div class="theme_showcase_area">
				<div class="theme_showcase_area_link"><a href="http://webriti.com/quality/" alt="Quality" title="Quality">Quality</a></div>
				<a href="http://webriti.com/quality/"><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/showcase5-new.jpg" title="Quality"></a>
				<p>Quality is Responsive Multi-Purpose Wordpress Theme. Can be successfully used for corporate site, blog site, portfolio and many more.</p>
				<div class="theme_btn_align">
					<a type="button" class="theme_btn demo_btn" href="https://demo.webriti.com/?theme=Quality%20Pro">Demo</a>
					<a type="button" class="theme_btn detail_btn" href="http://webriti.com/quality/">Detail</a>	
				</div>
			</div>			
		</div>
		
		<div class="col-md-12"><div class="theme_showcase_seperator"></div></div>	
		
		<div class="col-md-4 col-sm-6">			
			<div class="theme_showcase_area ">
				<div class="theme_showcase_area_link"><a href="http://webriti.com/elitepress/" alt="ElitePress WordPress Theme" title="ElitePress">ElitePress</a></div>
				<a href="http://webriti.com/elitepress/"><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/elitepress_showcase.jpg" title="ElitePress"></a>				
				<p>ElitePress is a clean and multipurpose wordpress theme for corporates. We want to present you a simple and functional theme "ElitePress".</p>
				<div class="theme_btn_align">
					<a type="button" class="theme_btn demo_btn" href="https://demo.webriti.com/?theme=ElitePress%20Pro">Demo</a>
					<a type="button" class="theme_btn detail_btn" href="http://webriti.com/elitepress">Detail</a>	
				</div>
			</div>
		</div>
		
		<div class="col-md-4 col-sm-6">
			<div class="theme_showcase_area">
				<div class="theme_showcase_area_link"><a href="http://webriti.com/corpbiz/" alt="Corpbiz" title="Corpbiz">Corpbiz</a></div>
				<a href="http://webriti.com/corpbiz/"><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/showcase6.jpg" title="Corpbiz"></a>
				<p>A responsive theme for Corporates and any type of business that uses multiple nav menus, Right-sidebar, Featured Slider. Beautifully designed home page all manage via option panel.</p>
				<div class="theme_btn_align">
					<a type="button" class="theme_btn demo_btn" href="http://webriti.com/demo/wp/preview/?prev=corpbiz/">Demo</a>
					<a type="button" class="theme_btn detail_btn" href="http://webriti.com/corpbiz/">Detail</a>
				</div>
			</div>		
		</div>	
		
		<!--<div class="col-md-12"><div class="theme_showcase_seperator"></div></div>-->	
		<br />
		
	</div>	
	<div class="row seperator_space"></div>	
 </div>
<!-- /Our Theme Section -->
<?php get_footer(); ?>