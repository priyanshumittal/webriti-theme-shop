<?php //Template Name: Busiprof-Theme-Detail ?>
<?php get_header(); ?>
<!-- Homepage Slider Section -->
<div class="themedetail_main_slider">
	<div class="container">
		<div class="row">
			
			<div class="col-md-7">
				<a href=""><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/busiprof_theme_slide.png"></a>
			</div>
			
			<div class="col-md-5 themedetail_data">
				<h2>Busiprof - Pro</h2>
				<p>A Beautiful and Flexible Responsive Businees Theme. It is ideal for creating a corporate website. It boasts of a highy functional Home Page and Widgetized Footer. Build an effective online presence with BusiProf.</p>
				<div class="themedetail_btntop">
                    <a class="themedetail_slide_btn" target="_blank" href="https://demo.webriti.com/?theme=Busiprof%20Pro" >View Demo</a><span>or</span>
					<a class="buy_theme_btn busiprof_pro-freemius" style="color:#fff">Buy Now</a>
                </div>
			</div>			
			<div id="myModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="background:transparent;border:0px;display:none;">
	</div>
			
		</div>
	</div>
</div>

<div class="homepage_main_slide_shadow"></div>
<!-- /Homepage Slider Section -->

<!--Theme Funfact-->
<div class="funfact bg-grey">
    <div class="container">
        <div class="row ">
            <div class="col-lg-4 col-sm-6 p-all-0">
                <div class="funfact-inner text-center">
                    <figure class="funfact-icon">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/changelog-icon.png" alt="Changelog">
                    </figure>
                    <h5 class="funfact-title"><a href="https://webriti.com/busiprof-pro-changelog/" target="_blank">CHANGELOG</a></h5>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 p-all-0">
                <div class="funfact-inner text-center">
                    <figure class="funfact-icon">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/support-icon.png" alt="Free vs Pro">
                    </figure>
                    <h5 class="funfact-title"><a href="https://users.freemius.com/login" target="_blank">THEME SUPPORT</a></h5>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 p-all-0">
                <div class="funfact-inner text-center">
                    <figure class="funfact-icon">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/doc-icon.png" alt="Documentation">
                    </figure>
                    <h5 class="funfact-title"><a href="https://help.webriti.com/category/themes/busiprof/" target="_blank">DOCUMENTATION</a></h5>
                </div>
            </div>

        </div>
    </div>
</div>
<!--/Theme Funfact-->

<!-- Theme Features Section -->
<div class="container">
	
	<div class="row">
		<div class="themedetail_heading_title">
			<h2>Our Unique Theme Features</h2>
			<div id="" class="themedetail_separator"></div>
		</div>
	</div>
	
	<div class="row">
		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-tablet"></i>
				</div>
				<div class="media-body">
					<h3>Reponsive Layout</h3>
					<p>Our all Themes are Mobile friendly and easily adapts the various screen sizes.</p>
				</div>
			</div>
		</div>		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-cog"></i>
				</div>
				<div class="media-body">
					<h3>Theme Options</h3>
					<p>Theme provides Theme Options Panel, for customizing the the theme.</p>
				</div>
			</div>
		</div>		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-thumbs-o-up"></i>
				</div>
				<div class="media-body">
					<h3>Friendly Support</h3>
					<p>Our great support team is ready to help.Our clients are valuable for us.</p>
				</div>
			</div>
		</div>		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-language"></i>
				</div>
				<div class="media-body">
					<h3>Translation Ready</h3>
					<p>Themes our translation ready you can translate theme in your own language.</p>
				</div>
			</div>
		</div>		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-globe"></i>
				</div>
				<div class="media-body">
					<h3>Browser Compatibility</h3>
					<p>Themes our cross browser competible. Theme supports all modern browser. </p>
				</div>
			</div>
		</div>		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-file-code-o"></i>
				</div>
				<div class="media-body">
					<h3>Shortcodes</h3>
					<p>Theme has a variety of short code.You can add them into Post / Page.</p>
				</div>
			</div>
		</div>		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-sliders"></i>
				</div>
				<div class="media-body">
					<h3>Custom Widgets</h3>
					<p>Theme has custom widgets to add in the top header sidebar.</p>
				</div>
			</div>
		</div>		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-file"></i>
				</div>
				<div class="media-body">
					<h3>Page Templates</h3>
					<p>There are 38 page templates in the theme. </p>
				</div>
			</div>
		</div>
    <div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-search"></i>
				</div>
				<div class="media-body">
					<h3>SEO Friendly urls</h3>
					<p>Option provided to rename the custom post types in order to create seo friendly urls.</p>
				</div>
			</div>
		</div>	
    <div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-header"></i>
				</div>
				<div class="media-body">
					<h3>Header Variations</h3>
					<p>There are various header variations in the theme like Classic Header, Overlap Header and more.</p>
				</div>
			</div>
		</div>	
    	<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-bars"></i>
				</div>
				<div class="media-body">
					<h3>Section Variations</h3>
					<p>There are various section variations of the business template in the theme like Service section variations.</p>
				</div>
			</div>
		</div>	
    	<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-search-plus"></i>
				</div>
				<div class="media-body">
					<h3>Search Effects</h3>
					<p>There are 3 options available in our theme for search: Toggle, Pop up light and Pop up dark.</p>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-line-chart"></i>
				</div>
				<div class="media-body">
					<h3>Performance Module</h3>
					<p>Host Google fonts locally which helps to make your site faster.</p>
				</div>
			</div>
		</div>	
	</div>
 </div>
<!-- /Theme Features Section -->
<!--Theme Detail Testimonial Section-->
<div class="themedatail_testimonial_section">
	<?php get_template_part('index', 'testimonial'); ?>
</div>

<!--Theme Detail Image Section -->
<div class="container">
	<div class="row">
		<div class="themedetail_img_heading_title">
			<h2>Our Unique Theme Features</h2>
			<div class="direction_arrow">
				<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/arrow.png">
			</div>
		</div>
	</div>	
	<div class="row">
		<div class="col-md-12">
			<img class="img-responsive" title="Busiprof Theme Detail 1" src="<?php echo get_template_directory_uri() ?>/images/busiprof-theme-detail-1.jpg">
		</div>
    <div class="col-md-12 themedetail_image">
			<img class="img-responsive" title="Busiprof Theme Detail 2" src="<?php echo get_template_directory_uri() ?>/images/busiprof-theme-detail-2.jpg">
		</div>
	</div>	
</div>
<!-- /Theme Detail Image Section -->

<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="https://checkout.freemius.com/checkout.min.js"></script>
<script>
    var handler = FS.Checkout.configure({
        plugin_id:  '11279',
        plan_id:    '19166',
        public_key: 'pk_5ea01e78f217552052e9a011d91c1',
        image:      'https://your-plugin-site.com/logo-100x100.png'
    });
    
    $('.busiprof_pro-freemius').on('click', function (e) {
        handler.open({
            name     : 'BusiProf Pro WordPress Theme',
            licenses : 1,
            // You can consume the response for after purchase logic.
            purchaseCompleted  : function (response) {
                // The logic here will be executed immediately after the purchase confirmation.                                // alert(response.user.email);
            },
            success  : function (response) {
                // The logic here will be executed after the customer closes the checkout, after a successful purchase.                                // alert(response.user.email);
            }
        });
        e.preventDefault();
    });
</script>
<?php get_footer(); ?>