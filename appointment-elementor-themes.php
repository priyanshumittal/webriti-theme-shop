<?php //Template Name: Appointment Elementor Themes  ?>
<?php get_header(); ?>
<!-- Page Title Section ---->
<div class="container elementor_heading ap-el-themes">
	<div class="row">
		<div class="webriti_page_heading">
			<h1>Start your Business with Appointment Starter</h1>
			<p>Get your business’s website live in no real time. Browse our beautiful collection of ready-to-use websites that
				have everything you need to present yourself online. Go pro to get them all and pick the starter best fits to
				your business.</p>
			<div class="page_separator" id=""></div>
		</div>
	</div>
</div>
<!-- /Page Title Section ---->
<!-- Our Theme Section ---->
<div class="ap-starter-sites ap-el-template">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="app-demo">
					<div class="app-demo-wrapper">
						<a target="_blank" href="<?php echo esc_url('https://demo-appointment.webriti.com/demo-one/');?>">
							<div class="ap-demo-img">
								<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/gutenberg-demos/ap-demo-one.jpg" alt="">
							</div>
							<div class="ap-demo-overlay">
								<i class="fa-solid fa-eye"></i>
							</div>
						</a>
						<div class="ap-demo-link">
							<a target="_blank" href="<?php echo esc_url('https://demo-appointment.webriti.com/demo-one/');?>">Gutenberg</a>
						</div>
					</div>
					<div class="ap-demo-title">
						<h4 class="ap-demo-head">
						Appointment Lite
						</h4>
						<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/gutenberg-icon.png" alt="">
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="app-demo">
					<div class="app-demo-wrapper">
						<a target="_blank" href="<?php echo esc_url('https://demo-appointment.webriti.com/demo-two/');?>">
							<div class="ap-demo-img">
								<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/gutenberg-demos/ap-demo-two.jpg" alt="">
							</div>
							<div class="ap-demo-overlay">
								<i class="fa-solid fa-eye"></i>
							</div>
						</a>
						<div class="ap-demo-link">
							<a target="_blank" href="<?php echo esc_url('https://demo-appointment.webriti.com/demo-two/');?>">Gutenberg</a>
						</div>
					</div>
					<div class="ap-demo-title">
						<h4 class="ap-demo-head">Growfitz</h4>
						<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/gutenberg-icon.png" alt="">
					</div>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="app-demo">
					<div class="app-demo-wrapper">
						<a target="_blank" href="<?php echo esc_url('https://demo-appointment.webriti.com/demo-three/');?>">
							<div class="ap-demo-img">
								<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/gutenberg-demos/ap-demo-three.jpg" alt="">
							</div>
							<div class="ap-demo-overlay">
								<i class="fa-solid fa-eye"></i>
							</div>
						</a>
						<div class="ap-demo-link">
							<a target="_blank" href="<?php echo esc_url('https://demo-appointment.webriti.com/demo-three/');?>">Gutenberg</a>
						</div>
					</div>
					<div class="ap-demo-title">
						<h4 class="ap-demo-head">Building Construction</h4>
						<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/gutenberg-icon.png" alt="">
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="app-demo">
					<div class="app-demo-wrapper pro">
						<a target="_blank" href="<?php echo esc_url('https://demo-appointment.webriti.com/demo-pro-one/');?>">
							<div class="ap-demo-img">
								<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/gutenberg-demos/ap-demo-pro-one.jpg" alt="">
							</div>
							<div class="ap-demo-overlay">
								<i class="fa-solid fa-eye"></i>
							</div>
						</a>
						<div class="ap-demo-link">
							<a target="_blank" href="<?php echo esc_url('https://demo-appointment.webriti.com/demo-pro-one/');?>">Gutenberg</a>
						</div>
					</div>
					<div class="ap-demo-title">
						<h4 class="ap-demo-head">Appointment Pro </h4>
						<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/gutenberg-icon.png" alt="">
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="app-demo">
					<div class="app-demo-wrapper pro">
						<a target="_blank" href="<?php echo esc_url('https://demo-appointment.webriti.com/demo-pro-two/');?>">
							<div class="ap-demo-img">
								<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/gutenberg-demos/ap-demo-pro-two.jpg" alt="">
							</div>
							<div class="ap-demo-overlay">
								<i class="fa-solid fa-eye"></i>
							</div>
						</a>
						<div class="ap-demo-link">
							<a target="_blank" href="<?php echo esc_url('https://demo-appointment.webriti.com/demo-pro-two/');?>">Gutenberg</a>
						</div>
					</div>
					<div class="ap-demo-title">
						<h4 class="ap-demo-head">Business</h4>
						<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/gutenberg-icon.png" alt="">
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="app-demo">
					<div class="app-demo-wrapper pro">
						<a target="_blank" href="<?php echo esc_url('https://demo-appointment.webriti.com/demo-pro-three/');?>">
							<div class="ap-demo-img">
								<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/gutenberg-demos/ap-demo-pro-three.jpg" alt="">
							</div>
							<div class="ap-demo-overlay">
								<i class="fa-solid fa-eye"></i>
							</div>
						</a>
						<div class="ap-demo-link">
							<a target="_blank" href="<?php echo esc_url('https://demo-appointment.webriti.com/demo-pro-three/');?>">Gutenberg</a>
						</div>
					</div>
					<div class="ap-demo-title">
						<h4 class="ap-demo-head">Corporate</h4>
						<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/gutenberg-icon.png" alt="">
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="app-demo">
					<div class="app-demo-wrapper pro">
						<a target="_blank" href="<?php echo esc_url('https://demo-appointment.webriti.com/demo-pro-four/');?>">
							<div class="ap-demo-img">
								<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/gutenberg-demos/ap-demo-pro-four.jpg" alt="">
							</div>
							<div class="ap-demo-overlay">
								<i class="fa-solid fa-eye"></i>
							</div>
						</a>
						<div class="ap-demo-link">
							<a target="_blank" href="<?php echo esc_url('https://demo-appointment.webriti.com/demo-pro-four/');?>">Gutenberg</a>
						</div>
					</div>
					<div class="ap-demo-title">
						<h4 class="ap-demo-head">Digital Agency</h4>
						<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/gutenberg-icon.png" alt="">
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="app-demo">
					<div class="app-demo-wrapper">
						<a target="_blank" href="<?php echo esc_url('https://ap-business.webriti.com/');?>">
							<div class="ap-demo-img">
								<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/Elementor-demos/ap-demo-business.jpg" alt="">
							</div>
							<div class="ap-demo-overlay">
								<i class="fa-solid fa-eye"></i>
							</div>
						</a>
						<div class="ap-demo-link">
							<a target="_blank" href="<?php echo esc_url('https://ap-business.webriti.com/');?>">Elementor</a>
						</div>
					</div>
					<div class="ap-demo-title">
						<h4 class="ap-demo-head">Business</h4>
						<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/elementor-icon.png" alt="">
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="app-demo">
					<div class="app-demo-wrapper">
						<a target="_blank" href="<?php echo esc_url('https://ap-restaurants.webriti.com/');?>">
							<div class="ap-demo-img">
								<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/Elementor-demos/ap-demo-restaurant.jpg" alt="">
							</div>
							<div class="ap-demo-overlay">
								<i class="fa-solid fa-eye"></i>
							</div>
						</a>
						<div class="ap-demo-link">
							<a target="_blank" href="<?php echo esc_url('https://ap-restaurants.webriti.com/');?>">Elementor</a>
						</div>
					</div>
					<div class="ap-demo-title">
						<h4 class="ap-demo-head">Restaurant</h4>
						<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/elementor-icon.png" alt="">
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="app-demo">
					<div class="app-demo-wrapper pro">
						<a target="_blank" href="<?php echo esc_url('https://ap-architect.webriti.com/');?>">
							<div class="ap-demo-img">
								<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/Elementor-demos/ap-demo-architect.jpg" alt="">
							</div>
							<div class="ap-demo-overlay">
								<i class="fa-solid fa-eye"></i>
							</div>
						</a>
						<div class="ap-demo-link">
							<a target="_blank" href="<?php echo esc_url('https://ap-architect.webriti.com/');?>">Elementor</a>
						</div>
					</div>
					<div class="ap-demo-title">
						<h4 class="ap-demo-head">Architect</h4>
						<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/elementor-icon.png" alt="">
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="app-demo">
					<div class="app-demo-wrapper pro">
						<a target="_blank" href="<?php echo esc_url('https://ap-corporate.webriti.com/');?>">
							<div class="ap-demo-img">
								<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/Elementor-demos/ap-demo-corporate.jpg" alt="">
							</div>
							<div class="ap-demo-overlay">
								<i class="fa-solid fa-eye"></i>
							</div>
						</a>
						<div class="ap-demo-link">
							<a target="_blank" href="<?php echo esc_url('https://ap-corporate.webriti.com/');?>">Elementor</a>
						</div>
					</div>
					<div class="ap-demo-title">
						<h4 class="ap-demo-head">Corporate</h4>
						<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/elementor-icon.png" alt="">
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="app-demo">
					<div class="app-demo-wrapper pro">
						<a target="_blank" href="<?php echo esc_url('https://ap-education.webriti.com/');?>">
							<div class="ap-demo-img">
								<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/Elementor-demos/ap-demo-education.jpg" alt="">
							</div>
							<div class="ap-demo-overlay">
								<i class="fa-solid fa-eye"></i>
							</div>
						</a>
						<div class="ap-demo-link">
							<a target="_blank" href="<?php echo esc_url('https://ap-education.webriti.com/');?>">Elementor</a>
						</div>
					</div>
					<div class="ap-demo-title">
						<h4 class="ap-demo-head">Education</h4>
						<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/elementor-icon.png" alt="">
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="app-demo">
					<div class="app-demo-wrapper pro">
						<a target="_blank" href="<?php echo esc_url('https://ap-finance.webriti.com/');?>">
							<div class="ap-demo-img">
								<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/Elementor-demos/ap-demo-finance.jpg" alt="">
							</div>
							<div class="ap-demo-overlay">
								<i class="fa-solid fa-eye"></i>
							</div>
						</a>
						<div class="ap-demo-link">
							<a target="_blank" href="<?php echo esc_url('https://ap-finance.webriti.com/');?>">Elementor</a>
						</div>
					</div>
					<div class="ap-demo-title">
						<h4 class="ap-demo-head">Finance</h4>
						<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/elementor-icon.png" alt="">
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="app-demo">
					<div class="app-demo-wrapper pro">
						<a target="_blank" href="<?php echo esc_url('https://ap-maintenance.webriti.com/');?>">
							<div class="ap-demo-img">
								<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/Elementor-demos/ap-demo-maintenance.jpg" alt="">
							</div>
							<div class="ap-demo-overlay">
								<i class="fa-solid fa-eye"></i>
							</div>
						</a>
						<div class="ap-demo-link">
							<a target="_blank" href="<?php echo esc_url('https://ap-maintenance.webriti.com/');?>">Elementor</a>
						</div>
					</div>
					<div class="ap-demo-title">
						<h4 class="ap-demo-head">Maintenance</h4>
						<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/elementor-icon.png" alt="">
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="app-demo">
					<div class="app-demo-wrapper pro">
						<a target="_blank" href="<?php echo esc_url('https://demo-appointment.webriti.com/demo-pro-five/');?>">
							<div class="ap-demo-img">
								<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/Elementor-demos/demo-pro-five.jpg" alt="">
							</div>
							<div class="ap-demo-overlay">
								<i class="fa-solid fa-eye"></i>
							</div>
						</a>
						<div class="ap-demo-link">
							<a target="_blank" href="<?php echo esc_url('https://demo-appointment.webriti.com/demo-pro-five/');?>">Elementor</a>
						</div>
					</div>
					<div class="ap-demo-title">
						<h4 class="ap-demo-head">Architecture</h4>
						<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/elementor-icon.png" alt="">
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="app-demo">
					<div class="app-demo-wrapper">
						<a target="_blank" href="<?php echo esc_url('https://ap-default.webriti.com/');?>">
							<div class="ap-demo-img">
								<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/Elementor-demos/ap-default.jpg" alt="">
							</div>
							<div class="ap-demo-overlay">
								<i class="fa-solid fa-eye"></i>
							</div>
						</a>
						<div class="ap-demo-link">
							<a target="_blank" href="<?php echo esc_url('https://ap-default.webriti.com/');?>">Elementor</a>
						</div>
					</div>
					<div class="ap-demo-title">
						<h4 class="ap-demo-head">Appointment Default</h4>
						<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/elementor-icon.png" alt="">
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="app-demo">
					<div class="app-demo-wrapper pro">
						<a target="_blank" href="<?php echo esc_url('https://ap-default-pro.webriti.com/');?>">
							<div class="ap-demo-img">
								<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/Elementor-demos/ap-default-pro.jpg" alt="">
							</div>
							<div class="ap-demo-overlay">
								<i class="fa-solid fa-eye"></i>
							</div>
						</a>
						<div class="ap-demo-link">
							<a target="_blank" href="<?php echo esc_url('https://ap-default-pro.webriti.com/');?>">Elementor</a>
						</div>
					</div>
					<div class="ap-demo-title">
						<h4 class="ap-demo-head">Appointment Default</h4>
						<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/elementor-icon.png" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /Our Theme Section -->
<?php get_footer(); ?>