<?php //Template Name: Facebook Feed Pro Detail ?>
<?php get_header(); ?>
<!-- Homepage Slider Section -->
<div class="themedetail_main_slider">
	<div class="container">
		<div class="row">
			
						
			<div class="col-md-12" style="text-align:center;height:425px;">
			
			<h2>FaceBook Feed Pro</h2>
				<p>Display a Customizable and Responsive Facebook feed on your WordPress Website.</p>
				<img style="padding-bottom:25px;" class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/wordpress-love-facebook.png" />
				
				<div class="themedetail_btntop">
                    <a class="themedetail_slide_btn" target="_blank" href="http://webriti.com/facebook-feed-demo/">View Demo</a><span>or</span>
					<a class="buy_theme_btn" href="#myModal"  data-toggle="modal" style="color:#fff">Buy Now</a>
                </div>
			</div>
	<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery(".close").click(function(){
				jQuery("#myModal").hide();
			});
		});
	</script>
	<style>.homebtn {
		padding: 5px 22px !important;
		font-family: 'OpenSansBold';
		margin-bottom: 7px !important;
		color: hsl(0, 100%, 100%);
		text-shadow: none;
		background-color: hsl(0, 81%, 44%);
		background-repeat: repeat-x;
		border-color: hsl(103, 50%, 54%) hsl(103, 50%, 54%) hsl(0, 0%, 18%);
		border-color: hsla(0, 0%, 0%, 0.1) hsla(0, 0%, 0%, 0.1) hsla(0, 0%, 0%, 0.25);
		display: inline-block;
		text-align: center;
		text-decoration: none;
		vertical-align: middle;
		cursor: pointer;
		font-size: 18px;
		line-height: 30px;
		border: 1px solid hsl(0, 0%, 80%);
		border-color: hsl(0, 0%, 90%) hsl(0, 0%, 90%) hsl(0, 0%, 75%);
		border-color: hsla(0, 0%, 0%, 0.1) hsla(0, 0%, 0%, 0.1) hsla(0, 0%, 0%, 0.25);
		border-bottom-color: hsl(0, 0%, 70%);
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		border-radius: 4px;
		-webkit-box-shadow: inset 0 1px 0 hsla(0, 100%, 100%, 0.2), 0 1px 2px hsla(0, 0%, 0%, 0.05);
		-moz-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
		box-shadow: inset 0 1px 0 hsla(0, 100%, 100%, 0.2), 0 1px 2px hsla(0, 0%, 0%, 0.05);
		}
		.homebtn:hover{ text-decoration:none; color:#fff;}
		table tr td { color :#000;} 
	</style>
	<div id="myModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="background:transparent;border:0px;display:none;">
	<div class="modal-body" align="center" style="margin-top:5%">
	<div class="busiprof-pro-modal" style="background:#FFFFFF; width:290px;border:2px solid #ddd;webkit-box-shadow: 0 5px 15px rgba(0,0,0,.5);box-shadow: 0 50px 50px rgba(0,0,0,0.5);" >
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-right: -19px; margin-top: -16px; opacity:1">
		   <img src="<?php echo get_template_directory_uri(); ?>/images/close_256.png">
		</button>
		<div id="title"><h4 style="background:#6BB3D1;margin-top:0%;padding-bottom:25px;padding-top:7px;color:#FFFFFF;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;">FaceBook Feed Pro</h4>
			<div class="rate" style="display: block;font: bold 25px/62px Georgia, Serif;color: #777;background: #CFEEFC;border: 5px solid #fff;height: 68px;width: 68px;margin: -31px auto 5px; -moz-border-radius: 100px;-webkit-border-radius: 100px;border-radius: 5200px;-moz-box-shadow: 0 5px 20px #ddd inset, 0 3px 0 #999 inset;-webkit-box-shadow: 0 5px 20px #ddd inset, 0 3px 0 #999 inset;box-shadow: 0 5px 20px #ddd inset, 0 3px 0 #999 inset;">
				<span>$</span><strong>29</strong>
			 </div>
		</div>
   				<h5 style="color:#F22853;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;">KEY FEATURES</h5>
                <table class="table table-hover" >
                    <tr ><td style="text-align:center;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:14px;">
                   <strong> Use on Unlimited Websites</strong>
                    </td></tr>
                    <tr><td style="text-align:center;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:14px;">
                    <strong>1 year of Support & Updates</strong> 
                    </td></tr>
                    <tr><td style="text-align:center;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:14px;">
                   <strong> Fast & Friendly Support</strong>
                    </td> </tr>                    
                </table>
    			<center>
    			<a href="http://www.webriti.com/amember/signup/fbfeedpro"  class="homebtn"><strong>Buy Now</strong></a>
    			</center><br />
    		</div>
  		</div> 
	</div>
			
		</div>
	</div>
</div>
<div class="homepage_main_slide_shadow"></div>
<!-- /Homepage Slider Section -->
<!-- Theme Features Section -->
<div class="container">
	
	<div class="row">
		<div class="themedetail_heading_title">
			<h2>FaceBook Feed Pro Features</h2>
			<div id="" class="themedetail_separator"></div>
		</div>
	</div>
	
	<div class="row">
		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-tablet"></i>
				</div>
				<div class="media-body">
					<h3>Display a Media Rich FaceBook Feed</h3>
					<p>The premium version of the plugin allows you to display Images and Video in the FaceBook Feed.</p>
				</div>
			</div>
		</div>		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-cog"></i>
				</div>
				<div class="media-body">
					<h3>Responsive Layout</h3>
					<p>The feed easily adapts the various screen sizes. Phone or a Tablet, your feed will display perfectly.</p>
				</div>
			</div>
		</div>		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-thumbs-o-up"></i>
				</div>
				<div class="media-body">
					<h3>Tons of Customization Options</h3>
					<p>Change Font Size, Font Color , Line height etc without touching any code. </p>
				</div>
			</div>
		</div>		
		
		<div class = "clearfix"></div>
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-language"></i>
				</div>
				<div class="media-body">
					<h3>Support for Custom CSS and Custom JS</h3>
					<p>Power Users will love this feature. Customize the feed to your hearts content by adding  custom js and css via option panel. </p>
				</div>
			</div>
		</div>	

		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-globe"></i>
				</div>
				<div class="media-body">
					<h3>Display Multiple FaceBook Pages using Shortcodes</h3>
					<p>Easily display multiple Facebook feed using shortcodes. Each feed can be customized via shortcode attributes  </p>
				</div>
			</div>
		</div>		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-file-code-o"></i>
				</div>
				<div class="media-body">
					<h3>Multiple Layout Support</h3>
					<p>Use 3 pre-built layouts to display the feed. </p>
				</div>
			</div>
		</div>	

<div class = "clearfix"></div>		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-sliders"></i>
				</div>
				<div class="media-body">
					<h3>Feed Filtering</h3>
					<p>Filter your feed to display posts of certain types. Only want to display events? Yes you can do that with feed filtering. </p>
				</div>
			</div>
		</div>		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-file"></i>
				</div>
				<div class="media-body">
					<h3>Cache Support</h3>
					<p>You can cache the facebook feed to improve the page load performance. Cache duration can be configure via the option panel</p>
				</div>
			</div>
		</div>		

	</div>
 </div>
<!-- /Theme Features Section -->
<!--Theme Detail Testimonial Section-->
<div class="themedatail_testimonial_section">
	<?php get_template_part('index', 'testimonial'); ?>
</div>

<!--Theme Detail Testimonial Section--
<div class="themedatail_testimonial_section">
	<div class="container">		
		<div class="row" id="webriti-testimonial">			
			<div class="col-md-12 themedatail_trestimonial_area pull-left">
				<p>"Webriti themes are elegant, simple & creative. They can fit a wide variety of business fields. As for the technical support, I've received a very quick, efficient and kind attention. Keep up the good work!"</p>
				<div><img class="img-circle testimonial_img" src="<?php //echo get_template_directory_uri() ?>/images/testi1.jpg"></div>
				<h2><i></i>YUNNY ANDRADE<i></i></h2>
			</div>

				
		</div>		
	</div>
</div>
<!--/Theme Detail Testimonial Section-->

<?php get_footer(); ?>
