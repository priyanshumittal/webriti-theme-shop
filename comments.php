<?php if ( post_password_required() ) : ?>
	<p class="nopassword"><?php _e( 'This post is password protected. Enter the password to view any comments.', 'webriti' ); ?></p>
	<?php return; endif; ?>
         <?php if ( have_comments() ) : ?>
		<div class="hc_comment_section">	
			<div class="hc_comment_title">
				<h3><i class="fa fa-comments"></i>
				<?php echo comments_number('No Comments', '1 Comment', '% Comments'); ?>
				</h3>
			</div>
			<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :  ?>		
			<?php endif; ?>
			<?php wp_list_comments( array( 'callback' => 'webriti_comment' ) ); ?>
		</div>		
		<?php elseif ( ! comments_open() && ! is_page() && post_type_supports( get_post_type(), 'comments' ) ) : 
        //_e("Comments Are Closed!!!",'webriti');
		?>
	<?php endif; ?>
	<?php if ('open' == $post->comment_status) : ?>
	<?php if ( get_option('comment_registration') && !$user_ID ) : ?>
<p><?php _e("You must be",'webriti'); ?> <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?redirect_to=<?php echo urlencode(get_permalink()); ?>"><?php _e("logged in",'webriti')?></a> <?php _e("to post a comment",'webriti'); ?>
</p>
<?php else : ?>
	<div class="webriti_comment_form_section">
	<?php  
	 $fields=array(
		'author' => '<div class="row"><div class="col-xs-6"><input class="blogdetail_input_control" name="author" id="author" value="" type="text" placeholder="Full Name" /></div>',
		'email' => '<div class="col-xs-6"><input placeholder="Email" class="blogdetail_input_control" name="email" id="email" value=""   type="email" ></div></div>',		
		);
	function my_fields($fields) { 
		return $fields;
	}
	add_filter('comment_form_default_fields','my_fields');
		$defaults = array(
		'fields'=> apply_filters( 'comment_form_default_fields', $fields ),
		'comment_field'=> '<div class="row"><div class="col-xs-12"><textarea placeholder="Message" id="comments" rows="5" class="blogdetail_textarea_control" name="comment"></textarea></div></div>',		
		'logged_in_as' => '<p class="logged-in-as">' . __( "Logged in as ",'webriti' ).'<a href="'. admin_url( 'profile.php' ).'">'.$user_identity.'</a>'. '<a href="'. wp_logout_url( get_permalink() ).'" title="Log out of this account">'.__(" Log out?",'webriti').'</a>' . '</p>',
		'id_submit'=> 'blogdetail_form_btn',
		'label_submit'=>__( 'Post Comment','webriti'),		
		'comment_notes_before'=>'',
		'comment_notes_after'=> '',		
		'title_reply'=> '<div class="webriti_comment_form_title"><h3><i class="fa fa-mail-reply"></i>'.__( 'Leave a Reply','webriti').'</h3>
		</div>',
		'id_form'=> 'commentform'
		);
	comment_form($defaults);?>						
	</div>
<?php endif; // If registration required and not logged in ?>
<?php endif;  ?>