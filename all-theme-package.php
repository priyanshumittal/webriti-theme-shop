<?php //Template Name: All-Theme-Package-Detail ?>
<?php get_header(); ?>
<!-- All Theme Slider Section -->
<div class="all_theme_slider">
	<div class="container">
		<div class="row">
			
			<div class="col-md-12 slide_area text-center">
				<h1>All Theme Package</h1>
				<p>GET EVERY SINGLE WEBRITI THEME !</p>
				<div class="btntop">
                    <a class="home_slide_btn" href="http://www.webriti.com/amember/signup/allthemes">BUY ALL THEME PACKAGE</a>
                </div>
			</div>
			
		</div>
	</div>
</div>

<div class="homepage_main_slide_shadow"></div>

<div class="clearfix"></div>

<!-- Theme Offer Section -->
<div class="container">
	
	<div class="row">
		
		<div class="col-md-8  offer-section">
			<h1>Perfect for <span>Freelancers & Agencies</span></h1>
			<div class="media offer-area">		
				<div class="media-body">
					<p>This ultimate web toolkit helps you jump start your web design process for client work, or eliminates it completely. Chop and change between themes, experiment with modifications and save time, money and energy in getting your business/products online.</p>
					<a href="http://webriti.com/browse-theme/" title="View Our Themes">View Our Themes <i class="fa fa-angle-double-right"></i></a>
				</div>
			</div>
		</div>
		
		<div class="col-md-4">
			<div><img class="img-responsive center-block" src="<?php echo get_template_directory_uri() ?>/images/theme-package.png" title="All Theme Package"></div>
		</div>
		
	</div>


 </div>
<!-- /Theme Offer Section -->

<div class="clearfix"></div>

<!--Theme Price Section-->
<!-- <div class="price-section">
<div class="overlay">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-sm-offset-3">
				<div><a href="#"><img class="img-responsive" src="images/Price-img.png" alt="Theme Package"></a></div>
			</div>		
		</div>
		
	</div>
		
</div>	
</div> -->
<!--/Theme Price Section-->

<div class="clearfix"></div>

<!--Theme Price Section-->
<div class="price-section">
<div class="overlay">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				
				<div class="all-themes-club">			

					<div class="price">
						<p><strong><span>$</span>129</strong></p>
						<p><a class="price_btn" href="http://www.webriti.com/amember/signup/allthemes" title="Buy All Themes Package">Buy All Themes Package</a></p>
					</div>

					<div class="features">
						<ul>
							<li>1 Year Access to all our exsisting themes and  upcoming themes</li>
							<li>1 Year Support access</li>
							<li>1 Year Regular theme updates</li>
							<li>Use on unlimited number of domains.</li>
							<li class="license">Comes with a standard <strong>1 year license</strong> for all themes.<li>
						</ul>
					</div>

				</div>
				
			</div>		
		</div>
		
	</div>
		
</div>	
</div>
<!--/Theme Price Section-->
<div class="container">
<div class="row">
<div class="webriti_heading_title">
				<h1>What You will get in this price?</h1>
				<div id="" class="webriti_separator"></div>
			</div>
	<div class="col-md-4"><i class="fa fa-cloud-download" style="font-size:35px;"></i>
	<h3 style="margin-left:20px;vertical-align:initial;display:inline-block;">1 Year Membership</h3>
	<p style="font-family: OpenSansRegular;font-size: 16px;color: #777777;line-height: 30px;}">During the year you will get access for all the themes which are already released as well for the upcoming theme during the year.</p></div>
	<div class="col-md-4"><i class="fa fa-life-ring" style="font-size:35px;"></i>
	<h3 style="margin-left:20px;vertical-align:initial;display:inline-block;">Regular Support</h3>
	<p style="font-family: OpenSansRegular;font-size: 16px;color: #777777;line-height: 30px;}">You will get support till one year. If you want more support after 1 year than for this you need to renew the membership.</p></div>
	<div class="col-md-4"><i class="fa fa-refresh" style="font-size:35px;"></i>
	<h3 style="margin-left:20px;vertical-align:initial;display:inline-block;">Regular Theme Updates</h3>
	<p style="font-family: OpenSansRegular;font-size: 16px;color: #777777;line-height: 30px;}">You will get all themes updates related to any feature additions, any bug fixes, etc etc during your membership. </p></div>
	</div>
</div>
<!-- Homepage Testimonial Section -->

	<div>
	<div class="row">
			<div class="webriti_heading_title">
				<h1>Happy <span>Customers</span></h1>
				<p>Over 1000 Extremely Satisfied Customers</p>
				<div id="" class="webriti_separator"></div>
			</div>
		</div>
	<?php get_template_part('index', 'testimonial'); ?>
</div>
	
<!-- /Homepage Testimonial Section -->


<!-- Footer Callout Section -->
<div class="footer_callout_area">
	<div class="container">
		<div class="row">
			<div class="col-md-9">		
			<h2>Get All Our Themes For Only $129!</h2>
		</div>
		<div class="col-md-3">
			<a class="qua_callout_btn" href="http://www.webriti.com/amember/signup/allthemes">Purchase Now <i class="fa fa-long-arrow-right"></i></a>
		</div>
		</div>
	</div>
</div>
<!-- /Footer Callout Section -->
<?php get_footer(); ?>