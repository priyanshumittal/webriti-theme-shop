<?php //Template Name: Custom Instagram Feed Pro?>
<?php get_header(); ?>
<!-- Homepage Slider Section -->
<div class="themedetail_main_slider">
	<div class="container">
		<div class="row">
			
			<div class="col-md-7">
			
				<a href=""><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/instagram_feed_pro_slide.png"></a>
			
			</div>
			
			<div class="col-md-5 themedetail_data">
				<h2>Custom Instagram Feed Pro in $29</h2>
				<p>Powerful plugin to integrate instagram feeds to your WordPress sites.</p>
				<div class="themedetail_btntop">
                    <a class="themedetail_slide_btn" target="_blank" href="http://webriti.com/easy-instagram-feed-demo">View Demo</a><span>or</span>
					<a class="buy_theme_btn" href="#myModal"  data-toggle="modal" style="color:#fff">Buy Now</a>
                </div>
			</div>
			<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery(".close").click(function(){
				jQuery("#myModal").hide();
			});
		});
	</script>
	<style>.homebtn {
		padding: 5px 22px !important;
		font-family: 'OpenSansBold';
		margin-bottom: 7px !important;
		color: hsl(0, 100%, 100%);
		text-shadow: none;
		background-color: hsl(0, 81%, 44%);
		background-repeat: repeat-x;
		border-color: hsl(103, 50%, 54%) hsl(103, 50%, 54%) hsl(0, 0%, 18%);
		border-color: hsla(0, 0%, 0%, 0.1) hsla(0, 0%, 0%, 0.1) hsla(0, 0%, 0%, 0.25);
		display: inline-block;
		text-align: center;
		text-decoration: none;
		vertical-align: middle;
		cursor: pointer;
		font-size: 18px;
		line-height: 30px;
		border: 1px solid hsl(0, 0%, 80%);
		border-color: hsl(0, 0%, 90%) hsl(0, 0%, 90%) hsl(0, 0%, 75%);
		border-color: hsla(0, 0%, 0%, 0.1) hsla(0, 0%, 0%, 0.1) hsla(0, 0%, 0%, 0.25);
		border-bottom-color: hsl(0, 0%, 70%);
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		border-radius: 4px;
		-webkit-box-shadow: inset 0 1px 0 hsla(0, 100%, 100%, 0.2), 0 1px 2px hsla(0, 0%, 0%, 0.05);
		-moz-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
		box-shadow: inset 0 1px 0 hsla(0, 100%, 100%, 0.2), 0 1px 2px hsla(0, 0%, 0%, 0.05);
		}
		.homebtn:hover{ text-decoration:none; color:#fff;}
		
		.insta_video {
			height: 100%;
			padding: 50px 0;
			overflow: hidden;
			padding: 0;
			width: 100%;
			margin: 60px 0 0;
		}
		.insta_video .overlay{
			background: #f5f5f5;
			height: 100%;
			padding: 74px 0 83px;
			position: relative;
			width: 100%;
		}
		.insta_video .themedetail_heading_title h2 {
			color: #242323;
		}
		.insta_video .insta_video_frame {
			width: 100%;
			height: 576px;
			border: solid 1px #dedede;
			padding: 3px;
			border-radius: 2px;
		}
		.testimonial-padding{
			padding: 60px 0 0;
		}
	</style>
			 <div id="myModal" class="modal  fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="background:transparent;border:0px;display:none;">
		
  		<div class="modal-body" align="center" style="margin-top:5%">
		
  			<div class="busiprof-pro-modal" style="background:#FFFFFF; width:290px;border:2px solid #ddd;webkit-box-shadow: 0 5px 15px rgba(0,0,0,.5);box-shadow: 0 50px 50px rgba(0,0,0,0.5);" >
  			<button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-right: -19px; margin-top: -16px; opacity:1">
		   <img src="<?php echo get_template_directory_uri(); ?>/images/close_256.png">
		</button>	
				<div id="title"><h4 style="background:#6BB3D1;margin-top:0%;padding-bottom:25px;padding-top:7px;color:#FFFFFF;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;">Custom Instagram Feed Pro</h4>
  					<div class="rate" style="display: block;font: bold 25px/62px Georgia, Serif;color: #777;
background: #CFEEFC;border: 5px solid #fff;height: 68px;width: 68px;margin: -31px auto 5px;
-moz-border-radius: 100px;-webkit-border-radius: 100px;border-radius: 5200px;-moz-box-shadow: 0 5px 20px #ddd inset, 0 3px 0 #999 inset;-webkit-box-shadow: 0 5px 20px #ddd inset, 0 3px 0 #999 inset;box-shadow: 0 5px 20px #ddd inset, 0 3px 0 #999 inset;">
                        <span>$</span><strong>29</strong>
                     </div>
    			</div>
   				<h5 style="color:#F22853;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;">KEY FEATURES</h5>
                <table class="table table-hover" >
                   
                    <tr><td style="text-align:center;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:14px;">
                    <strong>  Use on Unlimited Websites</strong>
                    </td></tr>
                    <tr><td style="text-align:center;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:14px;">
                    <strong>1 year of Support & Updates </strong>
                    </td></tr>
					  <tr><td style="text-align:center;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:14px;">
                    <strong>Fast & Friendly Support</strong>
                    </td></tr>
					
                </table>
    			<center>
    			<a href="http://www.webriti.com/amember/signup/custominstagramfeed"  class="homebtn"><strong>Buy Now</strong></a>
    			</center><br />
    		</div>
  		</div> 
	</div>
			
		</div>
	</div>
</div>
<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery(".close").click(function(){
				jQuery("#myModal").hide();
			});
		});
	</script>
	<style>.homebtn {
		padding: 5px 22px !important;
		font-family: 'OpenSansBold';
		margin-bottom: 7px !important;
		color: hsl(0, 100%, 100%);
		text-shadow: none;
		background-color: hsl(0, 81%, 44%);
		background-repeat: repeat-x;
		border-color: hsl(103, 50%, 54%) hsl(103, 50%, 54%) hsl(0, 0%, 18%);
		border-color: hsla(0, 0%, 0%, 0.1) hsla(0, 0%, 0%, 0.1) hsla(0, 0%, 0%, 0.25);
		display: inline-block;
		text-align: center;
		text-decoration: none;
		vertical-align: middle;
		cursor: pointer;
		font-size: 18px;
		line-height: 30px;
		border: 1px solid hsl(0, 0%, 80%);
		border-color: hsl(0, 0%, 90%) hsl(0, 0%, 90%) hsl(0, 0%, 75%);
		border-color: hsla(0, 0%, 0%, 0.1) hsla(0, 0%, 0%, 0.1) hsla(0, 0%, 0%, 0.25);
		border-bottom-color: hsl(0, 0%, 70%);
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		border-radius: 4px;
		-webkit-box-shadow: inset 0 1px 0 hsla(0, 100%, 100%, 0.2), 0 1px 2px hsla(0, 0%, 0%, 0.05);
		-moz-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
		box-shadow: inset 0 1px 0 hsla(0, 100%, 100%, 0.2), 0 1px 2px hsla(0, 0%, 0%, 0.05);
		}
		.homebtn:hover{ text-decoration:none; color:#fff;}
		table tr td { color :#000;} 
	</style>
<div id="myModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="background:transparent;border:0px;display:none;">
		<div class="modal-header" style="border:0px;">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true" ><img src="<?php echo get_stylesheet_directory_uri().'/images/close_256.png'?>" /></button>
    		<h4 id="myModalLabel"></h4>
  		</div>
  		<div class="modal-body" align="center">
  			<div class="spasalon-pro-modal" style="background:#FFFFFF; width:290px;border:2px solid #ddd;webkit-box-shadow: 0 5px 15px rgba(0,0,0,.5);box-shadow: 0 50px 50px rgba(0,0,0,0.5);" >
  				<button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-right: -19px;
margin-top: -16px; opacity:1"><img src="<?php echo get_template_directory_uri(); ?>/images/close_256.png"></button>
				<div id="title"><h4 style="background:#6BB3D1;margin-top:0%;padding-bottom:25px;padding-top:7px;color:#FFFFFF;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;">SpaSalon-Pro</h4>
  					<div class="rate" style="display: block;font: bold 25px/62px Georgia, Serif;color: #777;
background: #CFEEFC;border: 5px solid #fff;height: 68px;width: 68px;margin: -31px auto 5px;
-moz-border-radius: 100px;-webkit-border-radius: 100px;border-radius: 5200px;-moz-box-shadow: 0 5px 20px #ddd inset, 0 3px 0 #999 inset;-webkit-box-shadow: 0 5px 20px #ddd inset, 0 3px 0 #999 inset;box-shadow: 0 5px 20px #ddd inset, 0 3px 0 #999 inset;">
                        <span>$</span><strong>49</strong>
                     </div>
    			</div>
   				<h5 style="color:#F22853;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;">KEY FEATURES</h5>
                <table class="table table-hover" >
                    <tr ><td style="text-align:center;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:14px;">
                   <strong> Responsive Design</strong>
                    </td></tr>
                    <tr><td style="text-align:center;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:14px;">
                    <strong>06 Page Templates</strong> 
                    </td></tr>
                    <tr><td style="text-align:center;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:14px;">
                   <strong> Custom Post Types & Custom Widgets</strong>
                    </td> </tr>
                    <tr><td style="text-align:center;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:14px;">
                    <strong>  Unlimited Domain Usage</strong>
                    </td></tr>
                    <tr><td style="text-align:center;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:14px;">
                    <strong>1 year of Support & Updates </strong>
                    </td></tr>
                </table>
    			<center>
    			<a href="http://www.webriti.com/amember/signup/index"  class="homebtn"><strong>Buy Now</strong></a>
    			</center><br />
    		</div>
  		</div> 
	</div>
<div class="homepage_main_slide_shadow"></div>
<!-- /Homepage Slider Section -->

<!-- Plugin Features Section -->
<div class="container instagram-feed">	
	<div class="row">
		<div class="col-md-6">
		<h2><i class="fa fa-cloud"></i>Powerful Customization Settings</h2>
		<ul>
			<li><p>Customize instagram feed area by specifying <b>width</b>, <b>height</b> and <b>background color</b>.</p></li>
			<li><p>Specify number of <b>photos</b> to show on feed area.</p></li>
			<li><p>Set the <b>order</b> in which you want to display feeds.</p></li>
			<li><p>Set column <b>layout</b> to show your feeds.</p></li>
			<li><p>Set image <b>resolution</b> and <b>padding</b> around the images.</p></li>
			<li><p>Option to disable <b>light box</b>.</p></li>
			<li><p>Option to disable <b>header</b>.</p></li>
			<li><p>Easily control <b>caption</b> by specifying <b>text length</b>, <b>color</b> and <b>font size</b>.</p></li>
			<li><p>Easily Control Feed's <b>Likes</b> and <b>Comments</b>.</p></li>
			<li><p>Configure <b>load more</b> and <b>follow on instagram</b> buttons by specifying <b>background color</b>, <b>text color</b> and <b>button text</b>.</p></li>
			<li><p>Easily specify your <b>custom js</b> / <b>css scripts</b> to customize specific feed items.</p></li>
		</ul>
		</div>
		<div class="col-md-6">
		<h2><i class="fa fa-rocket"></i>Display Instagram feeds</h2>
		<ul>
			<li><p>Display <b>photos</b> / <b>videos</b> from instagram account.</p></li>
			<li><p>Feed stream is <b>responsive ready</b> for all devices.</p></li>
			<li><p>Display multiple feeds on <b>same page</b> or on <b>different pages</b>.</p></li>
			<li><p>Show feeds from your <b>User ID</b> or <b>Hashtag</b>.</p></li>
			<li><p>Only show feeds from your User ID and <b>Filter by a specific Hashtag</b>.</p></li>
			<li><p>Only show feeds from <b>Common Hashtag</b></b>.</p></li>
			<li><p><b>Lightbox</b> popup support.</p></li>
			<li><p>Share you feeds on other <b>social media platforms</b>.</p></li>
			<li><p>Display <b>likes</b> and <b>comments</b>.</p></li>
			<li><p>Use number of built-in <b>shortcode</b> options.</p></li>
			<li><p>Load as many media items you want with the help of <b>Load More button</b>.</p></li>
			<li><p>Display <b>photo caption</b>.</p></li>
		</ul>
		</div>				
	</div>
	
 </div>
<!-- /Plugin Features Section -->
<!-- Instagram Video Section -->
<div class="insta_video">
	<div class="overlay">
		<div class="container">
		
			<div class="row">
				<div class="themedetail_heading_title">
					<h2>Watch a Video</h2>
					<div id="" class="themedetail_separator"></div>
				</div>
			</div>
			
			<div class="row package">
				<div class="col-md-12 col-sm-12">
					<iframe class="insta_video_frame" src="https://www.youtube.com/embed/BBbg3etugX8" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</div>
 </div>
<!-- /Instagram Video Section -->

<!--Theme Detail Testimonial Section-->
<?php get_template_part('index', 'instagram-testimonial'); ?>
<!--/Theme Detail Testimonial Section-->
<?php get_footer(); ?>
