<?php //Template Name: Appointment-Theme-Detail ?>
<?php get_header(); ?>
<!-- Homepage Slider Section -->
<div class="themedetail_main_slider">
	<div class="container">
		<div class="row">
			
			<div class="col-md-7">
				<a href=""><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/appointment_theme_slide.png"></a>
			</div>
			
			<div class="col-md-5 themedetail_data">
				<h2>Appointment - Pro</h2>
				<p>Appointment is an incredibly multipurpose responsive theme coded & designed with a lot of care and love. You can use it for your business, portfolio, blogging or any type of site.</p>
				<div class="themedetail_btntop smooth-scroll">
          <a class="themedetail_slide_btn section-scroll" href="#demos">View Demo</a><span>or</span>
					<a class="buy_theme_btn appoint_pro-freemius" style="color:#fff">Buy Now</a>
        </div>
			</div>
			
		</div>
	</div>
</div>

<div class="homepage_main_slide_shadow"></div>
<!-- /Homepage Slider Section -->

<!--Theme Funfact-->
<div class="funfact bg-grey">
    <div class="container">
        <div class="row ">
            <div class="col-lg-4 col-sm-6 p-all-0">
                <div class="funfact-inner text-center">
                    <figure class="funfact-icon">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/changelog-icon.png" alt="Changelog">
                    </figure>
                    <h5 class="funfact-title"><a href="https://webriti.com/appointment-pro-changelog/" target="_blank">CHANGELOG</a></h5>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 p-all-0">
                <div class="funfact-inner text-center">
                    <figure class="funfact-icon">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/support-icon.png" alt="Free vs Pro">
                    </figure>
                    <h5 class="funfact-title"><a href="https://users.freemius.com/login" target="_blank">THEME SUPPORT</a></h5>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 p-all-0">
                <div class="funfact-inner text-center">
                    <figure class="funfact-icon">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/doc-icon.png" alt="Documentation">
                    </figure>
                    <h5 class="funfact-title"><a href="https://help.webriti.com/category/themes/appointment/" target="_blank">DOCUMENTATION</a></h5>
                </div>
            </div>

        </div>
    </div>
</div>
<!--/Theme Funfact-->

<!-- Theme Features Section -->
<div class="container">
	
	<div class="row">
		<div class="themedetail_heading_title">
			<h2>Our Unique Theme Features</h2>
			<div id="" class="themedetail_separator"></div>
		</div>
	</div>
	
	<div class="row">
		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-tablet"></i>
				</div>
				<div class="media-body">
					<h3>Reponsive Layout</h3>
					<p>Our all Themes are Mobile friendly and easily adapts the various screen sizes.</p>
				</div>
			</div>
		</div>		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-cog"></i>
				</div>
				<div class="media-body">
					<h3>Theme Options</h3>
					<p>Theme provides Theme Options Panel, for customizing the the theme.</p>
				</div>
			</div>
		</div>		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-thumbs-o-up"></i>
				</div>
				<div class="media-body">
					<h3>Friendly Support</h3>
					<p>Our great support team is ready to help. Our clients are valuable for us.</p>
				</div>
			</div>
		</div>		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-language"></i>
				</div>
				<div class="media-body">
					<h3>Translation Ready</h3>
					<p>Themes our translation ready you can translate theme in your own language.</p>
				</div>
			</div>
		</div>		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-globe"></i>
				</div>
				<div class="media-body">
					<h3>Browser Compatibility</h3>
					<p>Themes our cross browser compatible. Theme supports all modern browser. </p>
				</div>
			</div>
		</div>		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-flag"></i>
				</div>
				<div class="media-body">
					<h3>Font Awesome Icons</h3>
					<p>Easily style icon color, size, shadow, and anything that's possible with CSS.</p>
				</div>
			</div>
		</div>		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-sliders"></i>
				</div>
				<div class="media-body">
					<h3>Custom Widgets</h3>
					<p>Theme has custom widgets to add in top header sidebar and below the slider in Homepage.</p>
				</div>
			</div>
		</div>		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-file"></i>
				</div>
				<div class="media-body">
					<h3>Page Templates</h3>
					<p>There are 32 different page templates in the theme. </p>
				</div>
			</div>
		</div>		
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-search"></i>
				</div>
				<div class="media-body">
					<h3>SEO Friendly urls</h3>
					<p>Option provided to rename the custom post types in order to create seo friendly urls.</p>
				</div>
			</div>
		</div>
    	<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-header"></i>
				</div>
				<div class="media-body">
					<h3>Header Variations</h3>
					<p>There are various header variations in the theme like Classic Header, Overlap Header and more.</p>
				</div>
			</div>
		</div>	
    	<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-bars"></i>
				</div>
				<div class="media-body">
					<h3>Section Variations</h3>
					<p>There are various section variations of the business template in the theme like Service section variations.</p>
				</div>
			</div>
		</div>	
    	<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-search-plus"></i>
				</div>
				<div class="media-body">
					<h3>Search Effects</h3>
					<p>There are 3 options available in our theme for search: Toggle, Pop up light and Pop up dark.</p>
				</div>
			</div>
		</div>	
		<div class="col-md-4 col-md-6 theme_feature_area">
			<div class="media">
				<div class="theme_feature_icon">
					<i class="fa fa-line-chart"></i>
				</div>
				<div class="media-body">
					<h3>Performance Module</h3>
					<p>Host Google fonts locally which helps to make your site faster.</p>
				</div>
			</div>
		</div>
	</div>
 </div>
<!-- /Theme Features Section -->
<div class="themedatail_testimonial_section">
	<?php get_template_part('index', 'testimonial'); ?>
</div>

<div class="themedetail_demo_buttons" id="demos">
<div class="container">
		<div class="row">
        <div class="themedetail_heading_title">
			        <h2>Our Theme Demos</h2>
			        <div id="" class="themedetail_separator"></div>
      </div>
        
			<div class="col-md-6">
        <div>
          <a target="_blank" href="https://demo.webriti.com/?theme=Appointment%20Pro"><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/atd-light-demo-mockup.png"></a>
        </div>
        <br/>
        <div class="demobtn1">
             <a class="themedetail_slide_btn" target="_blank" href="https://demo.webriti.com/?theme=Appointment%20Pro">Light Demo</a>
			  </div>
      </div>
      
      <div class="col-md-6">
        <div>
           <a target="_blank" href="https://appointment-dark-pro.webriti.com/"><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/atd-dark-demo-mockup.png"></a>
        </div>
         <br/>
        <div class="demobtn2">
             <a class="themedetail_slide_btn" target="_blank" href="https://appointment-dark-pro.webriti.com/">Dark Demo</a>
        </div>
			</div>
      
   </div>
</div>
</div>

<!-- Showcase Section -->
<div class="themedatail_showcase_section">
	<?php get_template_part('index', 'showcase'); ?>
</div>
<!-- /Showcase Section -->

<div class="ap-starter-sites">
	<div class="container">
	<div class="row">
			<div class="showcase_heading_title">
		        <h2>Ready to Import Starter Sites</h2>
		        <div id="" class="themedetail_separator"></div>
      		</div>
				<div class="col-md-4">
					<div class="app-demo">
						<div class="app-demo-wrapper">
							<a target="_blank" href="<?php echo esc_url('https://demo-appointment.webriti.com/demo-two/');?>">
								<div class="ap-demo-img">
									<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/gutenberg-demos/ap-demo-two.jpg" alt="">
								</div>
								<div class="ap-demo-overlay">
									<i class="fa-solid fa-eye"></i>
								</div>
							</a>
							<div class="ap-demo-link">
								<a target="_blank" href="<?php echo esc_url('https://demo-appointment.webriti.com/demo-two/');?>">Gutenberg</a>
							</div>
						</div>
						<div class="ap-demo-title">
							<h4 class="ap-demo-head">GrowFitz</h4>
							<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/gutenberg-icon.png" alt="">
						</div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="app-demo">
						<div class="app-demo-wrapper pro">
							<a target="_blank" href="<?php echo esc_url('https://demo-appointment.webriti.com/demo-pro-two/');?>">
								<div class="ap-demo-img">
									<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/gutenberg-demos/ap-demo-pro-two.jpg" alt="">
								</div>
								<div class="ap-demo-overlay">
									<i class="fa-solid fa-eye"></i>
								</div>
							</a>
							<div class="ap-demo-link">
								<a target="_blank" href="<?php echo esc_url('https://demo-appointment.webriti.com/demo-pro-two/');?>">Gutenberg</a>
							</div>
						</div>
						<div class="ap-demo-title">
							<h4 class="ap-demo-head">Business</h4>
							<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/gutenberg-icon.png" alt="">
						</div>
					</div>
				</div>
				
				<div class="col-md-4">
					<div class="app-demo">
						<div class="app-demo-wrapper pro">
							<a target="_blank" href="<?php echo esc_url('https://ap-architect.webriti.com/');?>">
								<div class="ap-demo-img">
									<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/Elementor-demos/ap-demo-architect.jpg" alt="">
								</div>
								<div class="ap-demo-overlay">
									<i class="fa-solid fa-eye"></i>
								</div>
							</a>
							<div class="ap-demo-link">
								<a target="_blank" href="<?php echo esc_url('https://ap-architect.webriti.com/');?>">Elementor</a>
							</div>
						</div>
						<div class="ap-demo-title">
							<h4 class="ap-demo-head">Architect</h4>
							<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/elementor-icon.png" alt="">
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="app-demo">
						<div class="app-demo-wrapper pro">
							<a target="_blank" href="<?php echo esc_url('https://ap-corporate.webriti.com/');?>">
								<div class="ap-demo-img">
									<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/Elementor-demos/ap-demo-corporate.jpg" alt="">
								</div>
								<div class="ap-demo-overlay">
									<i class="fa-solid fa-eye"></i>
								</div>
							</a>
							<div class="ap-demo-link">
								<a target="_blank" href="<?php echo esc_url('https://ap-corporate.webriti.com/');?>">Elementor</a>
							</div>
						</div>
						<div class="ap-demo-title">
							<h4 class="ap-demo-head">Corporate</h4>
							<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/elementor-icon.png" alt="">
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="app-demo">
						<div class="app-demo-wrapper pro">
							<a target="_blank" href="<?php echo esc_url('https://demo-appointment.webriti.com/demo-pro-four/');?>">
								<div class="ap-demo-img">
									<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/gutenberg-demos/ap-demo-pro-four.jpg" alt="">
								</div>
								<div class="ap-demo-overlay">
									<i class="fa-solid fa-eye"></i>
								</div>
							</a>
							<div class="ap-demo-link">
								<a target="_blank" href="<?php echo esc_url('https://demo-appointment.webriti.com/demo-pro-four/');?>">Gutenberg</a>
							</div>
						</div>
						<div class="ap-demo-title">
							<h4 class="ap-demo-head">Digital Agency</h4>
							<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/gutenberg-icon.png" alt="">
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="app-demo">
						<div class="app-demo-wrapper">
							<a target="_blank" href="<?php echo esc_url('https://ap-restaurants.webriti.com/');?>">
								<div class="ap-demo-img">
									<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/Elementor-demos/ap-demo-restaurant.jpg" alt="">
								</div>
								<div class="ap-demo-overlay">
									<i class="fa-solid fa-eye"></i>
								</div>
							</a>
							<div class="ap-demo-link">
								<a target="_blank" href="<?php echo esc_url('https://ap-restaurants.webriti.com/');?>">Elementor</a>
							</div>
						</div>
						<div class="ap-demo-title">
							<h4 class="ap-demo-head">Restaurant</h4>
							<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/elementor-icon.png" alt="">
						</div>
					</div>
				</div>
			</div>
			<div class="ap-starter-sites-btn"> 
				<a href="https://webriti.com/appointment-demos/">MORE TEMPLATES <i class="fa-solid fa-arrow-right"></i></a>
			</div>
	</div>
</div>


<!--Theme Detail Image Section -->
<div class="container-fluid">
	<div class="row">
		<div class="themedetail_img_heading_title">
			<h2>Our Unique Theme Features</h2>
			<div class="direction_arrow">
				<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/arrow.png">
			</div>
		</div>
	</div>	
	<div class="row">
    <div class="col-md-6">
			<img class="img-responsive" title="Appointment Theme Detail 1" src="<?php echo get_template_directory_uri() ?>/images/appointment/atd-full-detail-one.jpg">
    </div>
    <div class="col-md-6">
			<img class="img-responsive" title="Appointment Theme Detail 2" src="<?php echo get_template_directory_uri() ?>/images/appointment/atd-full-detail-two.jpg">
    </div>
    <div class="col-md-12 themedetail_image">
			<img class="img-responsive" title="Appointment Theme Detail 3" src="<?php echo get_template_directory_uri() ?>/images/appointment/appointment-full-detail-page-3.jpg">
    </div>
	</div>		
</div>
<!-- /Theme Detail Image Section -->
<script>
  jQuery('.section-scroll').bind('click', function(e) {
            var anchor = jQuery(this);
            jQuery('html, body').stop().animate({
                scrollTop: jQuery(anchor.attr('href')).offset().top - 70
            }, 1000);
            e.preventDefault();
});
</script>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="https://checkout.freemius.com/checkout.min.js"></script>
<script>
    var handler = FS.Checkout.configure({
        plugin_id:  '15033',
        plan_id:    '25026',
        public_key: 'pk_38814482a8099f12dafa59aced4e1',
        image:      'https://your-plugin-site.com/logo-100x100.png'
    });
    
    $('.appoint_pro-freemius').on('click', function (e) {
        handler.open({
            name     : 'Appointment Pro Bundle',
            licenses : 1,
            // You can consume the response for after purchase logic.
            purchaseCompleted  : function (response) {
                // The logic here will be executed immediately after the purchase confirmation.                                // alert(response.user.email);
            },
            success  : function (response) {
                // The logic here will be executed after the customer closes the checkout, after a successful purchase.                                // alert(response.user.email);
            }
        });
        e.preventDefault();
    });
</script>
<?php get_footer(); ?>