<?php
/**
 * Template Name:Facebook Feed Demo Template

 */
 
 get_header();
?> 
<div class="container">
<div class="row">
<div class="webriti_page_heading">
<h1>Easy<span> Facebook Feed </span>Demo</h1>
<p></p>
<div class="page_separator"></div>
</div>
</div>
</div>
<div class="container">
<div class="col-md-8">
<?php 
echo do_shortcode('[facebook-feed]');
$design_setting_array = get_option ('wff_design_settings');
$default_feed_layout = $design_setting_array['wff_layout_type'];

?>
</div>

<div class="col-md-4">
<div class="webriti_sidebar_section"><div class="webriti_sidebar_widget">
<div class="webriti_sidebar_widget_title"><h2>Manage Feed Options:</h2>
<div class="webriti-separator-small"></div></div>
<div id="search_form">
	<form action="" method="post" id="form" class="">
		
	
				<div >
				
					<div style="margin-bottom:15px">
						
						<label for="user_id">Page ID:</label>
						<input type="text" name="id" id="id" placeholder="Eg: TheNaturalWorld" tabindex="2" style="width: 140px;">
            <input type="hidden" name="default_page_layout" id="default_page_layout" value="<?php echo $default_feed_layout; ?>">
            
					</div>
          
				<div style="margin-bottom:15px">
						
						<label for="feed_layout">Select Feed Layout:</label><br>
						<input type="radio" name="page_layout" id="thumbnail" value="thumbnail">
          	<span>Thumbnail</span><br>
            <input type="radio" name="page_layout" id="halfwidth" value="halfwidth">
            <span>Halfwidth</span><br>
          	<input type="radio" name="page_layout" id="fullwidth" value="fullwidth">
          <span>Fullwidth</span>
					</div>
					
				</div>
		     
    <input type="submit" value="View Feed" tabindex="11" style="float: left;">
				   
	
	</form>
</div>
	
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>

	<script type="text/javascript">
    
    var layout = $('input:radio[name=page_layout]:checked').val();
			
	    	$('input:radio[name=page_layout]').on('change', function(){
	    		layout = $(this).val();
				
	    	});
    
    
    var default_layout = $('#default_page_layout').val();
    //alert(default_layout);
    
    if(layout == undefined)
      {
      	layout = default_layout;
       // alert(layout);
      }
    
     if ( default_layout == 'thumbnail' ){
	        	$('input:radio[name=page_layout]#thumbnail').prop('checked', true);
	        	if(default_layout == false) default_layout = 'thumbnail';
	        	$('#thumbnail').val( default_layout );
         }
         else if ( default_layout == 'fullwidth' ){
	        	$('input:radio[name=page_layout]#fullwidth').prop('checked', true);
	        	if(default_layout == false) default_layout = 'fullwidth';
	        	$('#fullwidth').val( default_layout );
           }
         else if ( default_layout == 'halfwidth' ){
	        	$('input:radio[name=page_layout]#halfwidth').prop('checked', true);
	        	if(default_layout == false) default_layout = 'halfwidth';
	        	$('#halfwidth').val( default_layout );
           }
    
    $(document).ready(function(){
      
    $("#form").submit(function(){
      
      token = '1505917499687703|2133Lp1cLt6Zk0N2por8X8QJf9k';
      
      var id_string = $('#id').val();
      if(id_string == '')
        {
        id_string = 'TheNaturalWorld';
        
        }
      
      $(this).attr( 'action', '?page_id=1660&id=' + id_string +  '&token=' + token+ '&layout='+layout );
    });
      
      var cur_id = getQueryVariable('id'),
		cur_token = getQueryVariable('token'),
     page_layout = getQueryVariable('layout');
     
	   
		if ( cur_id != false ) $('#id').val( cur_id );

		if ( cur_token != false ) $('#token').val( cur_token );
      
   if( page_layout != false ) $ ('#page_layout').val( page_layout );
      
       if ( page_layout == 'thumbnail' ){
	        	$('input:radio[name=page_layout]#thumbnail').prop('checked', true);
	        	if(page_layout == false) page_layout = 'thumbnail';
	        	$('#thumbnail').val( page_layout );
         }
         else if ( page_layout == 'fullwidth' ){
	        	$('input:radio[name=page_layout]#fullwidth').prop('checked', true);
	        	if(page_layout == false) page_layout = 'fullwidth';
	        	$('#fullwidth').val( page_layout );
           }
         else if ( page_layout == 'halfwidth' ) {
	        	$('input:radio[name=page_layout]#halfwidth').prop('checked', true);
	        	if(page_layout == false) page_layout = 'halfwidth';
	        	$('#halfwidth').val( page_layout );
           }
		function getQueryVariable(variable){
			var query = window.location.search.substring(1);
			var vars = query.split("&");
			for (var i=0;i<vars.length;i++) {
				var pair = vars[i].split("=");
				if(pair[0] == variable){return pair[1];}
		}
		return(false);
		}
  

			});
			
		</script>
</div>
<div class="clear"></div>
</div></div></div>
<?php get_footer();?>