$(function() {
			//	This js For Homepage Testimonial Section
			$('#hc_testimonial').carouFredSel({
				width: '100%',
				responsive : true,
				circular: true,
				prev: '#prev3',
				next: '#next3',
				directon: 'left',
				auto: true,
				scroll : {
						items : 1,
						duration : 1200,
						timeoutDuration : 1200
					},
			});
			
			$('#webriti-testimonial').carouFredSel({
				//width: '100%',
				responsive : true,
				circular: true,
				pagination: "#pager2",
				
				 items: {
                        visible: {
                            min: 1,
                            max: 2
                        }
                    },

				prev: '#prev3',
				next: '#next3',
				directon: 'left',

				auto: true,
				 scroll : {
						items : 1,
						duration : 1500,
						//fx:"uncover-fade",
						//easing: "elastic",
						timeoutDuration : 1500
					},

			});
			
		});