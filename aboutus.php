<?php //Template Name: ABout Us 
get_header(); ?>
<div class="container">
	<div class="row">
		<div class="webriti_page_heading">
			<h1>About <span>Webriti</span></h1>
			<p>We're a group of passionate designers and developers.</p>
			<div class="page_separator" id=""></div>
		</div>
	</div>
</div>
<!-- About Us Section -->
<div class="container">


	<div class="row hc_aboutus_area">
		<div class="col-md-6">
			<img src="<?php echo get_template_directory_uri() ?>/images/about.jpg" alt="Webriti" class="img-responsive hc_img_shadow">
		</div>
		
		<div class="col-md-6">
			<h3>How We Work...</h3>
			<p>We are a small team of WordPress experts. We love creating beautiful and feature rich WordPress themes and Plugins.</p>
			
		</div>	
	</div>
	
	<div class="row">
		<div class="team_heading_title">
			<h2>Meet Our Great Team</h2>
			<div class="team_separator" id=""></div>
		</div>
	</div>
	
	<div class="row webriti_team_section">
		<div class="col-md-4 col-md-6">
			<div class="webriti_team_about_showcase">
				<img class="img-circle" alt="Webriti" src="<?php echo get_template_directory_uri() ?>/images/ankit_sir.jpg">
			<div class="caption">
				<h3>Ankit Agrawal</h3>
				<h6>(Founder)</h6>
			</div>
			</div>
		</div>
		
		<div class="col-md-4 col-md-6">
			<div class="webriti_team_about_showcase">
				<img class="img-circle" alt="Webriti" src="<?php echo get_template_directory_uri() ?>/images/priyanshu_sir.jpg">
			<div class="caption">
				<h3>Priyanshu Mittal</h3>
				<h6>(Founder)</h6>
			</div>
			</div>
		</div>
		
		
		<div class="col-md-4 col-md-6">
			<div class="webriti_team_about_showcase">
				<img class="img-circle" alt="Webriti" src="<?php echo get_template_directory_uri() ?>/images/mouse.jpg">
			<div class="caption">
				<h3>Stuart</h3>
				<h6>(Our Landlord)</h6>
			</div>
			</div>
		</div>
		
	</div>
	

</div>
<!-- /About Us Section -->
<?php get_footer(); ?>
