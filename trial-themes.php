<?php 
/*
* Template Name: Trial Themes
*/
?>
<?php get_header();?>	
<style>
.super-screenshots {

height: 284px;
}

.browse_themes:hover{
background: url("<?php echo get_stylesheet_directory_uri(). '/images/magnify.png' ?>") no-repeat scroll center center #ccc; 
}
#extra_style:hover{margin-bottom:13px;}
</style>
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(). '/css/style.css' ?> ">

<body class=" flatpack">
	
<!-- Header top Slide -->
<div class="container">
	<div class="row">
		<div class="webriti_page_heading">
			<h1>Free <span>WordPress</span> Themes</h1>
			<p>Our collection features Free/Premium WordPress Themes for bloggers, businesses, medical professions, freelancers etc etc.</p>
			<div class="page_separator" id=""></div>
		</div>
	</div>
</div>
<!-- /Header top Slide -->



<!-- Container -->

	<div class="container">
		
		
		<div class="row">
		
			<div class="col-md-4">
			<div >
			<h3 > SpaSalon</h3></div>
			<div class="browse_themes">
			<div class="super-screenshots">
			
				<a class="hover_thumb" target="_blank" href="http://webriti.com/free-trial/spasalon.1.4.4.4.zip"><img style="width: 100%" src="<?php echo get_stylesheet_directory_uri(). '/images/spasalon-trial-themes.png' ?> "></a>
				
				</div>
				</div>
				<p>A responsive theme for SPA SALON and BEAUTY SALON type of business that uses multiple nav menus, Right-sidebar, Featured Slider.</p>
			</div>
			<div class="col-md-4" >
			<div><h3>Busiprof</h3></div>
			<div class="browse_themes">
			<div class="super-screenshots">
			
				<a class="hover_thumb"  target="_blank" href="http://webriti.com/free-trial/busiprof.1.4.2.4.zip">
				<img style="width: 100%;height:284px;" src="<?php echo get_stylesheet_directory_uri(). '/images/Busiprof_Themeshop_Browse.jpg' ?> "></a>
				</div>
				
			</div>
			<p>A Beautiful and FLexible Responsive Businees Theme. It is ideal for creating a corporate website. It boasts of a highy functional Home Page</p>
			</div>
			
			<div class="col-md-4">
			<h3>Rambo</h3>
			<div class="browse_themes" id="extra_style">
				<div class="super-screenshots">
				<a class="hover_thumb" target="_blank" href="http://webriti.com/free-trial/rambo.1.2.1.zip"><img style="width: 100%;height:284px;" src="<?php echo get_stylesheet_directory_uri(). '/images/Home_Themeshop600.jpg' ?> "></a>	
				</div>
			</div>
			<p>Rambo is a Responsive Multi-Purpose Wordpress Theme. It is ideal for creating a corporate website. It boasts of a highy functional Home Page .Very Cleaned Design. 
			</p>			
			</div><div class="clearfix"></div>
			<div class="col-md-4">
				<h3>Health Centre</h3>
				<div class="browse_themes">
				<div class="super-screenshots">
				<a class="hover_thumb" target="_blank" href="http://webriti.com/free-trial/health-center-lite.1.1.4.2.zip"><img style="width: 100%;height:284px;" src="<?php echo get_stylesheet_directory_uri(). '/images/health-centre-trial-themes.jpg' ?> "></a>	
				</div>
				</div>
				<p>A Free Blue coloured Business/Health/Fitness theme that supports Primary menu's , Primary sidebar,Four widgets area at the footer region  etc. 
					It has a perfect design that's great for any Business/Firms  Blogs who wants a new look for their site.</p>		
			</div>
			<div class="col-md-4">
				<h3>Quality</h3>
				<div class="browse_themes">
				<div class="super-screenshots">
				<a class="hover_thumb" target="_blank" href="http://webriti.com/free-trial/quality.1.0.3.zip"><img style="width: 100%;height:284px;" src="<?php echo get_stylesheet_directory_uri(). '/images/quality-trail-themes.png' ?> "></a>	
				</div>
				</div>
				<p>Business theme which is ideal for creating a corporate / business website.It boasts of 2 beautifully designed page templates , Home and Blog Page.</p>		
			</div>
			<div class="col-md-4">
				<h3>WallStreet</h3>
				<div class="browse_themes">
				<div class="super-screenshots">
				<a class="hover_thumb" target="_blank" href="#"><img style="width: 100%;height:284px;" src="<?php echo get_stylesheet_directory_uri(). '/images/wallstreet-trial-themes.jpg' ?> "></a>	
				</div>
				</div>
				<p>Coming Soon</p>
				<div class="ribbon ribbon-large ribbon-red">
					<div class="banner">
						<div class="text" style="font-family: ronnia, Helvetica, Arial, sans-serif;"><strong><center>Coming Soon</center></strong></div>
					</div>
				</div>		
			</div><div class="clearfix"></div>
			<div class="col-md-4">
				<h3>Corpbiz</h3>
				<div class="browse_themes">
				<div class="super-screenshots">
				<a class="hover_thumb" target="_blank" href="#"><img style="width: 100%;height:284px;" src="<?php echo get_stylesheet_directory_uri(). '/images/corpbiz-trial-themes.png' ?> "></a>	
				</div>
				</div>
				<p>Coming Soon</p>
				<div class="ribbon ribbon-large ribbon-red">
					<div class="banner">
						<div class="text" style="font-family: ronnia, Helvetica, Arial, sans-serif;"><strong><center>Coming Soon</center></strong></div>
					</div>
				</div>		
			</div>
		</div>	
	</div><br><br>

<!-- /Container -->
<div class="inner_top_mn" >
	<div class="container" ></div></div>

<?php get_footer();?>
