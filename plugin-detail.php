<?php //Template Name: Coming -Soon-Detail ?>
<?php get_header(); ?>
<!-- Homepage Slider Section -->
<div class="themedetail_main_slider">
	<div class="container">
		<div class="row">
			
			<div class="col-md-7">
			
				<a href=""><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/plugin_theme_slide.png"></a>
			
			</div>
			
			<div class="col-md-5 themedetail_data">
				<h2>Easy Coming Soon Pro in $29</h2>
				<p>Quickly build and Launch Beautiful looking Landing Page for your website. Ideal for creating Landing Pages, Coming Soon Pages and Under Construction pages. Collect visitor emails and engage with them using Newsletter integration. Use on any number of sites.</p>
				<div class="themedetail_btntop">
                    <a class="themedetail_slide_btn" target="_blank" href="http://webriti.com/coming-soon-demo-select/">View Demo</a><span>or</span>
					<a class="buy_theme_btn" href="#myModal"  data-toggle="modal" style="color:#fff">Buy Now</a>
                </div>
			</div>
			<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery(".close").click(function(){
				jQuery("#myModal").hide();
			});
		});
	</script>
	<style>.homebtn {
		padding: 5px 22px !important;
		font-family: 'OpenSansBold';
		margin-bottom: 7px !important;
		color: hsl(0, 100%, 100%);
		text-shadow: none;
		background-color: hsl(0, 81%, 44%);
		background-repeat: repeat-x;
		border-color: hsl(103, 50%, 54%) hsl(103, 50%, 54%) hsl(0, 0%, 18%);
		border-color: hsla(0, 0%, 0%, 0.1) hsla(0, 0%, 0%, 0.1) hsla(0, 0%, 0%, 0.25);
		display: inline-block;
		text-align: center;
		text-decoration: none;
		vertical-align: middle;
		cursor: pointer;
		font-size: 18px;
		line-height: 30px;
		border: 1px solid hsl(0, 0%, 80%);
		border-color: hsl(0, 0%, 90%) hsl(0, 0%, 90%) hsl(0, 0%, 75%);
		border-color: hsla(0, 0%, 0%, 0.1) hsla(0, 0%, 0%, 0.1) hsla(0, 0%, 0%, 0.25);
		border-bottom-color: hsl(0, 0%, 70%);
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		border-radius: 4px;
		-webkit-box-shadow: inset 0 1px 0 hsla(0, 100%, 100%, 0.2), 0 1px 2px hsla(0, 0%, 0%, 0.05);
		-moz-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
		box-shadow: inset 0 1px 0 hsla(0, 100%, 100%, 0.2), 0 1px 2px hsla(0, 0%, 0%, 0.05);
		}
		.homebtn:hover{ text-decoration:none; color:#fff;}
	</style>
			 <div id="myModal" class="modal  fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="background:transparent;border:0px;display:none;">
		
  		<div class="modal-body" align="center" style="margin-top:5%">
		
  			<div class="busiprof-pro-modal" style="background:#FFFFFF; width:290px;border:2px solid #ddd;webkit-box-shadow: 0 5px 15px rgba(0,0,0,.5);box-shadow: 0 50px 50px rgba(0,0,0,0.5);" >
  			<button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-right: -19px; margin-top: -16px; opacity:1">
		   <img src="<?php echo get_template_directory_uri(); ?>/images/close_256.png">
		</button>	
				<div id="title"><h4 style="background:#6BB3D1;margin-top:0%;padding-bottom:25px;padding-top:7px;color:#FFFFFF;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;">Easy Coming Soon Pro</h4>
  					<div class="rate" style="display: block;font: bold 25px/62px Georgia, Serif;color: #777;
background: #CFEEFC;border: 5px solid #fff;height: 68px;width: 68px;margin: -31px auto 5px;
-moz-border-radius: 100px;-webkit-border-radius: 100px;border-radius: 5200px;-moz-box-shadow: 0 5px 20px #ddd inset, 0 3px 0 #999 inset;-webkit-box-shadow: 0 5px 20px #ddd inset, 0 3px 0 #999 inset;box-shadow: 0 5px 20px #ddd inset, 0 3px 0 #999 inset;">
                        <span>$</span><strong>29</strong>
                     </div>
    			</div>
   				<h5 style="color:#F22853;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;">KEY FEATURES</h5>
                <table class="table table-hover" >
                   
                    <tr><td style="text-align:center;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:14px;">
                    <strong>  Use on Unlimited Websites</strong>
                    </td></tr>
                    <tr><td style="text-align:center;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:14px;">
                    <strong>1 year of Support & Updates </strong>
                    </td></tr>
					  <tr><td style="text-align:center;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:14px;">
                    <strong>Fast & Friendly Support</strong>
                    </td></tr>
					
                </table>
    			<center>
    			<a href="http://webriti.com/amember/signup/ecspro"  class="homebtn"><strong>Buy Now</strong></a>
    			</center><br />
    		</div>
  		</div> 
	</div>
			
		</div>
	</div>
</div>
<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery(".close").click(function(){
				jQuery("#myModal").hide();
			});
		});
	</script>
	<style>.homebtn {
		padding: 5px 22px !important;
		font-family: 'OpenSansBold';
		margin-bottom: 7px !important;
		color: hsl(0, 100%, 100%);
		text-shadow: none;
		background-color: hsl(0, 81%, 44%);
		background-repeat: repeat-x;
		border-color: hsl(103, 50%, 54%) hsl(103, 50%, 54%) hsl(0, 0%, 18%);
		border-color: hsla(0, 0%, 0%, 0.1) hsla(0, 0%, 0%, 0.1) hsla(0, 0%, 0%, 0.25);
		display: inline-block;
		text-align: center;
		text-decoration: none;
		vertical-align: middle;
		cursor: pointer;
		font-size: 18px;
		line-height: 30px;
		border: 1px solid hsl(0, 0%, 80%);
		border-color: hsl(0, 0%, 90%) hsl(0, 0%, 90%) hsl(0, 0%, 75%);
		border-color: hsla(0, 0%, 0%, 0.1) hsla(0, 0%, 0%, 0.1) hsla(0, 0%, 0%, 0.25);
		border-bottom-color: hsl(0, 0%, 70%);
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		border-radius: 4px;
		-webkit-box-shadow: inset 0 1px 0 hsla(0, 100%, 100%, 0.2), 0 1px 2px hsla(0, 0%, 0%, 0.05);
		-moz-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
		box-shadow: inset 0 1px 0 hsla(0, 100%, 100%, 0.2), 0 1px 2px hsla(0, 0%, 0%, 0.05);
		}
		.homebtn:hover{ text-decoration:none; color:#fff;}
		table tr td { color :#000;} 
	</style>
<div id="myModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="background:transparent;border:0px;display:none;">
		<div class="modal-header" style="border:0px;">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true" ><img src="<?php echo get_stylesheet_directory_uri().'/images/close_256.png'?>" /></button>
    		<h4 id="myModalLabel"></h4>
  		</div>
  		<div class="modal-body" align="center">
  			<div class="spasalon-pro-modal" style="background:#FFFFFF; width:290px;border:2px solid #ddd;webkit-box-shadow: 0 5px 15px rgba(0,0,0,.5);box-shadow: 0 50px 50px rgba(0,0,0,0.5);" >
  				<button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-right: -19px;
margin-top: -16px; opacity:1"><img src="<?php echo get_template_directory_uri(); ?>/images/close_256.png"></button>
				<div id="title"><h4 style="background:#6BB3D1;margin-top:0%;padding-bottom:25px;padding-top:7px;color:#FFFFFF;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;">SpaSalon-Pro</h4>
  					<div class="rate" style="display: block;font: bold 25px/62px Georgia, Serif;color: #777;
background: #CFEEFC;border: 5px solid #fff;height: 68px;width: 68px;margin: -31px auto 5px;
-moz-border-radius: 100px;-webkit-border-radius: 100px;border-radius: 5200px;-moz-box-shadow: 0 5px 20px #ddd inset, 0 3px 0 #999 inset;-webkit-box-shadow: 0 5px 20px #ddd inset, 0 3px 0 #999 inset;box-shadow: 0 5px 20px #ddd inset, 0 3px 0 #999 inset;">
                        <span>$</span><strong>49</strong>
                     </div>
    			</div>
   				<h5 style="color:#F22853;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;">KEY FEATURES</h5>
                <table class="table table-hover" >
                    <tr ><td style="text-align:center;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:14px;">
                   <strong> Responsive Design</strong>
                    </td></tr>
                    <tr><td style="text-align:center;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:14px;">
                    <strong>06 Page Templates</strong> 
                    </td></tr>
                    <tr><td style="text-align:center;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:14px;">
                   <strong> Custom Post Types & Custom Widgets</strong>
                    </td> </tr>
                    <tr><td style="text-align:center;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:14px;">
                    <strong>  Unlimited Domain Usage</strong>
                    </td></tr>
                    <tr><td style="text-align:center;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:14px;">
                    <strong>1 year of Support & Updates </strong>
                    </td></tr>
                </table>
    			<center>
    			<a href="http://www.webriti.com/amember/signup/index"  class="homebtn"><strong>Buy Now</strong></a>
    			</center><br />
    		</div>
  		</div> 
	</div>
<div class="homepage_main_slide_shadow"></div>
<!-- /Homepage Slider Section -->
<!-- Theme Features Section -->
<div class="container">	
	<div class="row">
		<div class="col-md-12 themedetail_image">
			<img class="img-responsive" title="Easy Coming Soon Pro Detail" src="<?php echo get_template_directory_uri() ?>/images/comingsoon_homepage4.jpg">
		</div>
	</div>
	
 </div>
<!-- /Theme Features Section -->
<!--Theme Detail Testimonial Section-->
<?php get_template_part('index', 'testimonial'); ?>
<!--/Theme Detail Testimonial Section-->
<?php get_footer(); ?>