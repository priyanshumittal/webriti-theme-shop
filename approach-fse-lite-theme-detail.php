<?php 
//Template Name: Approach FSE Theme Detail

get_header(); 
?>
<!-- Banner Section -->
<div class="approach-banner themedetail_main_slider">
		<div class="container">
				<div class="row">
					<div class="col-lg-7 col-md-7 col-sm-6">
							<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/approach/approach-mockup.png">
					</div>
					
					<div class="col-lg-5 col-md-5 col-sm-6">
							<h2>Approach FSE</h2>
							<p>Approach is a Free WordPress Block Theme for the Full Site Editing. This theme comes with responsive design, 11 different block patterns, 6 FSE Templates, and 3 Templates parts to help you create your site. The theme suites for the all kinds of the sites like blog, corporate, business, etc. Download the Approach theme and start creating your site.</p>
					</div>
				</div>
		</div>
</div>

<!-- Features Section -->
<div class="approach-features">
		<div class="container">
				<div class="row">
						<div class="themedetail_heading_title">
								<h2>Theme Features</h2>
						</div>
						<div class="approach-features-list">
								<div class="col-lg-4 col-md-4 col-sm-4">
										<ul>
												<li><i class="fa fa-check-circle"></i> Responsive Design</li>
												<li><i class="fa fa-check-circle"></i> Dropdown Menu</li>
												<li><i class="fa fa-check-circle"></i> WordPress Standard Codes</li>
										</ul>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-4">
										<ul>
												<li><i class="fa fa-check-circle"></i> Custom Logo</li>
                                                <li><i class="fa fa-check-circle"></i> Threaded Comments</li>
										</ul>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-4">
										<ul>
												<li><i class="fa fa-check-circle"></i> Custom Menu</li>
												<li><i class="fa fa-check-circle"></i> Support</li>
										</ul>
								</div>
						</div>
				</div>
		</div>
</div>


<!-- Banner Section -->
<div class="approach-detail-section">
		<div class="container">
				<div class="row">
						<div class="col-lg-4 col-md-4 col-sm-4">
								<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/approach/approach-front.png">
						</div>
						<div class="col-lg-4 col-md-4 col-sm-4">
								<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/approach/approach-blog.png">
						</div>
						<div class="col-lg-4 col-md-4 col-sm-4">
								<img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/approach/approach-single.png">
						</div>
				</div>
		</div>
		<div class="container">
				<div class="row">
						<div class="approach-checkout-detail themedetail_heading_title">
								<h2>Checkout Lite Version</h2>
						</div>
				</div>
		</div>
		<div class="container">
				<div class="row">
						<div class="col-lg-4 col-md-4 col-sm-4">
								<div class="checkout-list">
										<a href="https://fse-approach.webriti.com/" target="_blank"><i class="fa fa-eye"></i> Free Demo</a>
								</div>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-4">
								<div class="checkout-list">
										<a href="https://wordpress.org/themes/approach/" target="_blank"><i class="fa fa-download"></i> Download Lite</a>
								</div>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-4">
								<div class="checkout-list">
										<a href="https://wordpress.org/support/theme/approach/" target="_blank"><i class="fa fa-cog"></i> Need Support?</a>
								</div>
						</div>
				</div>
		</div>
</div>

<!-- CTA Section -->
<div class="approach-cta">
		<div class="container">
				<div class="row">
						<div class="col-lg-8 col-md-8 col-sm-6 cta-info">
								<h2>Theme Instructions</h2>
								<p>The theme instruction helps you to build & edit the website.</p>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-6">
								<div class="btntop">
										<a class="doc_btn" href="https://help.webriti.com/category/themes/approach-fse/" target="_blank">Read Article</a>
								</div>
						</div>
				</div>
		</div>
</div>
<?php get_footer(); ?>