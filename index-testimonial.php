<script>
jQuery(function() {
			//	This js For Homepage Testimonial Section
			jQuery('#hc_testimonial').carouFredSel({
				width: '100%',
				responsive : true,
				circular: true,
				prev: '#prev3',
				next: '#next3',
				directon: 'left',
				auto: true,
				scroll : {
						items : 1,
						duration : 1200,
						timeoutDuration : 1200
					},
			});
			
			jQuery('#webriti-testimonial').carouFredSel({
				//width: '100%',
				responsive : true,
				circular: true,
				pagination: "#pager2",
				
				 items: {
                        visible: {
                            min: 1,
                            max: 10
                        }
                    },

				prev: '#prev3',
				next: '#next3',
				directon: 'left',

				auto: true,
				 scroll : {
						items : 1,
						duration : 1500,
						//fx:"uncover-fade",
						//easing: "elastic",
						timeoutDuration : 6000
					},

			});
			
		});
</script>
<!-- Homepage Testimonial Section -->
<div class="container home_trestimonial_section">		
		<div class="row" id="webriti-testimonial">
		<div class="col-md-12 home_trestimonial_area pull-left">
				<div><img class="img-circle testimonial_img" src="<?php echo get_template_directory_uri() ?>/images/steve.png"></div>
				<p>"This is a great theme from Webriti - it's packed with brilliant 'out of the box' features which make it quick and easy to build a superb business site.Along with excellent templates, a portfolio, front page slider, contact page, front page featured posts and services etc. you get quick, effective support from the developers whenever you need it.This is a fast-loading, responsive, adaptable theme that really does look equally good on a mobile, tablet or laptop. Highly recommended - the Pro version is great value for money!"</p>
				<h2><i></i>Steve Minto(https://www.freelancesteve.com/)<i></i></h2>
			</div>
		<div class="col-md-12 home_trestimonial_area pull-left">
				<div><img class="img-circle testimonial_img" src="<?php echo get_template_directory_uri() ?>/images/angela-flicker1.jpg"></div>
				<p>"Thank you so much, for all of your help.  Even though I am not a web designer, I was able to create a beautiful and functional website using webriti themes.  The support that I received  was exceptional!. I would recommend your services to anyone. "</p>
				<h2><i></i>Angela Flicker(Integrated Technology in Architecture)<i></i></h2>
			</div>
			<div class="col-md-12 home_trestimonial_area pull-left">
				<div><img class="img-circle testimonial_img" src="<?php echo get_template_directory_uri() ?>/images/testi1.jpg"></div>
				<p>"Webriti themes are elegant, simple & creative. They can fit a wide variety of business fields. As for the technical support, I've received a very quick, efficient and kind attention. Keep up the good work!"</p>
				<h2><i></i>Yunny Andrade<i></i></h2>
			</div>
			<div class="col-md-12 home_trestimonial_area pull-left">
				<div><img class="img-circle testimonial_img" src="<?php echo get_template_directory_uri() ?>/images/amit-webriti.png"></div>
				<p>"I was looking for a new well designed theme to refresh my website (www.askscientific.com). I went through loads of different themes on WordPress, but none matched the lovely design and value for money offered by Webriti’s Busiprof theme. Following installation I had a few design issues with the theme and the Webriti team provided great support and made sure the issues were resolved. I highly recommend them both for design and fantastic support."</p>
				<h2><i></i>Amit Nair<i></i></h2>
			</div>			
			<div class="col-md-12 home_trestimonial_area pull-left">
				<div><img class="img-circle testimonial_img" src="<?php echo get_template_directory_uri() ?>/images/shann.jpg"></div>
				<p>"I would highly recommend the Webriti Busiprof theme for corporate small to medium sized website development. I have found it to be highly customizable and elegantly simple in design. The Webriti team has been extremely helpful and quick to address any questions I have had and their support has been unmatched."</p>
				<h2><i></i>SHANN A. ELBLE (Palindrome Graphics)<i></i></h2>
			</div>
			<div class="col-md-12 home_trestimonial_area pull-left">
				<div><img class="img-circle testimonial_img" src="<?php echo get_template_directory_uri() ?>/images/testi2.jpg"></div>
				<p>"I would like to thank Webriti Team for elegant theme with the support and updates which made my work really easy.Effective communication with solution of my problem helped me a lot. I wish to work further with Webriti. Good luck for future."</p>
				<h2><i></i>BISHNU GOPALI <i></i></h2>
			</div>
			<div class="col-md-12 home_trestimonial_area pull-left">
				<div><img class="img-circle testimonial_img" src="<?php echo get_template_directory_uri() ?>/images/karima.jpg"></div>
				<p>"I have used 2 templates from Webriti to start and grow my small business. Although I am not a web designer it was easy to build my own website. I love the way Webriti design and structure their templates and each time when I am in need of help the respond quickly with valuable advise. I am delighted to recommend Webriti for all small businesses and for those who want to build their own website from scratch."</p>
				<h2><i></i>KARIMA BIHAKI (Entrepreneur & Business Coach - London) <i></i></h2>
			</div>
			<div class="col-md-12 home_trestimonial_area pull-left">				
				<p>"I have browsed though so many themes and Webriti Busiprof has definitely stood out because of its elegant design. To top that, the Webriti team has been very helpful in customizing our website to meet our expectations. With their prompt response and willingness to resolve my problems, I am very satisfied to have purchased the premium version of busiprof. Keep up the good work!"</p>
				<h2><i></i>JASMINE LIU<i></i></h2>
			</div>
		</div>		
		<div class="row">
			<div class="pager testi-pager" id="pager2">
				<a href="#" class=""><span>1</span></a>
				<a href="#" class="selected"><span>2</span></a>
				<a href="#" class=""><span>3</span></a>
				<a href="#" class=""><span>4</span></a>
				<a href="#" class=""><span>5</span></a>
				<a href="#" class=""><span>6</span></a>
				<a href="#" class=""><span>7</span></a>
			</div>
		</div>
		
</div>		
<!-- /Homepage Testimonial Section -->