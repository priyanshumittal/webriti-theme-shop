<script>
jQuery(function() {
			//	This js For Homepage Testimonial Section
			jQuery('#hc_testimonial').carouFredSel({
				width: '100%',
				responsive : true,
				circular: true,
				prev: '#prev3',
				next: '#next3',
				directon: 'left',
				auto: true,
				scroll : {
						items : 1,
						duration : 1200,
						timeoutDuration : 1200
					},
			});
			
			jQuery('#webriti-testimonial').carouFredSel({
				//width: '100%',
				responsive : true,
				circular: true,
				pagination: "#pager2",
				
				 items: {
                        visible: {
                            min: 1,
                            max: 10
                        }
                    },

				prev: '#prev3',
				next: '#next3',
				directon: 'left',

				auto: true,
				 scroll : {
						items : 1,
						duration : 1500,
						//fx:"uncover-fade",
						//easing: "elastic",
						timeoutDuration : 6000
					},

			});
			
		});
</script>
<!-- Homepage Testimonial Section -->
<div class="container home_trestimonial_section testimonial-padding">

		<div class="themedetail_heading_title">
			<h2>Know what our clients has to say about us</h2>
			<div id="" class="themedetail_separator"></div>
		</div>
			
		<div class="row" id="webriti-testimonial">
		<div class="col-md-12 home_trestimonial_area pull-left">
				<div><img class="img-circle testimonial_img" src="<?php echo get_template_directory_uri() ?>/images/testimonial/review-1.jpeg"></div>
				<p>"Easy to install and configure and fantastic support!"</p>
				<h2><i></i>n1mh<i></i></h2>
			</div>
		<div class="col-md-12 home_trestimonial_area pull-left">
				<div><img class="img-circle testimonial_img" src="<?php echo get_template_directory_uri() ?>/images/testimonial/review-2.jpeg"></div>
				<p>"This is very easy to use plugin. User can also customized its code. I like this plugin . Thanks to the author of this Plugin."</p>
				<h2><i></i>Akhlesh Nagar <i></i></h2>
			</div>
			<div class="col-md-12 home_trestimonial_area pull-left">
				<div><img class="img-circle testimonial_img" src="<?php echo get_template_directory_uri() ?>/images/testimonial/review-4.jpeg"></div>
				<p>"It works very well. I had a couple issues to start off, but don't panic these guys answer your questions really fast! So the service alone is worth it."</p>
				<h2><i></i>jessemartineau <i></i></h2>
			</div>			
			<div class="col-md-12 home_trestimonial_area pull-left">
				<div><img class="img-circle testimonial_img" src="<?php echo get_template_directory_uri() ?>/images/testimonial/review-5.jpeg"></div>
				<p>"Just simply works, easy to customize"</p>
				<h2><i></i>sccr410<i></i></h2>
			</div>
			<div class="col-md-12 home_trestimonial_area pull-left">
				<div><img class="img-circle testimonial_img" src="<?php echo get_template_directory_uri() ?>/images/testimonial/no-image.png"></div>
				<p>"I installed the free edition on my site and this worked great, it worked well and was just what i needed. So i bought the pro version. I had a few issues with it at first but the developer spent a few hours logged in to my site and fixed it. Top job!"</p>
				<h2><i></i>brentech  <i></i></h2>
			</div>
			<div class="col-md-12 home_trestimonial_area pull-left">
				<div><img class="img-circle testimonial_img" src="<?php echo get_template_directory_uri() ?>/images/testimonial/no-image.png"></div>
				<p>"Definitely happy with this plugin. Able to populate a list of pictures base on hashtag or username. Worth the money! Avoid the headache and just buy it."</p>
				<h2><i></i>rufio808 <i></i></h2>
			</div>
			<div class="col-md-12 home_trestimonial_area pull-left">
				<div><img class="img-circle testimonial_img" src="<?php echo get_template_directory_uri() ?>/images/testimonial/review-6.png"></div>		
				<p>"This plugin is amazing! Up and running in 15 minutes. Looks great - flexible customizations - perfect for our needs. Great job - thank you."</p>
				<h2><i></i>28palms<i></i></h2>
			</div>
			<div class="col-md-12 home_trestimonial_area pull-left">
				<div><img class="img-circle testimonial_img" src="<?php echo get_template_directory_uri() ?>/images/testimonial/no-image.png"></div>		
				<p>"Great plugin. Does what it says on the tin. Looks great, easy to use. Also, fantastic support. Abhishek was back to me within minutes. Thank you."</p>
				<h2><i></i>maisie<i></i></h2>
			</div>
			<div class="col-md-12 home_trestimonial_area pull-left">
				<div><img class="img-circle testimonial_img" src="<?php echo get_template_directory_uri() ?>/images/testimonial/no-image.png"></div>		
				<p>"This plugin works perfectly. It is easy to use."</p>
				<h2><i></i>rnarian <i></i></h2>
			</div>
			<div class="col-md-12 home_trestimonial_area pull-left">
				<div><img class="img-circle testimonial_img" src="<?php echo get_template_directory_uri() ?>/images/testimonial/no-image.png"></div>		
				<p>"i use it on multiple website with success customer care fast and affordable sometimes i have to re-authorize authcode because of instagram updates, but this is the best instagram plugin i've found!."</p>
				<h2><i></i>danieledezi <i></i></h2>
			</div>
			
		</div>		
		<div class="row">
			<div class="pager testi-pager" id="pager2">
				<a href="#" class=""><span>1</span></a>
				<a href="#" class="selected"><span>2</span></a>
				<a href="#" class=""><span>3</span></a>
				<a href="#" class=""><span>4</span></a>
				<a href="#" class=""><span>5</span></a>
				<a href="#" class=""><span>6</span></a>
				<a href="#" class=""><span>7</span></a>
			</div>
		</div>
		
</div>		
<!-- /Homepage Testimonial Section -->