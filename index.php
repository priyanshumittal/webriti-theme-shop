<?php //Template Name: HOME ?>
<?php get_header(); ?>
<div class="homepage_main_slider">
	<div class="container">
		<div class="row">			
			<div class="col-md-5 slide_data">
				<h2>Premium WordPress Themes.</h2>
				<p>WordPress themes for artists, bloggers and business professionals who want to look great on any device.</p>
				<div class="btntop">
                    <a class="home_slide_btn" href="http://webriti.com/browse-theme/">Browse Theme</a>
                </div>
			</div>
			
			<div class="col-md-7">
				<a href=""><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/slide1.png"></a>
			</div>
			
		</div>
	</div>
</div>
<div class="homepage_main_slide_shadow"></div>
<div class="container">
	<div class="row">
		<div class="webriti_heading_title">
			<h1>Beautiful <span>Responsive</span> Themes</h1>
			<p>Our responsive themes are designed to work seamlessly for all desktop and mobile devices.</p>
			<div id="" class="webriti_separator"></div>
		</div>		
	</div>	
	<div class="row">		
		<div class="col-md-4 col-sm-6">
			<div class="showcase_area">
				<a href="http://webriti.com/appointment"><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/showcase10.jpg" title="Appointment"></a>
				<div class="showcase_area_link"><a href="http://webriti.com/appointment" title="Appointment">Appointment Pro</a></div>
			</div>
			<div class="ribbon ribbon-large ribbon-red">
				<div class="banner">
				<div class="text" style="font-family: ronnia, Helvetica, Arial, sans-serif;"><strong><center>Popular</center></strong></div>
				</div>
			</div>
		</div>		
		<div class="col-md-4 col-sm-6">
			<div class="showcase_area">
				<a href="http://webriti.com/busiprof"><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/showcase2.jpg" title="Busiprof"></a>
				<div class="showcase_area_link"><a href="http://webriti.com/busiprof" title="Busiprof">Busiprof Pro</a></div>
			</div>
		</div>		
		<div class="col-md-4 col-sm-6">
			<div class="showcase_area">
				<a href="http://webriti.com/wallstreet"><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/showcase7.jpg" title="Wallstreet"></a>
				<div class="showcase_area_link"><a href="http://webriti.com/wallstreet" title="Wallstreet">Wallstreet Pro</a></div>
			</div>
		</div>		
	</div>	
	<div class="row">
		<div class="homepage_showcase_seperator"></div>
		<div class="showcase_btn_arrange"><a class="showcase_btn" href="http://webriti.com/browse-theme/">View All Themes</a></div>
	</div>
 </div>
<div class="homepage_service_section">
	<div class="container">
		
		<div class="row">
			<div class="webriti_heading_title">
				<h1>Why you <span>should choose</span> our Themes?</h1>
				<p>Our responsive themes are designed to work seamlessly for all desktop and mobile devices.</p>
				<div id="" class="webriti_separator"></div>
			</div>
		</div>
		
		<div class="row">			
			<div class="col-md-4 col-sm-6 home_service_area">
				<div class="service_box">
					<i class="fa fa-wrench"></i>
				</div>
				<h2>Easy to Customize</h2>
				<p>The majority of our themes contain responsive frameworks that mold perfectly to fit all your mobile and Apple devices. </p>
			</div>
			
			<div class="col-md-4 col-sm-6 home_service_area">
				<div class="service_box">
					<i class="fa fa-tablet"></i>
				</div>
				<h2>Responsive Mobile Friendly</h2>
				<p>The majority of our themes contain responsive frameworks that mold perfectly to fit all your mobile and Apple devices. </p>
			</div>
			
			<div class="col-md-4 col-sm-6 home_service_area">
				<div class="service_box">
					<i class="fa fa-weixin"></i>
				</div>
				<h2>Free Support</h2>
				<p>The majority of our themes contain responsive frameworks that mold perfectly to fit all your mobile and Apple devices. </p>
			</div>		
		</div>
		
	</div>
</div>
<!-- /Homepage Service Section -->
<div class="container">		
		<div class="row">
			<div class="webriti_heading_title">
				<h1>Happy <span>Customers</span></h1>
				<p>Over 500 Extremely Satisfied Customers</p>
				<div id="" class="webriti_separator"></div>
			</div>
		</div>
</div>
<?php get_template_part('index', 'testimonial'); ?>
<?php get_footer(); ?>  
