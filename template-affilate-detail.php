<?php //Template Name: Webriti Affiliate Registration ?>
<?php get_header(); ?>

<div class="affiliate_section_five">
	<div class="container">
		
		<div class="row">
			<div class="themedetail_heading_title">
				<h2>Affiliate <span>Registration</span></h2>
				<div id="" class="themedetail_separator"></div>
			</div>
		</div>
		
	
		
		
		<div class="row">
			
			<div class="col-md-8">
				<h2>Sign up is easy and takes just a few simple steps.</h2>
				<br><br>
				
				<h3>1. Apply</h3>
				<p>Sign-up for a ShareASale affiliate account and apply for our Affiliate program in one step by filling out the application <a href="http://www.shareasale.com/shareasale.cfm?merchantID=64846" target="_blank">here</a>. (If you already have a ShareASale account use the same link to apply to our program.)</p>
			
				<h3>2. Acceptance</h3>
				<p style="margin-bottom:15px;">We will manually approve the submitted applications and once you receive the approval email from us, you can start promiting our products. Find the links and graphical banners in your creatives section of dashboard.</p>
				<div class="alert alert-info" role="alert">We are not accepting <b>Coupon or Deal</b> type websites.</div>
				<h3>3. Linking and Banners</h3>
				<p>You can link to us using text links or banners. Once you have selected a product you want to promote you can then copy and paste the code into your website or blog. Find our <a href="http://www.shareasale.com/a-getmerchantcode.cfm?merchantID=64846&type=All" target="_blank">creatives</a> here including all banners and text links. (Requires a ShareASale account.)</p>
			
			</div>
			
			<div class="col-md-4 affiliate-promo">
				<img src="<?php echo get_template_directory_uri() ?>/images/affiliates/share-affiliate.jpg" class="img-responsive"/>
			</div>
			
		</div>
							
		</div>
	</div>
 </div>

<div class="clearfix"></div>

<div class="affiliate_section_four">
	<div class="container">
		
		<div class="row">
			<div class="themedetail_heading_title">
				<h2>Affiliates – FAQ’s</h2>
				<div id="" class="themedetail_separator"></div>
			</div>
		</div>
		
		<div class="row">
					
			<div class="col-md-6">
				
				<h3>What is your Affiliate Program?</h3>
				<p>Our affiliate program financially rewards you for directing customers to our WordPress theme collection. When a product is purchased by a customer you refer, we pay you a commission for the sale(s).</p>
			</div>
			
			<div class="col-md-6">	
				<h3>How do I join your Affiliate Program?</h3>
				<p>Our Affiliate Program is managed by a ShareASale affiliate application.Simply click Join button on the Affiliate registration page to enroll.</p>
				
				
			</div><div class="clearfix"></div>
					
			<div class="col-md-6">
				
				<h3>How will I know that orders came through my blog or website?</h3>
				<p>The affiliate links that you generate, via ShareASale, track's you as having referred the customer and ShareASale also places a 60-day cookie on each visitor’s computer, so if a visitor you refer to us doesn’t purchase immediately, but then returns within 60 days, you will still receive the commission for any products they purchase.</p>
			</div>
			
			<div class="col-md-6">	
				<h3>How much money will I earn through your affiliate program?</h3>
				<p>Our standard commission rate is 40% of the purchase price of our theme packages.</p>
			</div><div class="clearfix"></div>
			
			<div class="col-md-6">	
				<h3>How do I know how much commission I have earned?</h3>
				<p>You can login to the ShareASale dashboard at any time for access to real-time reporting, which provides up-to-the-minute information about the commissions you’ve earned.</p>
			</div>
			
			<div class="col-md-6">	
				<h3>When will I receive my Affiliate commissions?</h3>
				<p>All affiliate payments are processed by ShareASale and payments sent directly to you by the 20th of each month for the previous month’s sales, via the payment method you opt for in your SAS account.</p>
			</div><div class="clearfix"></div>
			
			<div class="col-md-6">	
				<h3>Can I earn commissions on my own purchases?</h3>
				<p>You will not receive credit when purchasing a theme package through your own affiliate link. You may receive an automatic notification from ShareASale to let you know that a sale has taken place, but your affiliate payments will not include this amount.</p>
			</div>
			
			<div class="col-md-6">	
				<h3>Is my blog or website eligible for your Affiliate Program?</h3>
				<p>Most blogs and websites qualify to participate in the Affiliate Program. However, we reserve the right to refuse membership to a website or revoke your membership at any time, if we determine that your website contains objectionable material.</p>
				
			</div><div class="clearfix"></div><!-- /.col -->
						
		</div>
	</div>
 </div>

<?php get_footer(); ?>
