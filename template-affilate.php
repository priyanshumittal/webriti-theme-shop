<?php //Template Name: Webriti Affiliate ?>
<?php get_header(); ?>


<!-- Homepage Slider Section -->
<div class="affiliate_main_slider">
	<div class="container">
		<div class="row">
			
			<div style="text-align:center; height:425px;" class="col-md-12">
			
				<h2>WordPress Themes <span>Affiliate</span> Program</h2>
				<p>Earn money while promoting Webriti Themes. Join our affiliate program and earn 40% commissions on all sales generated through your affiliate links.</p>
				<img src="<?php echo get_template_directory_uri() ?>/images/affiliates/affiliates-banner.png" class="img-responsive" style="padding-bottom:25px;">
				
				<div class="affiliate_btntop">
                    <a href="http://webriti.com/affiliate-registration/" class="affiliate_slide_btn">Join Now</a><span>or</span>
					<a class="affiliate_buy_theme_btn" href="https://account.shareasale.com/a-login.cfm?invalid=1&reason=1&redirect=%2Fa-getmerchantcode%2Ecfm?merchantID%3D28169%26type%3DAll">Login</a>
                </div>
			</div>
			
		</div>
	</div>
</div>

<div class="affiliate_slide_shadow"></div>

<!-- /Homepage Slider Section -->


<!-- Affiliate Section One -->
<div class="affiliate_section_one">
	<div class="container">
		<div class="row">
			<div class ="col-md-4 col-sm-6">
				<div class="affiliate_icon"><i class="fa fa-money"></i></div>
				<h3>Highest Comission %</h3>	
				<p>Sign up to become and affiliate and earn 40% commission on each sale you refer. </p>
			</div>
			
			<div class ="col-md-4 col-sm-6">
				<div class="affiliate_icon"><i class="fa fa-sticky-note-o"></i></div>
				<h3>Marketing Materials</h3>	
				<p>We’ll provide you with links and banners to help with your promotions.</p>
			</div>
			
			<div class ="col-md-4 col-sm-6">
				<div class="affiliate_icon"><i class="fa fa-usd"></i></div>
				<h3>Make money while you sleep!</h3>	
				<p>Enjoy generating revenue and making money while you sleep as a Webriti affiliate. </p>
			</div>
			
			<div class ="col-md-4 col-sm-6">
				<div class="affiliate_icon"><i class="fa fa-user"></i></div>
				<h3>Apply Now</h3>	
				<p>Sign-up for a ShareASale affiliate account and apply for our Affiliate program in one step by filling out the application <a href="#">here</a>. (If you already have a ShareASale account use the same link to apply to our program.)</p>
			</div>
			
			<div class ="col-md-4 col-sm-6">
				<div class="affiliate_icon"><i class="fa fa-thumbs-up"></i></div>
				<h3>On Acceptance</h3>	
				<p>We will manually approve the submitted applications and once you receive the approval email from us, you can start promiting our products. Find the links and graphical banners in your creatives section of dashboard.</p>
			</div>
			
			<div class ="col-md-4 col-sm-6">
				<div class="affiliate_icon"><i class="fa fa-usd"></i></div>
				<h3>Marketing Material</h3>	
				<p>You can link to us using text links or banners. Once you have selected a product you want to promote you can then copy and paste the code into your website or blog. Find our creatives here including all banners and text links. (Requires a ShareASale account.)</p>
			</div>
			
		</div>
		
	</div>
</div>
<!-- /Affiliate Section One -->

<div class="clearfix"></div>

<!-- Affiliate Section Two -->
<div class="affiliate_section_two">
	<div class="container">
		<div class="row">
			<div class="webriti_heading_title">
				<h1>How much can you <span>earn?</span></h1>
				<div id="" class="webriti_separator"></div>
			</div>
		</div>
		
		<div class="row">
			
			<div class="col-md-5 col-sm-12">
			<img src="<?php echo get_template_directory_uri() ?>/images/affiliates/chart.png" class="img-responsive" title="" alt="">
			</div>
			
			<div class="col-md-3 col-sm-12 earn_feature_area">
				<div class="media">
					<div class="earn_feature_icon">
						<i class="fa fa-chevron-circle-right"></i>
					</div>
					<div class="media-body">
						<h3>40% Commissions of Each Sale </h3>
					</div>
				</div>
				
				<div class="media">
					<div class="earn_feature_icon">
						<i class="fa fa-chevron-circle-right"></i>
					</div>
					<div class="media-body">
						<h3>Affiliate Dashboard Panel </h3>
					</div>
				</div>
				
				<div class="media">
					<div class="earn_feature_icon">
						<i class="fa fa-chevron-circle-right"></i>
					</div>
					<div class="media-body">
						<h3>No Minimum Payout Limit </h3>
					</div>
				</div>
				
				<div class="media">
					<div class="earn_feature_icon">
						<i class="fa fa-chevron-circle-right"></i>
					</div>
					<div class="media-body">
						<h3>Monthly Payouts </h3>
					</div>
				</div>
				
				<div class="media">
					<div class="earn_feature_icon">
						<i class="fa fa-chevron-circle-right"></i>
					</div>
					<div class="media-body">
						<h3>Comission Statistics </h3>
					</div>
				</div>
				
				<div class="media">
					<div class="earn_feature_icon">
						<i class="fa fa-chevron-circle-right"></i>
					</div>
					<div class="media-body">
						<h3>Insightful Reporting </h3>
					</div>
				</div>
				
				
			
			</div>
			
			<div class ="col-md-4 col-sm-12">
				<table class="rwd-table" border="0" cellspacing="0" cellpadding="0" align="center">
					<tbody>
					<tr>
					<th colspan="2">Monthly Income Statistics – 2 Sales Per Day</th>
					</tr>
					<tr>
					<td>Theme Price</td>
					<td>$59.00</td>
					</tr>
					<tr>
					<td>Commision Percentage</td>
					<td>40%</td>
					</tr>
					<tr>
					<td>Commision You Get</td>
					<td>$23.60</td>
					</tr>
					<tr>
					<td>Sales Per Day</td>
					<td>2</td>
					</tr>
					<tr>
					<td>Your Monthly Income</td>
					<td>$23.60 x 2 x 30</td>
					</tr>
					<tr>
					<th><strong> Total (Every Month)</strong></th>
					<th><strong> $1416.00 </strong></th>
					
					</tr>
					</tbody>
					</table>
				
			</div>
			
			
			
		</div>
	</div>
</div>
<!-- /Affiliate Section Two -->

<!-- Affiliate Section Two -->
<div class="affiliate_section_three">
	<div class="container">
		<div class="row">
			<div class="webriti_heading_title">
				<h1>Take a look at all the <span>themes</span> you can earn from </h1>
				<div id="" class="webriti_separator"></div>
			</div>
		</div>
		
		<div class="row">
			
			<div class="col-md-12 col-sm-12">
			<img src="<?php echo get_template_directory_uri() ?>/images/affiliates/all-themes.png" class="img-responsive" title="" alt="">
			</div>
		</div>
		<br><br>
		<div class="row btn-center">
			<div class="affiliate_btntop">
				<a href="http://webriti.com/browse-theme/" target="_blank" class="affiliate_slide_btn">Browse All Themes</a>
				
			</div>
		</div>
	</div>
</div>
<!-- /Affiliate Section Two -->


<div class="affiliate_section_four">
	<div class="container">
		
		<div class="row">
			<div class="themedetail_heading_title">
				<h2>Affiliates – FAQ’s</h2>
				<div id="" class="themedetail_separator"></div>
			</div>
		</div>
		
		<div class="row">
					
			<div class="col-md-6">
				
				<h3>What is your Affiliate Program?</h3>
				<p>Our affiliate program financially rewards you for directing customers to our WordPress theme collection. When a product is purchased by a customer you refer, we pay you a commission for the sale(s).</p>
			</div>
			
			<div class="col-md-6">	
				<h3>How do I join your Affiliate Program?</h3>
				<p>Our Affiliate Program is managed by a ShareASale affiliate application.Simply click Join button on the Affiliate registration page to enroll.</p>
				
				
			</div><div class="clearfix"></div>
					
			<div class="col-md-6">
				
				<h3>How will I know that orders came through my blog or website?</h3>
				<p>The affiliate links that you generate, via ShareASale, track's you as having referred the customer and ShareASale also places a 60-day cookie on each visitor’s computer, so if a visitor you refer to us doesn’t purchase immediately, but then returns within 60 days, you will still receive the commission for any products they purchase.</p>
			</div>
			
			<div class="col-md-6">	
				<h3>How much money will I earn through your affiliate program?</h3>
				<p>Our standard commission rate is 40% of the purchase price of our theme packages.</p>
			</div><div class="clearfix"></div>
			
			<div class="col-md-6">	
				<h3>How do I know how much commission I have earned?</h3>
				<p>You can login to the ShareASale dashboard at any time for access to real-time reporting, which provides up-to-the-minute information about the commissions you’ve earned.</p>
			</div>
			
			<div class="col-md-6">	
				<h3>When will I receive my Affiliate commissions?</h3>
				<p>All affiliate payments are processed by ShareASale and payments sent directly to you by the 20th of each month for the previous month’s sales, via the payment method you opt for in your SAS account.</p>
			</div><div class="clearfix"></div>
			
			<div class="col-md-6">	
				<h3>Can I earn commissions on my own purchases?</h3>
				<p>You will not receive credit when purchasing a theme package through your own affiliate link. You may receive an automatic notification from ShareASale to let you know that a sale has taken place, but your affiliate payments will not include this amount.</p>
			</div>
			
			<div class="col-md-6">	
				<h3>Is my blog or website eligible for your Affiliate Program?</h3>
				<p>Most blogs and websites qualify to participate in the Affiliate Program. However, we reserve the right to refuse membership to a website or revoke your membership at any time, if we determine that your website contains objectionable material.</p>
				
			</div><div class="clearfix"></div><!-- /.col -->
						
		</div>
	</div>
 </div>

<?php get_footer(); ?>
