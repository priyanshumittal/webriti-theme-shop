<!--Sidebar-->
	<div class="col-md-4">
	<?php if ( is_active_sidebar( 'sidebar-primary' ) )
			{ ?>
			<div class="webriti_sidebar_section">
				<?php dynamic_sidebar( 'sidebar-primary' ); ?>
			</div>	
			<?php } else { ?> 
			<div class="webriti_sidebar_section">		
			<div class="webriti_sidebar_widget">
				<div class="webriti_sidebar_widget_title">
					<h2>
						Categories Widget
						<div id="" class="webriti-separator-small"></div>
					</h2>
				</div>	
				<div class="webriti_sidebar_link">
					<p>
						<a href="#"><i class="fa fa-angle-right"></i>100% Responsive Design</a>
						<span>10</span>
					</p>
					<p>
						<a href="#"><i class="fa fa-angle-right"></i>Fresh And Clean Theme</a>
						<span>10</span>
					</p>
					<p>
						<a href="#"><i class="fa fa-angle-right"></i>Browser Compatibillity</a>
						<span>10</span>
					</p>
					<p>
						<a href="#"><i class="fa fa-angle-right"></i>Easiest Theme</a>
						<span>10</span>
					</p>
				
				</div>
			</div>
			
			<div class="webriti_sidebar_widget">
					<div class="webriti_sidebar_widget_title">
						<h2>
							Latest Post Widget
							<div id="" class="webriti-separator-small"></div>
						</h2>
					</div>	
					<div class="media webriti_media_sidebar">
						<a class="pull-left sidebar_pull_img" href="#">
							<img src="<?php echo get_template_directory_uri() ?>/images/sidebar_post1.jpg" alt="Webriti" class="media-object sidebar-img">
						</a>
						<div class="media-body">
							<h3><a href="#">Just a cool blog post with A Gallery with Awesome Pics.</a></h3>
							<span>Posted on Jan 7, 2014</span>
						</div>
					</div>
					
					<div class="media webriti_media_sidebar">
						<a class="pull-left sidebar_pull_img" href="#">
							<img src="<?php echo get_template_directory_uri() ?>/images/sidebar_post2.jpg" alt="Webriti" class="media-object sidebar-img">
						</a>
						<div class="media-body">
							<h3><a href="#">Just a cool blog post with A Gallery with Awesome Pics.</a></h3>
							<span>Posted on Feb 12, 2014</span>
						</div>
					</div>
					
					<div class="media webriti_media_sidebar">
						<a class="pull-left sidebar_pull_img" href="#">
							<img src="<?php echo get_template_directory_uri() ?>/images/sidebar_post3.jpg" alt="Webriti" class="media-object sidebar-img">
						</a>
						<div class="media-body">
							<h3><a href="#">Just a cool blog post with A Gallery with Awesome Pics.</a></h3>
							<span>Posted on Apr 10, 2014</span>
						</div>
					</div>
					<div class="media webriti_media_sidebar">
						<a class="pull-left sidebar_pull_img" href="#">
							<img src="<?php echo get_template_directory_uri() ?>/images/sidebar_post4.jpg" alt="Webriti" class="media-object sidebar-img">
						</a>
						<div class="media-body">
							<h3><a href="#">Just a cool blog post with A Gallery with Awesome Pics.</a></h3>
							<span>Posted on May 15, 2014</span>
						</div>
					</div>
				
			</div>
			
			<div class="webriti_sidebar_widget">
				<div class="webriti_sidebar_widget_title">
					<h2>
						Tag Widget
						<div id="" class="webriti-separator-small"></div>
					</h2>
				</div>
				<div class="webriti_widget_tags">
					<a href="#">Wordpress</a>
					<a href="#">Php</a>
					<a href="#">Jquery</a>
					<a href="#">Css</a>
					<a href="#">Php</a>
					<a href="#">Jquery</a>
					<a href="#">Css</a>
					<a href="#">Javascript</a>
					<a href="#">Photoshop</a>
					<a href="#">Html</a>	
				</div>
			</div>
			
			<div class="webriti_sidebar_widget">
				<div class="webriti_sidebar_widget_title">
					<h2>
						Flicker Widget
						<div id="" class="webriti-separator-small"></div>
					</h2>
					<div class="webriti_sidebar_flicker">
					<span><a href="#"><img src="<?php echo get_template_directory_uri() ?>/images/sidebar_post1.jpg" alt="Webriti"></a></span>
					<span><a href="#"><img src="<?php echo get_template_directory_uri() ?>/images/sidebar_post2.jpg" alt="Webriti"></a></span>
					<span><a href="#"><img src="<?php echo get_template_directory_uri() ?>/images/sidebar_post3.jpg" alt="Webriti"></a></span>
					<span><a href="#"><img src="<?php echo get_template_directory_uri() ?>/images/sidebar_post4.jpg" alt="Webriti"></a></span>
					<span><a href="#"><img src="<?php echo get_template_directory_uri() ?>/images/sidebar_post1.jpg" alt="Webriti"></a></span>
					<span><a href="#"><img src="<?php echo get_template_directory_uri() ?>/images/sidebar_post2.jpg" alt="Webriti"></a></span>
					<span><a href="#"><img src="<?php echo get_template_directory_uri() ?>/images/sidebar_post3.jpg" alt="Webriti"></a></span>
					<span><a href="#"><img src="<?php echo get_template_directory_uri() ?>/images/sidebar_post4.jpg" alt="Webriti"></a></span>
					<span><a href="#"><img src="<?php echo get_template_directory_uri() ?>/images/sidebar_post1.jpg" alt="Webriti"></a></span>
					<span><a href="#"><img src="<?php echo get_template_directory_uri() ?>/images/sidebar_post2.jpg" alt="Webriti"></a></span>
					</div>
				</div>
			</div>			
		</div>
		<?php } ?>
	</div>
<!--/Sidebar-->