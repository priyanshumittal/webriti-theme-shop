<?php //Template Name: New SpaSalon-Theme-Detail ?>
<?php get_header(); ?>
<!-- Theme Demonstration Section -->
<div class="home-demonstration">
	<div class="container-fluid">
		<div class="row">
			
			<div class="col-md-7">
				<figure class="demo-thumbnail">
					<img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/themedetail/mockup-spa.png" alt="Spasalon">
				</figure>
			</div>
			
			<div class="col-md-5">
				<div class="demo-content">
					<h1 class="theme-title">SpaSalon - Pro</h1>
					<h2 class="theme-subtitle">A Beauty Salon Wordpress Theme</h2>
					<p class="theme-description">
					SpaSalon is a responsive multipurpose WordPress theme best suitable for  beauty industry like Cosmetology, Hairstyling, Spa, Beauty Salon, Massage & Therapy Center and Beauty Care website. Theme is not only limited to beauty sector rather you can use it for any type of business.</p>
					<div class="themedemo-btn">
						<a class="btn1 btn-green" target="_blank" href="https://spasalon-pro.webriti.com/"><i class="fa fa-television"></i>View Demo</a><span class="or">Or</span>
						<a class="btn1 btn-darkblue spa_premium-freemius" href="http://www.webriti.com/amember/signup/spasalon"><i class="fa fa-cart-plus"></i>Buy Now</a>
					</div>
					</div>
			</div>
			
		</div>
	</div>
</div>
<!-- End of Theme Demonstration Section -->

<div class="clearfix"></div>

<section class="theme-info">
	<div class="container">
		<div class="row">
			<!--<div class="col-md-4 col-sm-6"><span class="fa fa-check-square-o">Current Version: <b>2.5.1</b></span></div>
			<div class="col-md-4 col-sm-6"><span class="fa fa-clock-o">Created: <b>11 September 13</b></span></div>	
			<div class="col-md-4 col-sm-6"><span class="fa fa-refresh">WP Compatibility:  <b><a href="#">v4.4+</a></b></span></div>-->
			<div class="col-md-4 col-sm-6"><span class="fa fa-download">Help Docs: <b><a target="_blank" href="http://webriti.com/help/themes/spasalon/spasalon-wordpress-theme/">Click Here</a></b></span></div>
			<div class="col-md-4 col-sm-6"><span class="fa fa-support">Need Support: <b><a target="_blank" href="https://users.freemius.com/login">Create a ticket</a></b></span></div>	
			<div class="col-md-4 col-sm-6"><span class="fa fa-tablet">Feedback: <b><a target="_blank" href="https://wordpress.org/support/theme/spasalon/reviews/#new-post">Make a Review</a></b></span></div>
		</div>
	</div>
</section>

<!-- Theme Detail Section -->
<div class="container">

	<div class="row">
		<div class="section-header">
			<h2 class="section-title"><b>Spasalon</b> is the most popular <b>Beauty salon/Multi-Purpose</b> WordPress theme at <b>wordpress.org</b></h2>
		</div>
	</div>
	
	<!-- Nav tabs -->
	<div class="row">
		<div class="col-md-12 col-xs-12 mytabs">
			<ul class="nav nav-tabs tabs-style-linemove" role="tablist">
				<li class="active"><a href="#features" data-toggle="tab" class="features">Theme Features</a></li>
				<li><a href="#reviews" data-toggle="tab" class="reviews">Reviews</a></li>
				<li><a href="#core-features" data-toggle="tab" class="core-features">The Core Features</a></li>
				<li><a href="#free-pro" data-toggle="tab" class="free-pro">Free vs Pro</a></li>
			</ul>
		</div>
	</div>
	
	<!-- Tab panes -->
	<div class="tab-content">
	
		<!-- Features -->
		<div class="tab-pane active" id="features">
			<div class="row">
							
				<div class="col-md-4 col-xs-12">
					<article class="post">
						<figure class="post-thumbnail padding-4em">
							<img src="<?php echo get_template_directory_uri(); ?>/images/themedetail/features/mockup-spa.png" class="img-responsive" alt="img">						
						</figure>
						<div class="entry-header">
							<h4 class="entry-title">Responsive Design</h4>
						</div>
						<div class="entry-content">
							<p>Tablets and mobile devices are only getting more popular, and your site needs to perform on them. SpaSalon Pro gives you a fully responsive design that never fails! on any of the devices.</p>
						</div>
					</article>
				</div>
				
				<div class="col-md-4 col-xs-12">
					<article class="post">
						<figure class="post-thumbnail padding-4em">
							<img src="<?php echo get_template_directory_uri(); ?>/images/themedetail/features/support2.jpg" class="img-responsive" alt="img">						
						</figure>
						<div class="entry-header">
							<h4 class="entry-title">Reliable Support Center</h4>
						</div>
						<div class="entry-content">
							<p>We have an outstanding support team  who will  help you any time. Active user can post their support queries on our <a target="_blank" href="https://users.freemius.com/login">support forum</a>. For sales related questions leave us a message <a target="_blank" href="http://webriti.com/contact-us/">here</a></p>
						</div>
					</article>
				</div>
				
				<div class="col-md-4 col-xs-12">
					<article class="post">
						<figure class="post-thumbnail padding-4em">
							<img src="<?php echo get_template_directory_uri(); ?>/images/themedetail/features/browser-compatible.jpg" class="img-responsive" alt="img">						
						</figure>
						<div class="entry-header">
							<h4 class="entry-title">Major Browsers Compatibility</h4>
						</div>
						<div class="entry-content">
							<p>It’s important that your website performs well for everyone who visits it. Our themes are developed to work in all modern web browsers. The site looks smooth irrespective of the browser that you view your site on.</p>
						</div>
					</article>
				</div>
				
				<div class="col-md-4 col-xs-12">
					<article class="post">
						<figure class="post-thumbnail padding-4em">
							<img src="<?php echo get_template_directory_uri(); ?>/images/themedetail/features/translation.jpg" class="img-responsive" alt="img">						
						</figure>
						<div class="entry-header">
							<h4 class="entry-title">Translation Ready</h4>
						</div>
						<div class="entry-content">
							<p>So you’re building a website that might not be written in English? No problem, Spasalon Pro has you covered. All our themes are fully localized and can be translated easily into your language.</p>
						</div>
					</article>
				</div>
				
				<div class="col-md-4 col-xs-12">
					<article class="post">
						<figure class="post-thumbnail padding-4em">
							<img src="<?php echo get_template_directory_uri(); ?>/images/themedetail/features/woocommerce.jpg" class="img-responsive" alt="img">						
						</figure>
						<div class="entry-header">
							<h4 class="entry-title">WooCommerce Compatible</h4>
						</div>
						<div class="entry-content">
							<p>Want to create online store, It's really easy using our theme, It transform your website in a online store. Spasalon Pro has given special attention to its WooCommerce integration. </p>
						</div>
					</article>
				</div>
				
				<div class="col-md-4 col-xs-12">
					<article class="post ribbon-pro">
						<div class="ribbon-wrapper"><div class="ribbon-text">PRO</div></div>
						<figure class="post-thumbnail padding-4em">
							<img src="<?php echo get_template_directory_uri(); ?>/images/themedetail/features/plugin-support.jpg" class="img-responsive" alt="img">						
						</figure>
						<div class="entry-header">
							<h4 class="entry-title">Plugin Supports</h4>
						</div>
						<div class="entry-content">
							<p>Compatible With 100+ Free WordPress Plugins. Contact Page Template in premium version  have a support for  <a target="_blank" href="https://wordpress.org/plugins/wp-google-maps/" style="color:#428bca;">Wp Google Maps</a> and <a target="_blank" href="https://wordpress.org/plugins/contact-form-7/" style="color:#428bca;">Contact Form 7</a> popular WordPress plugins. Infact use these popular plugins anywhere on your site.</p>
						</div>
					</article>
				</div>
				
				<div class="col-md-4 col-xs-12">
					<article class="post ribbon-pro">
						<div class="ribbon-wrapper"><div class="ribbon-text">PRO</div></div>
						<figure class="post-thumbnail padding-4em">
							<img src="<?php echo get_template_directory_uri(); ?>/images/themedetail/features/unlimited-colors2.jpg" class="img-responsive" alt="img">						
						</figure>
						<div class="entry-header">
							<h4 class="entry-title">Multiple Color Option</h4>
						</div>
						<div class="entry-content">
							<p>Generate perfect color combinations for your website. SpaSalon Theme comes with 9 default color skins. You can also create your own color skin / palette without editing any css code. </p>
						</div>
					</article>
				</div>
							
				<div class="col-md-4 col-xs-12">
					<article class="post ribbon-pro">
						<div class="ribbon-wrapper"><div class="ribbon-text">PRO</div></div>
						<figure class="post-thumbnail padding-4em">
							<img src="<?php echo get_template_directory_uri(); ?>/images/themedetail/features/typography.jpg" class="img-responsive" alt="img">						
						</figure>
						<div class="entry-header">
							<h4 class="entry-title">Advanced Typography</h4>
						</div>
						<div class="entry-content">
							<p>Typography is a very important element of design. You can manage font typography like font family, font size and font style. </p>
						</div>
					</article>
				</div>
				
				<div class="col-md-4 col-xs-12">
					<article class="post ribbon-pro">
						<div class="ribbon-wrapper"><div class="ribbon-text">PRO</div></div>
						<figure class="post-thumbnail padding-4em">
							<img src="<?php echo get_template_directory_uri(); ?>/images/themedetail/features/custom-widget.jpg" class="img-responsive" alt="img">						
						</figure>
						<div class="entry-header">
							<h4 class="entry-title">Custom Widgets</h4>
						</div>
						<div class="entry-content">
							<p>Intutive Custom Widget for managing various website modules. <i>Contact Widget</i>, <i>Latest News Widget</i>, <i>Latest Post Widget</i>, <i>Page / Service Widget</i>, <i>Product Category Widget</i>, <i>Recent Products</i>, <i>Team Widget</i> are bundeled with the theme package.</p>
						</div>
					</article>
				</div>
				
				<div class="col-md-4 col-xs-12">
					<article class="post ribbon-pro">
						<div class="ribbon-wrapper"><div class="ribbon-text">PRO</div></div>
						<figure class="post-thumbnail padding-4em">
							<img src="<?php echo get_template_directory_uri(); ?>/images/themedetail/features/wpml.jpg" class="img-responsive" alt="img">						
						</figure>
						<div class="entry-header">
							<h4 class="entry-title">WPML / Polylang Compatible</h4>
						</div>
						<div class="entry-content">
							<p>Theme supports  yet another famous translations plugin <a href="http://wpml.org/" target="_blank" style="color:#428bca;">WPML</a> and <a href="https://wordpress.org/plugins/polylang/" target="_blank" style="color:#428bca;">Polylang</a>.</p>
						</div>
					</article>
				</div>
				
				<div class="col-md-4 col-xs-12">
					<article class="post">
						<figure class="post-thumbnail padding-4em">
							<img src="<?php echo get_template_directory_uri(); ?>/images/themedetail/features/wp-install.jpg" class="img-responsive" alt="img">						
						</figure>
						<div class="entry-header">
							<h4 class="entry-title">Easy Installation & Setup</h4>
						</div>
						<div class="entry-content">
							<p>All our premium themes are easy to install. Theme comes with Dummy Data file which you can import from the WordPress Tools setting for getting the exact replica as our <a href="https://spasalon-pro.webriti.com/" target="_blank">demo</a> site.</p>
						</div>
					</article>
				</div>
				
				<div class="col-md-4 col-xs-12">
					<article class="post">
						<figure class="post-thumbnail padding-4em">
							<img src="<?php echo get_template_directory_uri(); ?>/images/themedetail/features/css.jpg" class="img-responsive" alt="img">						
						</figure>
						<div class="entry-header">
							<h4 class="entry-title">Custom Css</h4>
						</div>
						<div class="entry-content">
							<p>Custom css option ready. If you want to use your own css style? You no need to edit any file. Simply paste css code in custom css editor.</p>
						</div>
					</article>
				</div>
				
				<div class="col-md-4 col-xs-12">
					<article class="post ribbon-pro">
						<div class="ribbon-wrapper"><div class="ribbon-text">PRO</div></div>
						<figure class="post-thumbnail padding-4em">
							<img src="<?php echo get_template_directory_uri(); ?>/images/themedetail/features/color-scheme.jpg" class="img-responsive" alt="img">						
						</figure>
						<div class="entry-header">
							<h4 class="entry-title">9 Predefined Color Schemes</h4>
						</div>
						<div class="entry-content">
							<p>There are 9 predefined color schemes included in this theme. You can change color schemes through theme appearance customizer.</p>
						</div>
					</article>
				</div>
				
				<div class="col-md-4 col-xs-12">
					<article class="post">
						<figure class="post-thumbnail padding-4em">
							<img src="<?php echo get_template_directory_uri(); ?>/images/themedetail/features/documented.jpg" class="img-responsive" alt="img">						
						</figure>
						<div class="entry-header">
							<h4 class="entry-title">Documentation / Video tutorials</h4>
						</div>
						<div class="entry-content">
							<p>All our theme are well documented. Video series helps you in configuring content .</p>
						</div>
					</article>
				</div>
				
				<div class="col-md-4 col-xs-12">
					<article class="post ribbon-pro">
						<div class="ribbon-wrapper"><div class="ribbon-text">PRO</div></div>
						<figure class="post-thumbnail padding-4em">
							<img src="<?php echo get_template_directory_uri(); ?>/images/themedetail/features/font-awesome.jpg" class="img-responsive" alt="img">						
						</figure>
						<div class="entry-header">
							<h4 class="entry-title">Font Awesome Icons</h4>
						</div>
						<div class="entry-content">
							<p>Spasalon is fully integrated with the entire Font Awesome Icon Set. Each icon can easily be used via shortcodes.</p>
						</div>
					</article>
				</div>
					
			</div>
		</div>
		
		
		<!-- Reviews -->
		<div class="tab-pane" id="reviews">
			<div class="row">

				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="testimonial">
						<div class="media">
							<aside class="user"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/spa/review/eupterraen.jpeg" alt="img"></aside>
							<div class="media-body">
								<h4 class="review-title">Perfect for what I needed</h4>
								<blockquote>This theme is exactly what I wanted for my site and developing eCommerce shop in natural wellness. I am very happy with it!</blockquote>
								<h4 class="user-name">- eupterraen</h4>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="testimonial">
						<div class="media">
							<aside class="user"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/spa/review/bosmolskate.jpeg" alt="img"></aside>
							<div class="media-body">
								<h4 class="review-title">Excellent Service</h4>
								<blockquote>Great theme and very helpful support team!</blockquote>
								<h4 class="user-name">- bosmolskate</h4>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="testimonial">
						<div class="media">
							<aside class="user"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/spa/review/elsa-liliana.jpeg" alt="img"></aside>
							<div class="media-body">
								<h4 class="review-title">Good Support</h4>
								<blockquote>I have had some doubts and the support has been very fast and helpful.</blockquote>
								<h4 class="user-name">- elsa-liliana</h4>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="testimonial">
						<div class="media">
							<aside class="user"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/spa/review/alisonfkelly.jpeg" alt="img"></aside>
							<div class="media-body">
								<h4 class="review-title">Support for Spa Salon</h4>
								<blockquote>Excellent support, very quick feedback and assistance.</blockquote>
								<h4 class="user-name">- alisonfkelly</h4>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="testimonial">
						<div class="media">
							<aside class="user"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/spa/review/bellealacampagne.png" alt="img"></aside>
							<div class="media-body">
								<h4 class="review-title">Issues resolved!</h4>
								<blockquote>I really like this spa theme. The team support is reactive and always find solution to customize as my needs. I do recommand this the spasalon theme by webriti 😉</blockquote>
								<h4 class="user-name">- bellealacampagne</h4>
								
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="testimonial">
						<div class="media">
							<aside class="user"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/spa/review/mayakoruncheva.jpeg" alt="img"></aside>
							<div class="media-body">
								<h4 class="review-title">Issues resolved!</h4>
								<blockquote>The theme has resolved all its issues,support from developers has been very helpful, and I am happily using it again 🙂</blockquote>
								<h4 class="user-name">- mayakoruncheva</h4>
								
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="testimonial">
						<div class="media">
							<aside class="user"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/spa/review/nash4.jpeg" alt="img"></aside>
							<div class="media-body">
								<h4 class="review-title">Easy, clean, high representative</h4>
								<blockquote>Another well-done-job from webriti! great guys! go on! 😉</blockquote>
								<h4 class="user-name">- nash4</h4>
								
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="testimonial">
						<div class="media">
							<aside class="user"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/spa/review/user-2.jpeg" alt="img"></aside>
							<div class="media-body">
								<h4 class="review-title">Great design and support!</h4>
								<blockquote>I love the SpaSalon theme and the support it’s great!!</blockquote>
								<h4 class="user-name">- FreeTimeServices</h4>
								
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="testimonial">
						<div class="media">
							<aside class="user"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/spa/review/user-3.jpeg" alt="img"></aside>
							<div class="media-body">
								<h4 class="review-title">Great theme</h4>
								<blockquote>We love this theme and they have great support.</blockquote>
								<h4 class="user-name">- Av-events</h4>
								
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="testimonial">
						<div class="media">
							<aside class="user"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/spa/review/user-1.jpeg" alt="img"></aside>
							<div class="media-body">
								<h4 class="review-title">Perfect for what I needed</h4>
								<blockquote>This theme is exactly what I wanted for my site and developing eCommerce shop in natural wellness. I am very happy with it!</blockquote>
								<h4 class="user-name">- Eupterraen</h4>
								
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="testimonial">
						<div class="media">
							<aside class="user"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/spa/review/no-image.png" alt="img"></aside>
							<div class="media-body">
								<h4 class="review-title">Awesome Theme</h4>
								<blockquote>This theme is very helpful to me. I am not a programmer but help of these support members i made my website beautiful. Thank you.</blockquote>
								<h4 class="user-name">- Aanvidh</h4>
								
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="testimonial">
						<div class="media">
							<aside class="user"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/spa/review/no-image.png" alt="img"></aside>
							<div class="media-body">
								<h4 class="review-title">Very good support & Theme</h4>
								<blockquote>Template seems very well made, and support has been very reactive for the moment. 5* very happy customer</blockquote>
								<h4 class="user-name">- Kevinponseel</h4>
								
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="testimonial">
						<div class="media">
							<aside class="user"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/spa/review/no-image.png" alt="img"></aside>
							<div class="media-body">
								<h4 class="review-title">Best theme ever!</h4>
								<blockquote>I love this theme because if it’s simplicity and most of all because of the support – it is really amazing!</blockquote>
								<h4 class="user-name">- Zaaz89</h4>
								
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="testimonial">
						<div class="media">
							<aside class="user"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/spa/review/no-image.png" alt="img"></aside>
							<div class="media-body">
								<h4 class="review-title">I just love this theme</h4>
								<blockquote>I love the SpaSalon theme and the support it’s great!! It is as per my expectation</blockquote>
								<h4 class="user-name">- Domylook</h4>
								
							</div>
						</div>
					</div>
				</div>
				
				
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="testimonial">
						<div class="media">
							<aside class="user"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/spa/review/no-image.png" alt="img"></aside>
							<div class="media-body">
								<h4 class="review-title">It is good and manageable template</h4>
								<blockquote>I found this on google search and find it very helpful and easy to use.</blockquote>
								<h4 class="user-name">- Jasmine Doe</h4>
								
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="testimonial">
						<div class="media">
							<aside class="user"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/spa/review/no-image.png" alt="img"></aside>
							<div class="media-body">
								<h4 class="review-title">Nice template and great support</h4>
								<blockquote>Template looks cool. Initially it was difficult to setup but due to continuous support and updates it works well.</blockquote>
								<h4 class="user-name">- Bizzy613</h4>
								
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="testimonial">
						<div class="media">
							<aside class="user"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/spa/review/no-image.png" alt="img"></aside>
							<div class="media-body">
								<h4 class="review-title">Simple and effective</h4>
								<blockquote>Great site, easy on eye and simple to configure</blockquote>
								<h4 class="user-name">- Widefrog</h4>
								
							</div>
						</div>
					</div>
				</div>
				
				
				
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="testimonial">
						<div class="media">
							<aside class="user"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/spa/review/no-image.png" alt="img"></aside>
							<div class="media-body">
								<h4 class="review-title">Nice looks, good support</h4>
								<blockquote>"This plugin is great especially if you want to work behind the scene with any theme. We experienced a problem with this plugin and the creators fixed it within a day."</blockquote>
								<h4 class="user-name">- Nr27</h4>
								
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="testimonial">
						<div class="media">
							<aside class="user"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/spa/review/no-image.png" alt="img"></aside>
							<div class="media-body">
								<h4 class="review-title">Very good people ,thank you</h4>
								<blockquote>Spasalon theme is very clean & Sharp, customizable and stylish WordPress theme that lets you display your picture, contact infomation/form, social media icons, Blog & Blog Detail page more… very good people ,thank you</blockquote>
								<h4 class="user-name">- Szryanzhu</h4>
								
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="testimonial">
						<div class="media">
							<aside class="user"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/spa/review/no-image.png" alt="img"></aside>
							<div class="media-body">
								<h4 class="review-title">Very Nice Theme</h4>
								<blockquote>I am using the basic version of the theme and I love it.</blockquote>
								<h4 class="user-name">- Ankit8881</h4>
								
							</div>
						</div>
					</div>
				</div>

			</div>
			<br><br>
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12 text-center">
					<a class="btn1 btn-darkblue" href="https://wordpress.org/support/theme/spasalon/reviews/" target="blank">View More Latest Reviews at Official WordPress.org site</a>
				</div>	
			</div>
		</div>
		
			<!-- Core Features -->
		<div class="tab-pane" id="core-features">
			<div class="row">
			
				<div class="col-md-4 col-sm-6 col-xs-12">
					<ul class="core-features-list">
						<h4><b>Header & Footer</b></h4>
						<li class="fa fa-check">Custom Logo</li>
						<li class="fa fa-check">Header & Footer Navigation</li>
						<li class="fa fa-check">Footer Copyright Editor</li>
						<br>
						<h4><b>Homepage Sections</b></h4>
						<li class="fa fa-home">Thumbnail Slider <sup class="pro">pro</sup></li>
						<li class="fa fa-home">Caption Slider <sup class="pro">pro</sup></li>
						<li class="fa fa-home">Service Section<sup class="pro">pro</sup></li>
						<li class="fa fa-home">Product Sidebar - add unlimited products and show them in the form carousel slider <sup class="pro">pro</sup></li>
						<li class="fa fa-home">Latest News Section</li>
						<li class="fa fa-home">Team Section - Member name, small info , designation and social profile links<sup class="pro">pro</sup></li>
						<li class="fa fa-home">4 Column Footer</li>
						<li class="fa fa-home">Copyright Section</li>
						<br>
						<h4><b>Page Templates</b></h4>
						<li class="fa fa-file-text">Homepage <sup class="pro">pro</sup></li>
						<li class="fa fa-file-text">Homepage One <sup class="pro">pro</sup></li>
						<li class="fa fa-file-text">About Us Page <sup class="pro">pro</sup></li>
						<li class="fa fa-file-text">Product Page <sup class="pro">pro</sup></li>
						<li class="fa fa-file-text">Blog Full Width Page <sup class="pro">pro</sup></li>
						<li class="fa fa-file-text">Blog Letf Sidebar Page <sup class="pro">pro</sup></li>
						<li class="fa fa-file-text">Blog Right Sidebar Page <sup class="pro">pro</sup></li>
						<li class="fa fa-file-text">Page Full Width Page </li>
						<li class="fa fa-file-text">Contact Page <sup class="pro">pro</sup></li>
					</ul>
				</div>
			
				<div class="col-md-4 col-sm-6 col-xs-12">
					<ul class="core-features-list">
						<h4><b>Page Sidebars</b></h4>
						<li class="fa fa-columns">Primary Sidebar</li>
						<li class="fa fa-columns">Footer Sidebar 1, 2, 3 & 4</li>
						<li class="fa fa-columns">WooCommerce Sidebar</li>
						<br>
						<h4><b>Custom Widgets</b></h4>
						<li class="fa fa-bars">Contact Widget <sup class="pro">pro</sup></li>
						<li class="fa fa-bars">Latest News Widget <sup class="pro">pro</sup></li>
						<li class="fa fa-bars">Latest Post Widget <sup class="pro">pro</sup></li>
						<li class="fa fa-bars">Page / Service Widget with custom link<sup class="pro">pro</sup></li>
						<li class="fa fa-bars">Product Category Widget - Carousel<sup class="pro">pro</sup></li>
						<li class="fa fa-bars">Recent Product Widget <sup class="pro">pro</sup></li>
						<li class="fa fa-bars">Team Widget <sup class="pro">pro</sup></li>
						<br>
						
						<h4><b>Custom Post Type</b></h4>
						<li class="fa fa-check">Our Team <sup class="pro">pro</sup></li>
						<li class="fa fa-check">Product <sup class="pro">pro</sup></li>
						
						<br>
						<h4><b>Contact Page Features</b></h4>
						<li class="fa fa-map">Google Map <sup class="pro">pro</sup></li>
						<li class="fa fa-check-square-o">Contact Form <sup class="pro">pro</sup></li>
						<li class="fa fa-clock-o">Business Hours <sup class="pro">pro</sup></li>
						
						<br>
						<h4><b>Plugin Supports</b></h4>
						<li class="fa fa-shopping-cart">WooCommerce</li>
						<li class="fa fa-map">Google Maps <sup class="pro">pro</sup></li>
						<li class="fa fa-check-square-o">Contact Form 7 <sup class="pro">pro</sup></li>
					</ul>
				</div>
				
				<div class="col-md-4 col-sm-6 col-xs-12">
					<ul class="core-features-list">
						<h4><b>General Features</b></h4>
						<li class="fa fa-television">Responsive Layout</li>
						<li class="fa fa-toggle-on">Built with <b>Latest Bootstrap</b></li>
						<li class="fa fa-globe">Tested with all Major Browsers</li>
						<li class="fa fa-file-code-o">Well Organized <b>Code</b></li>
						<li class="fa fa-eyedropper"><b>9 Predefined</b> Color Schemes<sup class="pro">pro</sup></li>
						<li class="fa fa-cart-plus">WooCommerce Compatible<sup class="pro">pro</sup></li>
						<li class="fa fa-cogs">Highly Customizable</li>
						<li class="fa fa-html5">HTML5 & CSS3</li>
						<li class="fa fa-font">Google Webfonts<sup class="pro">pro</sup></li>
						<li class="fa fa-font-awesome">Fontawesome Icons<sup class="pro">pro</sup></li>
						<li class="fa fa-tablet">Responsive Design</li>
						<li class="fa fa-wpforms">Custom <b>Css</b> Editor</li>
						<li class="fa fa-language">Translation Ready</li>
						<li class="fa fa-eye-slash">Show/Hide post meta like <b>date, tags, author, category</b> etc.<sup class="pro">pro</sup></li>
						<li class="fa fa-television">Support featured image in post view</li>
						<li class="fa fa-arrow-up">Scroll to Top button</li>
						<li class="fa fa-list-alt">Unlimited slides for <b>slider</b><sup class="pro">pro</sup></li>
						<li class="fa fa-commenting-o">Premium Support <b>via</b> <a href="https://users.freemius.com/login" target="_blank">Forum</a></li>
					</ul>
				</div>
					
			</div>
		</div>
		
		
		<!-- Free v Pro - Pricing Details -->
		<div class="tab-pane" id="free-pro">
			<div class="row pricing">
				<div class="col-md-12">
					
					<div class="col-md-4 col-xs-12">
						<div class="plan">
							<header>
								<h4 class="price-title">Features<small>All you'll ever need</small></h4>
								<span class="price"><span class="fa fa-angle-double-down txt-darkblue"></span></span>
							</header>
							<ul class="feature">
								<li>Custom Logo</li>
								<li>Image Slider</li>
								<li>Service Section</li>
								<li>Product Section</li>
								<li>Latest News Section</li>
								<li>Page Templates</li>
								<li>Custom Widget</li>
								<li>Banner Setting</li>
								<li>Woocommerce Compatible</li>
								<li>Translation Ready</li>
								<li>Custom CSS Editor</li>
								<li>Footer Copyright Editor</li>
								<li>Contact Form</li>
								<li>Additional Sidebar Section on HomePage</li>
								<li>Google Fonts</li>
								<li>Font Typography</li>
								<li>Custom Color Schemes</li>
								<li>WPML & Polylang Support</li>
								<li>Layout Manager</li>
								<li>Demo Content</li>
								<li>Google Map</li>
								<li>SEO Friendly URL</li>
								<li>Support</li>
								<li>Download Package</li>
							</ul>
						</div>
					</div>
				
					<div class="col-md-4 col-xs-12">
						<div class="plan">
							<header>
								<h4 class="price-title">Spasalon Free<small> With Limited Flexibility</small></h4>
								<span class="price bg-dark"><span>Free</span></span>
							</header>
							<ul class="feature">
								<li><i class="fa fa-check green"></i></li>
								<li>Static Image</li>
								<li>No Read More Link</li>
								<li>Static Product</li>
								<li><i class="fa fa-check green"></i></li>
								<li>2 Page Templates <i class="fa fa-question-circle green" data-toggle="tooltip" data-placement="top" title="" data-original-title="Business Template, Page Full Width"></i></li>
								<li>2 Custom Widget <i class="fa fa-question-circle green" data-toggle="tooltip" data-placement="top" title="" data-original-title="Page Widget, Latest News Widget"></i></li>
								<li><i class="fa fa-check green"></i></li>
								<li><i class="fa fa-check green"></i></li>
								<li><i class="fa fa-check green"></i></li>
								<li><i class="fa fa-check green"></i></li>
								<li><i class="fa fa-check green"></i></li>
								<li><i class="fa fa-check green"></i></li>
								<li><i class="fa fa-close red"></i></li>
								<li><i class="fa fa-close red"></i></li>
								<li><i class="fa fa-close red"></i></li>
								<li><i class="fa fa-close red"></i></li>
								<li><i class="fa fa-close red"></i></li>
								<li><i class="fa fa-close red"></i></li>
								<li><i class="fa fa-close red"></i></li>
								<li><i class="fa fa-close red"></i></li>
								<li><i class="fa fa-close red"></i></li>
								<li><a href="https://wordpress.org/support/theme/spasalon" class="txt-blue" target="_blank">WordPress Support Forum</a></li>
								<li><a class="btn1 btn-darkblue" href="https://wordpress.org/themes/spasalon/" target="_blank"><i class="fa fa-download"></i>Download !</a></li>
							</ul>
						</div>
					</div>
				
					<div class="col-md-4 col-xs-12">
						<div class="plan">
							<header>
								<h4 class="price-title">SpaSalon Pro<small>With more Flexibility</small></h4>
								<span class="price bg-blue"><span><sup>$</sup>59</span></span>			
							</header>
							<ul class="feature">
								<li><i class="fa fa-check green"></i></li>
								<li>Unlimited Slides 
									<!-- <a href="http://webriti.com/demo/wp/spasalon/" target="_blank">Slider 1</a>, 
									<a href="http://webriti.com/demo/wp/spasalon/home-page-two/" target="_blank">Slider 2</a> -->
								</li>
								<li>Custom Read More link</li>
								<li>Product Carousel</li>
								<li><i class="fa fa-check green"></i></li>
								<li>9 Page Templates <i class="fa fa-question-circle green" data-toggle="tooltip" data-placement="top" title="" data-original-title="Homepage, Homepage 2, About Page, Blog Full Width, Blog Left Sidebar, Blog Right Sidebar, Product Page, Contact Page & Page Full Width"></i></li>
								<li>6 Custom Widget <i class="fa fa-question-circle green" data-toggle="tooltip" data-placement="top" title="" data-original-title="Contact Widget, Latest News Widget, Latest Post Widget, Page / Service Widget, Product Category Widget, Recent Products Widget, Team Widget"></i></li>
								<li><i class="fa fa-check green"></i></li>
								<li><i class="fa fa-check green"></i></li>
								<li><i class="fa fa-check green"></i></li>
								<li><i class="fa fa-check green"></i></li>
								<li><i class="fa fa-check green"></i></li>
								<li><i class="fa fa-check green"></i></li>
								<li><i class="fa fa-check green"></i></li>
								<li><i class="fa fa-check green"></i></li>
								<li><i class="fa fa-check green"></i></li>
								<li><i class="fa fa-check green"></i></li>
								<li><i class="fa fa-check green"></i></li>
								<li><i class="fa fa-check green"></i></li>
								<li><i class="fa fa-check green"></i></li>
								<li><i class="fa fa-check green"></i></li>
								<li><i class="fa fa-check green"></i></li>
								<li><a href="https://users.freemius.com/login" class="txt-blue" target="_blank">Webriti Support Forum</a></li>
								<li><a class="btn1 btn-blue spa_premium-freemius" href="http://www.webriti.com/amember/signup/spasalon"><i class="fa fa-shopping-cart"></i>Buy Now !</a></li>
							</ul>
						</div>
					</div>
				
				</div>
			</div>
		</div>
			
	</div>
</div>
<!-- End of Theme Detail Section -->

<div class="clearfix"></div>

<!-- Footer Callout Section -->
<div class="callout">
	<div class="container">
		<div class="row">
			<div class="col-md-8">		
				<h2>Get Spasalon Pro Theme For Only $59!</h2>
			</div>
			<div class="col-md-4">
				<div class="callout-btn">
					<a class="btn1 buy-btn btn-white spa_premium-freemius" target="_blank" href="http://www.webriti.com/amember/signup/spasalon"><i class="fa fa-cart-plus"></i>Buy Pro Version</a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /Footer Callout Section -->

<div class="clearfix"></div>

<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="https://checkout.freemius.com/checkout.min.js"></script>
<script>
    var handler = FS.Checkout.configure({
        plugin_id:  '11283',
        plan_id:    '19174',
        public_key: 'pk_bbf2edd33df2ecbab4887066f1391',
        image:      'https://your-plugin-site.com/logo-100x100.png'
    });
    
    $('.spa_premium-freemius').on('click', function (e) {
        handler.open({
            name     : 'SpaSalon Pro WordPress Theme',
            licenses : 1,
            // You can consume the response for after purchase logic.
            purchaseCompleted  : function (response) {
                // The logic here will be executed immediately after the purchase confirmation.                                // alert(response.user.email);
            },
            success  : function (response) {
                // The logic here will be executed after the customer closes the checkout, after a successful purchase.                                // alert(response.user.email);
            }
        });
        e.preventDefault();
    });
</script>

<?php get_footer(); ?>