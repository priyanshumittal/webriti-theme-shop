<script>
jQuery(function() {	

			//	This js For Homepage Testimonial Section
			jQuery('#hc_testimonial').carouFredSel({
				width: '100%',
				responsive : true,
				circular: true,
				prev: '#prev3',
				next: '#next3',
				directon: 'left',
				auto: true,
				scroll : {
						items : 1,
						duration : 1200,
						timeoutDuration : 1200
					},
			});

			jQuery('#webriti-showcase').carouFredSel({
				//width: '100%',
				responsive : true,
				circular: true,
				//pagination: "#case_pager",
				
				 items: {
                        visible: {
                            min: 1,
                            max: 10
                        }
                    },

				prev: '#prev3',
				next: '#next3',
				directon: 'left',

				auto: false,
				 scroll : {
						items : 1,
						duration : 1500,
						timeoutDuration : 6000
					},

			});
			
		});
</script>
<!-- Homepage Testimonial Section -->
<div class="container home_showcase_section">
		<div class="row">
			<div class="showcase_heading_title">
		        <h2>Our Showcase</h2>
		        <div id="" class="themedetail_separator"></div>
      		</div>
		</div>		
		<div class="row" id="webriti-showcase">
			<div class="col-md-12"> 
				<div class="col-md-6">
					<div><img class="showcase-img" src="<?php echo get_template_directory_uri() ?>/images/showcase/showcase (1).jpg"></div>
				</div>
				<div class="col-md-6">
					<div class="col-md-12 showcase_area">
						<div class="appointment-rating">★★★★★</div>
						<span>abariasbiz</span>
						<p>"I love the appointment theme. The team at Webriti are always responsive and knowledgeable! Thanks for your help."</p>
						<a class="themedetail_slide_btn" target="_blank" href="https://abariasvirtualassistance.com/">Live Preview</a>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="col-md-6">
					<div><img class="showcase-img" src="<?php echo get_template_directory_uri() ?>/images/showcase/showcase (2).jpg"></div>
				</div>
				<div class="col-md-6">
					<div class="col-md-12 showcase_area">
						<div class="appointment-rating">★★★★★</div>
						<span>marcpinar</span>
						<p>"I am very happy with the theme but I’m more satisfied also with the great and fast customer service, I hight recommend this site for your WordPress themes"</p>
						<a class="themedetail_slide_btn" target="_blank" href="https://www.elpinar.cat/">Live Preview</a>

					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="col-md-6">
					<div><img class="showcase-img" src="<?php echo get_template_directory_uri() ?>/images/showcase/showcase (3).jpg"></div>
				</div>
				<div class="col-md-6">
					<div class="col-md-12 showcase_area">
						<div class="appointment-rating">★★★★★</div>
						<span>masaist1983</span>
						<p>"I’m Japanese, using this theme because of easy and awesome design and UI. But the other day, unfortunately, I got a trouble, unable to fix the homepage. But guys handling this theme gave us quick and kind support. Now everythings’ OK, and as I didn’t expect such deep support I got more loyarity in this company."</p>
						<a class="themedetail_slide_btn" target="_blank" href="https://goodgreen.jp/">Live Preview</a>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="col-md-6">
					<div><img class="showcase-img" src="<?php echo get_template_directory_uri() ?>/images/showcase/showcase (4).jpg"></div>
				</div>
				<div class="col-md-6">
					<div class="col-md-12 showcase_area">
						<div class="appointment-rating">★★★★★</div>
						<span>trembita90</span>
						<p>"I’m very satisfied from support this team. They are very quickly solved my problem with a slider on the site."</p>
						<a class="themedetail_slide_btn" target="_blank" href="https://www.avskontakt.com.ua/">Live Preview</a>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="col-md-6">
					<div><img class="showcase-img" src="<?php echo get_template_directory_uri() ?>/images/showcase/showcase (5).jpg"></div>
				</div>
				<div class="col-md-6">
					<div class="col-md-12 showcase_area">
						<div class="appointment-rating">★★★★★</div>
						<span>jmginfo</span>
						<p>"Really good support from the seller. Great options on this theme. Fast update."</p>
						<a class="themedetail_slide_btn" target="_blank" href="https://www.jmginformatique.ca/">Live Preview</a>
					</div>
				</div>
			</div>
		</div>		
		<div class="row">
			<div class="pager testi-pager" id="case_pager">
				<a id="prev3" class="prev" href="#"><i class="fa fa-arrow-left"></i></a>
				<a id="next3" class="next" href="#"><i class="fa fa-arrow-right"></i></a>
			</div>
			<div class="view-review-showcase">
			<a class="themedetail_slide_btn" href="https://webriti.com/appointment-showcase/">View all Showcase</a>
		</div>
	</div>
</div>		
<!-- /Homepage Showcase Section -->