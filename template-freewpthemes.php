<?php //Template Name: Webriti Free WordPress Themes  ?>
<?php get_header(); ?>
<!-- Page Title Section ---->
<div class="container">
	<div class="row">
		<div class="webriti_page_heading">
			<h1>Free <span>WordPress</span> Themes</h1>
			<p>Try our free collection and upgrade to premium version for more awesome features.</p>
			<div class="page_separator" id=""></div>
		</div>
	</div>
</div>
<!-- /Page Title Section ---->

<!-- Browse Free wordPress Theme Section ---->
<div class="container theme-showcase">
	<div class="row">
		<div class="col-md-6 col-sm-6">
			<div class="theme_showcase_area">
				<a href="http://webriti.com/appointment-free/"><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/free/appointment-new.png" title="Appointment free wordpress theme" alt="Appointment free wordpress theme"></a>
			</div>
		</div>
		<div class="col-md-6 col-sm-6">
			<div class="demo-content">
				<h1 class="theme-title">Appointment</h1>
				<h2 class="theme-subtitle">A  Business & Multipurpose WordPress Theme</h2>
				<p class="theme-description">
				Appointment WordPress Theme is a fully responsive and translation ready theme that allows you to create stunning blogs and websites. Theme is well suited for companies, law firms, travel, photography, recipe, design, art, personal and any other creative websites and blogs.</p>
				<div class="themedemo-btn">
					<a class="btn1 btn-green" target="_blank" href="http://webriti.com/appointment-free/"><i class="fa fa-television"></i>Demo Free</a><span class="or">Or</span>
					<a class="btn1 btn-darkblue" href="https://downloads.wordpress.org/theme/appointment.zip" target="_blank"><i class="fa fa-cart-plus"></i>Download Free</a>
				</div>
			</div>
		</div>
	</div>	
		
	<!-- separator --><div class="col-md-12"><div class="theme_showcase_seperator"></div></div><!-- /separator -->
		
	<div class="row">
		<div class="col-md-6 col-sm-6">
			<div class="theme_showcase_area">
				<a href="https://rambo.webriti.com/"><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/free/rambo-new.png" title="Rambo free wordpress theme" alt="Rambo free wordpress theme"></a>
			</div>
		</div>
		<div class="col-md-6 col-sm-6">
			<div class="demo-content">
				<h1 class="theme-title">Rambo</h1>
				<h2 class="theme-subtitle">Rambo is a Responsive Multipurpose WordPress Theme</h2>
				<p class="theme-description">
				Rambo is an ideal WordPress theme for those who wish to create an impressive web presence. It is professional, smooth and sleek, with a clean modern layout, for almost any business types: agency, freelance, blog, startup, portfolio, corporate, firms, law firms, digital media agency.</p>
				<div class="themedemo-btn">
					<a class="btn1 btn-green" target="_blank" href="https://rambo.webriti.com/"><i class="fa fa-television"></i>Demo Free</a><span class="or">Or</span>
					<a class="btn1 btn-darkblue" href="https://downloads.wordpress.org/theme/rambo.zip" target="_blank"><i class="fa fa-cart-plus"></i>Download Free</a>
				</div>
			</div>
		</div>
	</div>
		
	<!-- separator --><div class="col-md-12"><div class="theme_showcase_seperator"></div></div><!-- /separator -->
		
	<div class="row">
		<div class="col-md-6 col-sm-6">
			<div class="theme_showcase_area">
				<a href="https://wallstreet.webriti.com/"><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/free/wallstreet-new.png" title="WallStreet free wordpress theme" alt="WallStreet free wordpress theme"></a>
			</div>
		</div>
		<div class="col-md-6 col-sm-6">
			<div class="demo-content">
				<h1 class="theme-title">WallStreet</h1>
				<h2 class="theme-subtitle">A  Business, Modern & Portfolio Responsive WordPress Theme</h2>
				<p class="theme-description">				
				Wallstreet is an incredible multipurpose responsive theme coded & designed with a lot of care and love. Theme is well suited for companies, law firms, travel, photography, recipe, design, art, personal and any other creative websites and blogging.
				</p>
				<div class="themedemo-btn">
					<a class="btn1 btn-green" target="_blank" href="https://wallstreet.webriti.com/"><i class="fa fa-television"></i>Demo Free</a><span class="or">Or</span>
					<a class="btn1 btn-darkblue" href="https://downloads.wordpress.org/theme/wallstreet.zip" target="_blank"><i class="fa fa-cart-plus"></i>Download Free</a>
				</div>
			</div>
		</div>
	</div>	
		
	<!-- separator --><div class="col-md-12"><div class="theme_showcase_seperator"></div></div><!-- /separator -->
		
	<div class="row">
		<div class="col-md-6 col-sm-6">
			<div class="theme_showcase_area">
				<a href="http://webriti.com/quality-free/"><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/free/quality-new.png" title="Quality free wordpress theme" alt="Quality free wordpress theme"></a>
			</div>
		</div>
		<div class="col-md-6 col-sm-6">
			<div class="demo-content">
				<h1 class="theme-title">Quality</h1>
				<h2 class="theme-subtitle">A Responsive Business Theme</h2>
				<p class="theme-description">
				Quality Business theme which is ideal for creating a corporate / business website. It boasts of 3 beautifully designed page template namely Business Page, Full-width and Blog. Theme is built on Bootstrap Css Framework, hence make it responsive in all the devices.</p>
				<div class="themedemo-btn">
					<a class="btn1 btn-green" target="_blank" href="http://webriti.com/quality-free/"><i class="fa fa-television"></i>Demo Free</a><span class="or">Or</span>
					<a class="btn1 btn-darkblue" href="https://downloads.wordpress.org/theme/quality.zip" target="_blank"><i class="fa fa-cart-plus"></i>Download Free</a>
				</div>
			</div>
		</div>
	</div>	
	
	<!-- separator --><div class="col-md-12"><div class="theme_showcase_seperator"></div></div><!-- /separator -->
		
	<div class="row">
		<div class="col-md-6 col-sm-6">
			<div class="theme_showcase_area">
				<a href="https://busiprof.webriti.com/"><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/free/busiprof-new.png" title="Busiprof free wordpress theme" alt="Busiprof free wordpress theme"></a>
			</div>
		</div>
		<div class="col-md-6 col-sm-6">
			<div class="demo-content">
				<h1 class="theme-title">Busiprof</h1>
				<h2 class="theme-subtitle">A Beautiful and Flexible Responsive Business WordPress Theme</h2>
				<p class="theme-description">
				Busiprof is a business WordPress theme. Already thousands of users loving this theme because it is designed for multiple businesses like app landing page, product launching page, agency, freelancers, blog, startup, portfolio, corporate and any kind of business.</p>
				<div class="themedemo-btn">
					<a class="btn1 btn-green" target="_blank" href="https://busiprof.webriti.com/"><i class="fa fa-television"></i>Demo Free</a><span class="or">Or</span>
					<a class="btn1 btn-darkblue" href="https://downloads.wordpress.org/theme/busiprof.zip" target="_blank"><i class="fa fa-cart-plus"></i>Download Free</a>
				</div>
			</div>
		</div>
	</div>
	
	<!-- separator --><div class="col-md-12"><div class="theme_showcase_seperator"></div></div><!-- /separator -->
		
	<div class="row">
		<div class="col-md-6 col-sm-6">
			<div class="theme_showcase_area">
				<a href="https://elitepress.webriti.com/"><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/free/elitepress-new.png" title="ElitePress free wordpress theme" alt="ElitePress free wordpress theme"></a>
			</div>
		</div>
		<div class="col-md-6 col-sm-6">
			<div class="demo-content">
				<h1 class="theme-title">ElitePress</h1>
				<h2 class="theme-subtitle">Responsive Multipurpose Wordpress Theme.</h2>
				<p class="theme-description">
				ElitePress WordPress theme is developed for corporate business. It provides you simple and elegant look. Already thousands of users loving this theme because it is designed for multiple businesses like digital agency, freelancers, blogger, startup, portfolio, corporate and any kind of business.</p>
				<div class="themedemo-btn">
					<a class="btn1 btn-green" target="_blank" href="https://elitepress.webriti.com/"><i class="fa fa-television"></i>Demo Free</a><span class="or">Or</span>
					<a class="btn1 btn-darkblue" href="https://downloads.wordpress.org/theme/elitepress.zip" target="_blank"><i class="fa fa-cart-plus"></i>Download Free</a>
				</div>
			</div>
		</div>
	</div>		
  
  <!-- separator --><div class="col-md-12"><div class="theme_showcase_seperator"></div></div><!-- /separator -->
  
  <div class="row">
		<div class="col-md-6 col-sm-6">
			<div class="theme_showcase_area">
				<a href="https://spasalon.webriti.com/"><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/free/spasalon-new.png" title="Spasalon free wordpress theme" alt="Spasalon free wordpress theme"></a>
			</div>
		</div>
		<div class="col-md-6 col-sm-6">
			<div class="demo-content">
				<h1 class="theme-title">SpaSalon</h1>
				<h2 class="theme-subtitle">A Beauty Salon Wordpress Theme</h2>
				<p class="theme-description">
				SpaSalon is a responsive multipurpose WordPress theme best suitable for  beauty industry like Cosmetology, Hairstyling, Spa, Beauty Salon, Massage &amp; Therapy Center and Beauty Care website. Theme is not only limited to beauty sector rather you can use it for any type of business.</p>
				<div class="themedemo-btn">
					<a class="btn1 btn-green" target="_blank" href="https://spasalon.webriti.com/"><i class="fa fa-television"></i>Demo Free</a><span class="or">Or</span>
					<a class="btn1 btn-darkblue" href="https://downloads.wordpress.org/theme/spasalon.zip" target="_blank"><i class="fa fa-cart-plus"></i>Download Free</a>
				</div>
			</div>
		</div>
	</div>	
  
  <!-- separator --><div class="col-md-12"><div class="theme_showcase_seperator"></div></div><!-- /separator -->
		
	<div class="row">
		<div class="col-md-6 col-sm-6">
			<div class="theme_showcase_area">
				<a href="http://webriti.com/demo/wp/lite/corpbiz/"><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/free/corpbiz.png" title="Corpbiz free wordpress theme" alt="Corpbiz free wordpress theme"></a>
			</div>
		</div>
		<div class="col-md-6 col-sm-6">
			<div class="demo-content">
				<h1 class="theme-title">Corpbiz</h1>
				<h2 class="theme-subtitle">A responsive Wordpress Theme for Corporates</h2>
				<p class="theme-description">
				Corpbiz Theme is very powerful yet easy to use and customize. This theme is a creative responsive WordPress theme for many purposes.</p>
				<div class="themedemo-btn">
					<a class="btn1 btn-green" target="_blank" href="http://webriti.com/demo/wp/lite/corpbiz/"><i class="fa fa-television"></i>Demo Free</a><span class="or">Or</span>
					<a class="btn1 btn-darkblue" href="https://github.com/webriti/free-wordpress-themes" target="_blank"><i class="fa fa-cart-plus"></i>Download Free</a>
				</div>
			</div>
		</div>
	</div>
  
  <!-- separator --><div class="col-md-12"><div class="theme_showcase_seperator"></div></div><!-- /separator -->
  
  <div class="row">
		<div class="col-md-6 col-sm-6">
			<div class="theme_showcase_area">
				<a href="https://appointment-green.webriti.com/"><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/free/appointment-green.png" title="Appointment free wordpress theme" alt="Appointment free wordpress theme"></a>
			</div>
		</div>
		<div class="col-md-6 col-sm-6">
			<div class="demo-content">
				<h1 class="theme-title">Appointment Green</h1>
				<h2 class="theme-subtitle">A  Business & Multipurpose WordPress Theme</h2>
				<p class="theme-description">
				Appointment Green WordPress Theme is a fully responsive and translation ready theme that allows you to create stunning blogs and websites. Theme is well suited for companies, law firms, travel, photography, recipe, design, art, personal and any other creative websites and blogs.</p>
				<div class="themedemo-btn">
					<a class="btn1 btn-green" target="_blank" href="https://appointment-green.webriti.com/"><i class="fa fa-television"></i>Demo Free</a><span class="or">Or</span>
					<a class="btn1 btn-darkblue" href="https://downloads.wordpress.org/theme/appointment-green.zip" target="_blank"><i class="fa fa-cart-plus"></i>Download Free</a>
				</div>
			</div>
		</div>
	</div>	
		
	<!-- separator --><div class="col-md-12"><div class="theme_showcase_seperator"></div></div><!-- /separator -->
  
  <div class="row">
		<div class="col-md-6 col-sm-6">
			<div class="theme_showcase_area">
				<a href="https://appointment-blue.webriti.com/"><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/free/appointment-blue.png" title="Appointment free wordpress theme" alt="Appointment free wordpress theme"></a>
			</div>
		</div>
		<div class="col-md-6 col-sm-6">
			<div class="demo-content">
				<h1 class="theme-title">Appointment Blue</h1>
				<h2 class="theme-subtitle">A  Business & Multipurpose WordPress Theme</h2>
				<p class="theme-description">
				Appointment Blue WordPress Theme is a fully responsive and translation ready theme that allows you to create stunning blogs and websites. Theme is well suited for companies, law firms, travel, photography, recipe, design, art, personal and any other creative websites and blogs.</p>
				<div class="themedemo-btn">
					<a class="btn1 btn-green" target="_blank" href="https://appointment-blue.webriti.com/"><i class="fa fa-television"></i>Demo Free</a><span class="or">Or</span>
					<a class="btn1 btn-darkblue" href="https://downloads.wordpress.org/theme/appointment-blue.zip" target="_blank"><i class="fa fa-cart-plus"></i>Download Free</a>
				</div>
			</div>
		</div>
	</div>	
		
	<!-- separator --><div class="col-md-12"><div class="theme_showcase_seperator"></div></div><!-- /separator -->
  
  <div class="row">
		<div class="col-md-6 col-sm-6">
			<div class="theme_showcase_area">
				<a href="https://appointment-red.webriti.com/"><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/free/appointment-red.png" title="Appointment free wordpress theme" alt="Appointment free wordpress theme"></a>
			</div>
		</div>
		<div class="col-md-6 col-sm-6">
			<div class="demo-content">
				<h1 class="theme-title">Appointment Red</h1>
				<h2 class="theme-subtitle">A  Business & Multipurpose WordPress Theme</h2>
				<p class="theme-description">
				Appointment Red WordPress Theme is a fully responsive and translation ready theme that allows you to create stunning blogs and websites. Theme is well suited for companies, law firms, travel, photography, recipe, design, art, personal and any other creative websites and blogs.</p>
				<div class="themedemo-btn">
					<a class="btn1 btn-green" target="_blank" href="https://appointment-red.webriti.com/"><i class="fa fa-television"></i>Demo Free</a><span class="or">Or</span>
					<a class="btn1 btn-darkblue" href="https://downloads.wordpress.org/theme/appointment-red.zip" target="_blank"><i class="fa fa-cart-plus"></i>Download Free</a>
				</div>
			</div>
		</div>
	</div>	
		
	<!-- separator --><div class="col-md-12"><div class="theme_showcase_seperator"></div></div><!-- /separator -->
	
	<div class="row">
		<div class="col-md-6 col-sm-6">
			<div class="theme_showcase_area">
				<a href="https://shk-corporate.webriti.com/"><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/free/shk-corporate-new.png" title="Shk corporate free wordpress theme" alt="Shk corporate free wordpress theme"></a>
			</div>
		</div>
		<div class="col-md-6 col-sm-6">
			<div class="demo-content">
				<h1 class="theme-title">Shk Corporate</h1>
				<h2 class="theme-subtitle">A Responsive Business Wordpress Theme</h2>
				<p class="theme-description">
				A Business theme which is ideal for creating corporate and any type of business website. SHK Corporate theme is a child theme of Appointment WordPress Theme. Theme uses the blue color variation that WordPress uses as default.</p>
				<div class="themedemo-btn">
					<a class="btn1 btn-green" target="_blank" href="https://shk-corporate.webriti.com/"><i class="fa fa-television"></i>Demo Free</a><span class="or">Or</span>
					<a class="btn1 btn-darkblue" href="https://downloads.wordpress.org/theme/shk-corporate.zip" target="_blank"><i class="fa fa-cart-plus"></i>Download Free</a>
				</div>
			</div>
		</div>
	</div>

  <!-- separator --><div class="col-md-12"><div class="theme_showcase_seperator"></div></div><!-- /separator -->
  
  <div class="row">
		<div class="col-md-6 col-sm-6">
			<div class="theme_showcase_area">
				<a href="https://vice.webriti.com/"><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/free/vice.png" title="Vice free wordpress theme" alt="Vice free wordpress theme"></a>
			</div>
		</div>
		<div class="col-md-6 col-sm-6">
			<div class="demo-content">
				<h1 class="theme-title">Vice</h1>
				<h2 class="theme-subtitle">A  Business & Multipurpose WordPress Theme</h2>
				<p class="theme-description">
				Appointment WordPress Theme is a fully responsive and translation ready theme that allows you to create stunning blogs and websites. Theme is well suited for companies, law firms, travel, photography, recipe, design, art, personal and any other creative websites and blogs.</p>
				<div class="themedemo-btn">
					<a class="btn1 btn-green" target="_blank" href="https://vice.webriti.com/"><i class="fa fa-television"></i>Demo Free</a><span class="or">Or</span>
					<a class="btn1 btn-darkblue" href="https://downloads.wordpress.org/theme/vice.zip" target="_blank"><i class="fa fa-cart-plus"></i>Download Free</a>
				</div>
			</div>
		</div>
	</div>

  <!-- separator --><div class="col-md-12"><div class="theme_showcase_seperator"></div></div><!-- /separator -->
  
  <div class="row">
		<div class="col-md-6 col-sm-6">
			<div class="theme_showcase_area">
				<a href="https://quality-green.webriti.com/"><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/free/quality-green.png" title="Quality free wordpress theme" alt="Quality free wordpress theme"></a>
			</div>
		</div>
		<div class="col-md-6 col-sm-6">
			<div class="demo-content">
				<h1 class="theme-title">Quality Green</h1>
				<h2 class="theme-subtitle">A Responsive Business Theme</h2>
				<p class="theme-description">
				Quality Green Business theme which is ideal for creating a corporate / business website. It boasts of 3 beautifully designed page template namely Business Page, Full-width and Blog. Theme is built on Bootstrap Css Framework, hence make it responsive in all the devices.</p>
				<div class="themedemo-btn">
					<a class="btn1 btn-green" target="_blank" href="https://quality-green.webriti.com/"><i class="fa fa-television"></i>Demo Free</a><span class="or">Or</span>
					<a class="btn1 btn-darkblue" href="https://downloads.wordpress.org/theme/quality-green.zip" target="_blank"><i class="fa fa-cart-plus"></i>Download Free</a>
				</div>
			</div>
		</div>
	</div>	
	
	<!-- separator --><div class="col-md-12"><div class="theme_showcase_seperator"></div></div><!-- /separator -->
  
  <div class="row">
		<div class="col-md-6 col-sm-6">
			<div class="theme_showcase_area">
				<a href="https://quality-blue.webriti.com/"><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/free/quality-blue.png" title="Quality free wordpress theme" alt="Quality free wordpress theme"></a>
			</div>
		</div>
		<div class="col-md-6 col-sm-6">
			<div class="demo-content">
				<h1 class="theme-title">Quality Blue</h1>
				<h2 class="theme-subtitle">A Responsive Business Theme</h2>
				<p class="theme-description">
				Quality Blue Business theme which is ideal for creating a corporate / business website. It boasts of 3 beautifully designed page template namely Business Page, Full-width and Blog. Theme is built on Bootstrap Css Framework, hence make it responsive in all the devices.</p>
				<div class="themedemo-btn">
					<a class="btn1 btn-green" target="_blank" href="https://quality-blue.webriti.com/"><i class="fa fa-television"></i>Demo Free</a><span class="or">Or</span>
					<a class="btn1 btn-darkblue" href="https://downloads.wordpress.org/theme/quality-blue.zip" target="_blank"><i class="fa fa-cart-plus"></i>Download Free</a>
				</div>
			</div>
		</div>
	</div>	
	
	<!-- separator --><div class="col-md-12"><div class="theme_showcase_seperator"></div></div><!-- /separator -->
  
  <div class="row">
		<div class="col-md-6 col-sm-6">
			<div class="theme_showcase_area">
				<a href="https://quality-orange.webriti.com/"><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/free/quality-orange.png" title="Quality free wordpress theme" alt="Quality free wordpress theme"></a>
			</div>
		</div>
		<div class="col-md-6 col-sm-6">
			<div class="demo-content">
				<h1 class="theme-title">Quality Orange</h1>
				<h2 class="theme-subtitle">A Responsive Business Theme</h2>
				<p class="theme-description">
				Quality Orange Business theme which is ideal for creating a corporate / business website. It boasts of 3 beautifully designed page template namely Business Page, Full-width and Blog. Theme is built on Bootstrap Css Framework, hence make it responsive in all the devices.</p>
				<div class="themedemo-btn">
					<a class="btn1 btn-green" target="_blank" href="https://quality-orange.webriti.com/"><i class="fa fa-television"></i>Demo Free</a><span class="or">Or</span>
					<a class="btn1 btn-darkblue" href="https://downloads.wordpress.org/theme/quality-orange.zip" target="_blank"><i class="fa fa-cart-plus"></i>Download Free</a>
				</div>
			</div>
		</div>
	</div>	
	
	<!-- separator --><div class="col-md-12"><div class="theme_showcase_seperator"></div></div><!-- /separator -->
  
  <div class="row">
		<div class="col-md-6 col-sm-6">
			<div class="theme_showcase_area">
				<a href="https://heroic.webriti.com/"><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/free/heroic.png" title="Quality free wordpress theme" alt="Quality free wordpress theme"></a>
			</div>
		</div>
		<div class="col-md-6 col-sm-6">
			<div class="demo-content">
				<h1 class="theme-title">Heroic</h1>
				<h2 class="theme-subtitle">A Responsive Business Theme</h2>
				<p class="theme-description">
				Heroic Business theme which is ideal for creating a corporate / business website.It boasts of 3 beautifully designed page template namely Business Page, Full-width and Blog. Theme is built on Bootstrap Css Framework, hence make it responsive in all the devices.</p>
				<div class="themedemo-btn">
					<a class="btn1 btn-green" target="_blank" href="https://heroic.webriti.com/"><i class="fa fa-television"></i>Demo Free</a><span class="or">Or</span>
					<a class="btn1 btn-darkblue" href="https://downloads.wordpress.org/theme/heroic.zip" target="_blank"><i class="fa fa-cart-plus"></i>Download Free</a>
				</div>
			</div>
		</div>
	</div>	
	
	<!-- separator --><div class="col-md-12"><div class="theme_showcase_seperator"></div></div><!-- /separator -->
  
   <div class="row">
		<div class="col-md-6 col-sm-6">
			<div class="theme_showcase_area">
				<a href="https://mazino.webriti.com/"><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/free/mazino.png" title="Mazino free wordpress theme" alt="Mazino free wordpress theme"></a>
			</div>
		</div>
		<div class="col-md-6 col-sm-6">
			<div class="demo-content">
				<h1 class="theme-title">Mazino</h1>
				<h2 class="theme-subtitle">A Responsive, Multi-purpose WordPress Theme</h2>
				<p class="theme-description">
				Mazino theme which is ideal for creating a corporate / business website.It offers a beautifully designed masonry blog layout. Theme is built on Bootstrap Css Framework, hence make it responsive in all the devices.</p>
				<div class="themedemo-btn">
					<a class="btn1 btn-green" target="_blank" href="https://mazino.webriti.com/"><i class="fa fa-television"></i>Demo Free</a><span class="or">Or</span>
					<a class="btn1 btn-darkblue" href="https://downloads.wordpress.org/theme/mazino.zip" target="_blank"><i class="fa fa-cart-plus"></i>Download Free</a>
				</div>
			</div>
		</div>
	</div>	
	
	<!-- separator --><div class="col-md-12"><div class="theme_showcase_seperator"></div></div><!-- /separator -->
		
	<div class="row">
		<div class="col-md-6 col-sm-6">
			<div class="theme_showcase_area">
				<a href="https://bluestreet.webriti.com/"><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/free/bluestreet.png" title="BlueStreet free wordpress theme" alt="BlueStreet free wordpress theme"></a>
			</div>
		</div>
		<div class="col-md-6 col-sm-6">
			<div class="demo-content">
				<h1 class="theme-title">BlueStreet</h1>
				<h2 class="theme-subtitle">A  Business, Modern & Portfolio Responsive WordPress Theme</h2>
				<p class="theme-description">				
				Bluestreet is an incredible multipurpose responsive theme coded & designed with a lot of care and love. Theme is well suited for companies, law firms, travel, photography, recipe, design, art, personal and any other creative websites and blogging.
				</p>
				<div class="themedemo-btn">
					<a class="btn1 btn-green" target="_blank" href="https://bluestreet.webriti.com/"><i class="fa fa-television"></i>Demo Free</a><span class="or">Or</span>
					<a class="btn1 btn-darkblue" href="https://downloads.wordpress.org/theme/bluestreet.zip" target="_blank"><i class="fa fa-cart-plus"></i>Download Free</a>
				</div>
			</div>
		</div>
	</div>
  
  <!-- separator --><div class="col-md-12"><div class="theme_showcase_seperator"></div></div><!-- /separator -->
  
  <div class="row">
		<div class="col-md-6 col-sm-6">
			<div class="theme_showcase_area">
				<a href="https://leo.webriti.com"><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/free/leo.png" title="Leo free wordpress theme" alt="Leo free wordpress theme"></a>
			</div>
		</div>
		<div class="col-md-6 col-sm-6">
			<div class="demo-content">
				<h1 class="theme-title">Leo</h1>
				<h2 class="theme-subtitle">A  Business, Modern & Portfolio Responsive WordPress Theme</h2>
				<p class="theme-description">				
				Leo is an incredible multipurpose responsive theme coded & designed with a lot of care and love. Theme is well suited for companies, law firms, travel, photography, recipe, design, art, personal and any other creative websites and blogging.
				</p>
				<div class="themedemo-btn">
					<a class="btn1 btn-green" target="_blank" href="https://leo.webriti.com"><i class="fa fa-television"></i>Demo Free</a><span class="or">Or</span>
					<a class="btn1 btn-darkblue" href="https://downloads.wordpress.org/theme/leo.zip" target="_blank"><i class="fa fa-cart-plus"></i>Download Free</a>
				</div>
			</div>
		</div>
	</div>
  
  <!-- separator --><div class="col-md-12"><div class="theme_showcase_seperator"></div></div><!-- /separator -->
  
  <div class="row">
		<div class="col-md-6 col-sm-6">
			<div class="theme_showcase_area">
				<a href="https://vdequator.webriti.com/"><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/free/vdequator.png" title="Vdequator free wordpress theme" alt="Vdequator free wordpress theme"></a>
			</div>
		</div>
		<div class="col-md-6 col-sm-6">
			<div class="demo-content">
				<h1 class="theme-title">Vdequator</h1>
				<h2 class="theme-subtitle">A Beautiful and Flexible Responsive Business WordPress Theme</h2>
				<p class="theme-description">				
				Vdequator is a business WordPress theme. Already thousands of users loving this theme because it is designed for multiple businesses like app landing page, product launching page, agency, freelancers, blog, startup, portfolio, corporate and any kind of business.
				</p>
				<div class="themedemo-btn">
					<a class="btn1 btn-green" target="_blank" href="https://vdequator.webriti.com/"><i class="fa fa-television"></i>Demo Free</a><span class="or">Or</span>
					<a class="btn1 btn-darkblue" href="https://downloads.wordpress.org/theme/vdequator.zip" target="_blank"><i class="fa fa-cart-plus"></i>Download Free</a>
				</div>
			</div>
		</div>
	</div>
  
  <!-- separator --><div class="col-md-12"><div class="theme_showcase_seperator"></div></div><!-- /separator -->
  
  <div class="row">
		<div class="col-md-6 col-sm-6">
			<div class="theme_showcase_area">
				<a href="https://vdperanto.webriti.com/"><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/free/vdperanto.png" title="Vdperanto free wordpress theme" alt="Vdperanto free wordpress theme"></a>
			</div>
		</div>
		<div class="col-md-6 col-sm-6">
			<div class="demo-content">
				<h1 class="theme-title">Vdperanto</h1>
				<h2 class="theme-subtitle">A Beautiful and Flexible Responsive Business WordPress Theme</h2>
				<p class="theme-description">				
				Vdperanto is a business WordPress theme. Already thousands of users loving this theme because it is designed for multiple businesses like app landing page, product launching page, agency, freelancers, blog, startup, portfolio, corporate and any kind of business.
				</p>
				<div class="themedemo-btn">
					<a class="btn1 btn-green" target="_blank" href="https://vdperanto.webriti.com/"><i class="fa fa-television"></i>Demo Free</a><span class="or">Or</span>
					<a class="btn1 btn-darkblue" href="https://downloads.wordpress.org/theme/vdperanto.zip" target="_blank"><i class="fa fa-cart-plus"></i>Download Free</a>
				</div>
			</div>
		</div>
	</div>
  
  <!-- separator --><div class="col-md-12"><div class="theme_showcase_seperator"></div></div><!-- /separator -->
  
  <div class="row">
		<div class="col-md-6 col-sm-6">
			<div class="theme_showcase_area">
				<a href="https://arzine.webriti.com/"><img class="img-responsive" src="<?php echo get_template_directory_uri() ?>/images/free/arzine.png" title="Arzine free wordpress theme" alt="Arzine free wordpress theme"></a>
			</div>
		</div>
		<div class="col-md-6 col-sm-6">
			<div class="demo-content">
				<h1 class="theme-title">Arzine</h1>
				<h2 class="theme-subtitle">A Beautiful and Flexible Responsive Business WordPress Theme</h2>
				<p class="theme-description">				
				Arzine is a business WordPress theme. It’s flexible and suitable for agencies, blogs, business, corporate. Customization is easy and straight-forward, with options provided that allow you to setup your site to perfectly fit your desired online presence. Arzine offers a beautifully designed masonry blog layout.
				</p>
				<div class="themedemo-btn">
					<a class="btn1 btn-green" target="_blank" href="https://arzine.webriti.com/"><i class="fa fa-television"></i>Demo Free</a><span class="or">Or</span>
					<a class="btn1 btn-darkblue" href="https://downloads.wordpress.org/theme/arzine.zip" target="_blank"><i class="fa fa-cart-plus"></i>Download Free</a>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row seperator_space"></div>
	
 </div>
<!-- /Browse Free wordPress Theme Section ---->

<?php get_footer(); ?>